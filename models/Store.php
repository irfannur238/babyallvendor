<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "store".
 *
 * @property int $idstore
 * @property string $name
 * @property string|null $description
 * @property string|null $owner
 * @property string $province_id
 * @property string $province_name
 * @property string $city_id
 * @property string $city_name
 * @property string $postal_code
 * @property string|null $address
 * @property string $phone
 * @property string|null $phone_other
 * @property string $email
 * @property string|null $district
 * @property int|null $status
 */
class Store extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'store';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'province_id', 'province_name', 'city_id', 'city_name', 'postal_code', 'phone', 'email'], 'required'],
            [['status'], 'integer'],
            [['name', 'owner'], 'string', 'max' => 30],
            [['description'], 'string', 'max' => 1024],
            [['province_id', 'postal_code', 'phone_other'], 'string', 'max' => 10],
            [['province_name', 'city_name', 'email', 'district'], 'string', 'max' => 50],
            [['city_id', 'phone'], 'string', 'max' => 20],
            [['address'], 'string', 'max' => 225],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idstore' => 'Idstore',
            'name' => 'Nama Toko',
            'description' => 'Deskripsi Toko',
            'owner' => 'Owner',
            'province_id' => 'Province ID',
            'province_name' => 'Province Name',
            'city_id' => 'City ID',
            'city_name' => 'City Name',
            'postal_code' => 'Kode Pos',
            'address' => 'Alamat',
            'phone' => 'Telepon',
            'phone_other' => 'Telepon Lainnya',
            'email' => 'Email',
            'district' => 'Kecamatan',
            'status' => 'Status',
        ];
    }
}
