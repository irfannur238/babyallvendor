<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payments".
 *
 * @property string $idpayment
 * @property int $idlevel
 * @property string $name
 * @property string|null $accountnumber
 * @property string|null $accountname
 * @property string|null $img
 * @property string|null $parent
 * @property int $status
 */
class Payments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idpayment', 'idlevel', 'name'], 'required'],
            [['idlevel', 'status'], 'integer'],
            [['idpayment', 'accountnumber', 'accountname', 'parent'], 'string', 'max' => 30],
            [['name', 'img'], 'string', 'max' => 50],
            [['idpayment'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idpayment' => 'Idpayment',
            'idlevel' => 'Idlevel',
            'name' => 'Name',
            'accountnumber' => 'Accountnumber',
            'accountname' => 'Accountname',
            'img' => 'Img',
            'parent' => 'Parent',
            'status' => 'Status',
        ];
    }
}
