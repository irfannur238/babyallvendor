<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shippers".
 *
 * @property string $idshipping
 * @property string $idorder
 * @property string|null $idprovorigin
 * @property string|null $nameprovorigin
 * @property string|null $idcityorigin
 * @property string|null $namecityorigin
 * @property string|null $idprovdestination
 * @property string|null $nameprovdestination
 * @property string|null $idcitydestination
 * @property string|null $namecitydestination
 * @property string|null $idcourier
 * @property string $namecourier
 * @property string|null $nameservice
 * @property int $cost
 * @property int|null $weight
 * @property string|null $receiptnumber
 * @property string|null $address
 * @property string|null $postal_code
 * @property string|null $address_phone
 *
 * @property Orders $idorder0
 */
class Shippers extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'shippers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['idshipping', 'idorder', 'namecourier', 'cost'], 'required'],
            [['cost', 'weight', 'postal_code'], 'integer'],
            [['idshipping', 'idorder', 'address_phone'], 'string', 'max' => 30],
            [['idprovorigin', 'idcityorigin', 'idprovdestination', 'idcitydestination', 'idcourier'], 'string', 'max' => 10],
            [['nameprovorigin', 'namecityorigin', 'nameprovdestination', 'namecitydestination', 'namecourier', 'receiptnumber', 'nameservice'], 'string', 'max' => 50],
            [['idshipping'], 'unique'],
            [['address'], 'string', 'max' => 1024],
            [['shipdate'], 'safe'],
            [['idorder'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['idorder' => 'idorder']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'idshipping' => 'Idshipping',
            'idorder' => 'Idorder',
            'idprovorigin' => 'Idprovorigin',
            'nameprovorigin' => 'Nameprovorigin',
            'idcityorigin' => 'Idcityorigin',
            'namecityorigin' => 'Namecityorigin',
            'idprovdestination' => 'Idprovdestination',
            'nameprovdestination' => 'Nameprovdestination',
            'idcitydestination' => 'Idcitydestination',
            'namecitydestination' => 'Namecitydestination',
            'idcourier' => 'Idcourier',
            'namecourier' => 'Namecourier',
            'nameservice' => 'Nameservice',
            'cost' => 'Cost',
            'weight' => 'Weight',
            'receiptnumber' => 'Receiptnumber',
            'address' => 'Address',
            'postal_code' => 'Kode Pos',
            'address_phone' => 'address_phone',
            'shipdate' => 'Tanggal Pengiriman'
        ];
    }

    /**
     * Gets query for [[Idorder0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdorder0() {
        return $this->hasOne(Orders::className(), ['idorder' => 'idorder']);
    }

}
