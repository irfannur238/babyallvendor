<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orderlog".
 *
 * @property int $idlog
 * @property string $idorder
 * @property string $datelog
 * @property int $status
 * @property string|null $note
 *
 * @property Orders $idorder0
 */
class Orderlog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orderlog';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idorder', 'datelog', 'status'], 'required'],
            [['datelog'], 'safe'],
            [['status'], 'integer'],
            [['idorder'], 'string', 'max' => 30],
            [['note'], 'string', 'max' => 1080],
            [['idorder'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['idorder' => 'idorder']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idlog' => 'Idlog',
            'idorder' => 'Idorder',
            'datelog' => 'Datelog',
            'status' => 'Status',
            'note' => 'Note',
        ];
    }

    /**
     * Gets query for [[Idorder0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdorder0()
    {
        return $this->hasOne(Orders::className(), ['idorder' => 'idorder']);
    }
}
