<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "verifypayment".
 *
 * @property int $idverify
 * @property string $idorder
 * @property string $date
 * @property string|null $note
 * @property string $img
 * @property int|null $isarchived
 * @property string $accountname
 * @property int $nominal
 *
 * @property Orders $idorder0
 */
class Verifypayment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'verifypayment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idorder', 'date', 'img', 'accountname', 'nominal'], 'required'],
            [['date'], 'safe'],
            [['isarchived', 'nominal'], 'integer'],
            [['idorder'], 'string', 'max' => 30],
            [['note'], 'string', 'max' => 1080],
            [['img'], 'string', 'max' => 100],
            [['accountname'], 'string', 'max' => 50],
            [['idorder'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['idorder' => 'idorder']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idverify' => 'Idverify',
            'idorder' => 'Idorder',
            'date' => 'Date',
            'note' => 'Note',
            'img' => 'Foto Bukti Transfer',
            'isarchived' => 'Isarchived',
            'accountname' => 'Accountname',
            'nominal' => 'Nominal',
        ];
    }

    /**
     * Gets query for [[Idorder0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdorder0()
    {
        return $this->hasOne(Orders::className(), ['idorder' => 'idorder']);
    }
}
