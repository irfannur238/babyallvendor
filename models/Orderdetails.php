<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orderdetails".
 *
 * @property string $idorderdetail
 * @property string $idorder
 * @property string $idproduct
 * @property string $idvarian
 * @property string $namevarian
 * @property int $price
 * @property string|null $size
 * @property int $weight
 * @property int|null $percent
 * @property int|null $iddiscount
 * @property int $qty
 * @property int $finalprice
 * @property int $finalweight
 * @property string $img
 * @property string|null $dateimg
 * @property int|null $oriprice
 *
 * @property Orders $idorder0
 */
class Orderdetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orderdetails';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idorderdetail', 'idorder', 'idproduct', 'idvarian', 'namevarian', 'price', 'weight', 'qty', 'finalprice', 'finalweight', 'img'], 'required'],
            [['price', 'weight', 'percent', 'iddiscount', 'qty', 'finalprice', 'finalweight', 'oriprice'], 'integer'],
            [['dateimg'], 'safe'],
            [['idorderdetail', 'idorder', 'idproduct', 'idvarian', 'size'], 'string', 'max' => 30],
            [['namevarian'], 'string', 'max' => 100],
            [['img'], 'string', 'max' => 50],
            [['idorderdetail'], 'unique'],
            [['idorder'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['idorder' => 'idorder']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idorderdetail' => 'Idorderdetail',
            'idorder' => 'Idorder',
            'idproduct' => 'Idproduct',
            'idvarian' => 'Idvarian',
            'namevarian' => 'Namevarian',
            'price' => 'Price',
            'size' => 'Size',
            'weight' => 'Weight',
            'percent' => 'Percent',
            'iddiscount' => 'Iddiscount',
            'qty' => 'Qty',
            'finalprice' => 'Finalprice',
            'finalweight' => 'Finalweight',
            'img' => 'Img',
            'dateimg' => 'Dateimg',
            'oriprice' => 'Oriprice',
        ];
    }

    /**
     * Gets query for [[Idorder0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdorder0()
    {
        return $this->hasOne(Orders::className(), ['idorder' => 'idorder']);
    }
}
