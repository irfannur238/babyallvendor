<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "varianimg".
 *
 * @property string $idvarian
 * @property string $idproduct
 * @property string $namevarian
 * @property int $qty
 * @property int|null $minqty
 * @property int $price
 * @property int $type
 * @property string|null $img
 * @property string|null $size
 *
 * @property Products $idproduct0
 */
class Varianimg extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'varianimg';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idvarian', 'idproduct', 'namevarian', 'qty', 'price', 'type'], 'required'],
            [['qty', 'minqty', 'price', 'type', 'ordering'], 'integer'],
            [['idvarian', 'idproduct', 'size', 'parent'], 'string', 'max' => 30],
            [['namevarian', 'uniquekey'], 'string', 'max' => 100],
            [['img'], 'string', 'max' => 50],
            [['datecreated'], 'safe'],
            [['img'], 'file', 'extensions' => 'png, jpg'], 
            [['idvarian', 'uniquekey'], 'unique'],
            [['idproduct'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['idproduct' => 'idproduct']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idvarian' => 'Idvarian',
            'idproduct' => 'Idproduct',
            'namevarian' => 'Namevarian',
            'qty' => 'Qty',
            'minqty' => 'Minqty',
            'price' => 'Price',
            'type' => 'Type',
            'img' => 'Img',
            'size' => 'Size',
            'uniquekey' => 'Key per product', 
            'parent' => 'Parent', 
            'datecreated' => 'Date created', 
            'ordering' => 'Urutan', 
        ];
    }

    /**
     * Gets query for [[Idproduct0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdproduct0()
    {
        return $this->hasOne(Products::className(), ['idproduct' => 'idproduct']);
    }
}
