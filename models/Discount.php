<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "discount".
 *
 * @property int $iddiscount
 * @property int $percent
 * @property string|null $startdate
 * @property string|null $enddate
 * @property int $idproduct
 * @property int $isperiod
 * @property int $status
 * @property int|null $minqty
 *
 * @property Products $idproduct0
 */
class Discount extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'discounts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['percent', 'idproduct', 'status'], 'required'],
            [['percent', 'idproduct', 'isperiod', 'status', 'minqty'], 'integer'],
            [['startdate', 'enddate'], 'safe'],
            [['idproduct'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['idproduct' => 'idproduct']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'iddiscount' => 'Iddiscount',
            'percent' => 'Percent',
            'startdate' => 'Startdate',
            'enddate' => 'Enddate',
            'idproduct' => 'Idproduct',
            'isperiod' => 'Isperiod',
            'status' => 'Status',
            'minqty' => 'Minqty',
        ];
    }

    /**
     * Gets query for [[Idproduct0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdproduct0()
    {
        return $this->hasOne(Products::className(), ['idproduct' => 'idproduct']);
    }
}
