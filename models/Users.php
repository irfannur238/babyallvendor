<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property int $iduser
 * @property string $username
 * @property string $password
 * @property string|null $name
 * @property int $role
 * @property string|null $lastlogin
 * @property string $userkey
 * @property string|null $lastname
 * @property string $email
 * @property int $status
 * @property string $phone
 *
 * @property Address[] $addresses
 */
class Users extends \yii\db\ActiveRecord
{
    var $province_id;
    var $city_id;
    var $address;
    var $postal_code;
    var $passconfirm;
    var $district;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'password', 'role', 'userkey', 'email', 'status', 'phone'], 'required'],
            [['role', 'status'], 'integer'],
            [['lastlogin', 'province_id', 'city_id'], 'safe'],
            [['username', 'phone'], 'string', 'max' => 20],
            [['password', 'lastname', 'email'], 'string', 'max' => 50],
            [['name', 'userkey'], 'string', 'max' => 30],
            [['userkey'], 'unique'],
            [['email'], 'unique'],
            [['province_id', 'city_id', 'address', 'postal_code', 'name', 'passconfirm', 'district'], 'required', 'on' => 'register'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'iduser' => 'Iduser',
            'username' => 'Username',
            'password' => 'Password',
            'name' => 'Nama Depan',
            'role' => 'Role',
            'lastlogin' => 'Lastlogin',
            'userkey' => 'Userkey',
            'lastname' => 'Nama Belakang',
            'email' => 'Email',
            'status' => 'Status',
            'phone' => 'No. HP', 
            'passconfirm' => 'Konfirmasi Password', 
            'district' => 'Kecamatan', 
        ];
    }

    /**
     * Gets query for [[Addresses]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(Address::className(), ['iduser' => 'iduser']);
    }
}
