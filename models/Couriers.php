<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "couriers".
 *
 * @property string $idcourier
 * @property string $name
 * @property int $status
 * @property string|null $note
 * @property string|null $img
 */
class Couriers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'couriers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idcourier', 'name', 'status'], 'required'],
            [['status'], 'integer'],
            [['idcourier'], 'string', 'max' => 30],
            [['name', 'img'], 'string', 'max' => 50],
            [['note'], 'string', 'max' => 1024],
            [['idcourier'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idcourier' => 'Idcourier',
            'name' => 'Name',
            'status' => 'Status',
            'note' => 'Note',
            'img' => 'Img',
        ];
    }
}
