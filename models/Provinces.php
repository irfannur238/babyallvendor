<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "provinces".
 *
 * @property string $province_id
 * @property string $province
 */
class Provinces extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'provinces';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['province_id', 'province'], 'required'],
            [['province_id'], 'string', 'max' => 10],
            [['province'], 'string', 'max' => 50],
            [['province_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'province_id' => 'Province ID',
            'province' => 'Province',
        ];
    }
}
