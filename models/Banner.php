<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "banner".
 *
 * @property int $idbanner
 * @property string|null $bannername
 * @property string|null $note
 * @property string $img
 * @property string $buttontext
 */
class Banner extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'banner';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['img', 'buttontext'], 'required'],
            [['bannername'], 'string', 'max' => 20],
            [['note', 'img'], 'string', 'max' => 50],
            [['buttontext'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idbanner' => 'Idbanner',
            'bannername' => 'Bannername',
            'note' => 'Note',
            'img' => 'Img',
            'buttontext' => 'Buttontext',
        ];
    }
}
