<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "configweb".
 *
 * @property int $idconfig
 * @property string|null $name
 * @property string|null $note
 * @property string|null $datecreated
 * @property int|null $type
 * @property string|null $address
 * @property string|null $phone
 * @property string|null $support
 * @property string|null $map
 * @property int|null $status
 * @property string|null $description
 */
class Configweb extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'configwebs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['note', 'address', 'map', 'description'], 'string'],
            [['datecreated'], 'safe'],
            [['type', 'status'], 'integer'],
            [['name', 'support'], 'string', 'max' => 100],
            [['phone'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idconfig' => 'Idconfig',
            'name' => 'Name',
            'note' => 'Note',
            'datecreated' => 'Datecreated',
            'type' => 'Type',
            'address' => 'Address',
            'phone' => 'Phone',
            'support' => 'Support',
            'map' => 'Map',
            'status' => 'Status',
            'description' => 'Description',
        ];
    }
}
