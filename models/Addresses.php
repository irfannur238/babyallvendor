<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "addresses".
 *
 * @property int $idaddress
 * @property int $iduser
 * @property string $province_id
 * @property string|null $province_name
 * @property string $city_id
 * @property string|null $city_name
 * @property string $postal_code
 * @property string $address
 * @property string|null $description
 * @property string|null $note
 * @property string|null $phone
 * @property int|null $status
 * @property string|null $district
 * @property string|null $name
 *
 * @property Users $iduser0
 */
class Addresses extends \yii\db\ActiveRecord
{
    var $fullname;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'addresses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iduser', 'province_id', 'city_id', 'postal_code', 'address'], 'required'],
            [['iduser', 'status'], 'integer'],
            [['province_id', 'postal_code'], 'string', 'max' => 10],
            [['province_name', 'city_name', 'district'], 'string', 'max' => 50],
            [['city_id', 'phone'], 'string', 'max' => 20],
            [['address', 'description', 'note'], 'string', 'max' => 225], 
            [['fullname'], 'safe'], 
            [['name'], 'string', 'max' => 30],
            [['iduser'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['iduser' => 'iduser']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idaddress' => 'Idaddress',
            'iduser' => 'Iduser',
            'province_id' => 'Province ID',
            'province_name' => 'Province Name',
            'city_id' => 'City ID',
            'city_name' => 'City Name',
            'postal_code' => 'Postal Code',
            'address' => 'Address',
            'description' => 'Description',
            'note' => 'Note',
            'phone' => 'Telepon',
            'status' => 'Status', 
            'district' => 'Kecamatan', 
            'name' => 'Nama Alamat', 
        ];
    }

    /**
     * Gets query for [[Iduser0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIduser0()
    {
        return $this->hasOne(Users::className(), ['iduser' => 'iduser']);
    }
}
