<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cities".
 *
 * @property string $city_id
 * @property string $province_id
 * @property string $province
 * @property string $type
 * @property string $city_name
 * @property int|null $postal_code
 */
class Checkout extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'checkout';
    }
    
    var $name;
    var $lastname;
    var $email;
    var $phone;
    var $province_id;
    var $province;
    var $city_id;
    var $city;
    var $district;
    var $postal_code;
    var $address;
    var $dropship;
    var $idcourier;
    var $note;

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['dropship', 'note'], 'safe'], 
            [['name', 'lastname', 'email', 'phone', 'province_id', 'province', 'city_id', 'city', 'district', 'address', 'idcourier'], 'required'], 
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'name' => 'Nama Depan',
            'lastname' => 'Nama Belakang',
            'email' => 'Email',
            'phone' => 'Telp',
            'province_id' => 'id prov',
            'province' => 'Provinsi',
            'city_id' => 'id kota',
            'city' => 'Kabupaten/Kota',
            'district' => 'Kecamatan',
            'address' => 'Alamat',
            'dropship' => 'Dropship', 
            'idcourier' => 'Kurir', 
        ];
    }

}
