<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "products".
 *
 * @property int $idproduct
 * @property string $productname
 * @property int $price
 * @property int|null $discount
 * @property int|null $idcategory
 * @property int|null $amount
 * @property string|null $description
 * @property string $datecreated
 * @property string|null $size
 * @property int $status
 * @property string|null $note
 * @property string|null $img
 * @property string|null $link1
 * @property string|null $link2
 * @property string|null $link3
 * @property string|null $sku
 * @property int|null $isfeatured
 * @property int|null $isvarian
 * @property int|null $width
 * @property int|null $height
 * @property int|null $weight
 * @property int|null $createby
 * @property int|null $hits
 *
 * @property Discount[] $discounts
 * @property Category $idcategory0
 * @property Varianimg[] $varianimgs
 */
class Products extends \yii\db\ActiveRecord {

    var $img1;
    var $img2;
    var $img3;
    var $img4;
    var $img5;
    var $search;
    var $variansel;
    var $sizesel;

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'products';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['productname', 'datecreated', 'status', 'weight'], 'required'],
            [['discount', 'idcategory', 'amount', 'status', 'isfeatured', 'isvarian', 'width', 'height', 'weight', 'createby', 'hits', 'isarchived'], 'integer'],
            [['description', 'link1', 'link2', 'link3'], 'string'],
            [['datecreated', 'variansel', 'sizesel', 'imgcreated'], 'safe'],
            [['productname', 'note', 'img'], 'string', 'max' => 225],
            [['size'], 'string', 'max' => 5],
            [['sku'], 'string', 'max' => 50],
            [['price', 'idproduct'], 'string', 'max' => 30], 
            [['idcategory'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['idcategory' => 'idcategory']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'idproduct' => 'Idproduct',
            'productname' => 'Nama Produk',
            'price' => 'Harga',
            'discount' => 'Diskon',
            'idcategory' => 'Kategori',
            'amount' => 'Amount',
            'description' => 'Deskripsi',
            'datecreated' => 'Datecreated',
            'size' => 'Size',
            'status' => 'Status',
            'note' => 'Note',
            'img' => 'Img',
            'link1' => 'Link1',
            'link2' => 'Link2',
            'link3' => 'Link3',
            'sku' => 'Sku',
            'isfeatured' => 'Isfeatured',
            'isvarian' => 'Isvarian',
            'width' => 'Lebar ',
            'height' => 'Tinggi ',
            'weight' => 'Berat',
            'createby' => 'Createby',
            'hits' => 'Hits', 
            'imgcreated' => 'imgcreated', 
            'isarchived' => 'isarchived'
        ];
    }

    /**
     * Gets query for [[Discounts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDiscounts() {
        return $this->hasMany(Discount::className(), ['idproduct' => 'idproduct']);
    }

    /**
     * Gets query for [[Idcategory0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdcategory0() {
        return $this->hasOne(Categories::className(), ['idcategory' => 'idcategory']);
    }

    /**
     * Gets query for [[Varianimgs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVarianimgs() {
        return $this->hasMany(Varianimg::className(), ['idproduct' => 'idproduct']);
    }

}
