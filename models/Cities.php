<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cities".
 *
 * @property string $city_id
 * @property string $province_id
 * @property string $province
 * @property string $type
 * @property string $city_name
 * @property int|null $postal_code
 */
class Cities extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cities';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city_id', 'province_id', 'province', 'type', 'city_name'], 'required'],
            [['postal_code'], 'integer'],
            [['city_id', 'type'], 'string', 'max' => 30],
            [['province_id'], 'string', 'max' => 10],
            [['province', 'city_name'], 'string', 'max' => 50],
            [['city_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'city_id' => 'City ID',
            'province_id' => 'Province ID',
            'province' => 'Province',
            'type' => 'Type',
            'city_name' => 'City Name',
            'postal_code' => 'Postal Code',
        ];
    }
}
