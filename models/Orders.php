<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property string $idorder
 * @property string $orderdate
 * @property int $status
 * @property int $idcustomer
 * @property string $invoicekey
 * @property int|null $totalweight
 * @property int|null $totalprice
 * @property int|null $typepayment
 * @property string|null $idpayment
 * @property string|null $paymentname
 * @property string|null $note
 * @property int|null $orderprice
 * @property string|null $typepaymentname
 * @property string|null $receiptnumber
 * @property string|null $shipdate
 * @property string|null $namevarian
 * @property int|null $totalqty
 *
 * @property Orderdetails[] $orderdetails
 * @property Orderlog[] $orderlogs
 * @property Shippers[] $shippers
 * @property Verifypayment[] $verifypayments
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idorder', 'orderdate', 'status', 'idcustomer', 'invoicekey'], 'required'],
            [['orderdate', 'shipdate'], 'safe'],
            [['status', 'idcustomer', 'totalweight', 'totalprice', 'typepayment', 'orderprice', 'totalqty'], 'integer'],
            [['namevarian'], 'string'],
            [['idorder', 'idpayment', 'paymentname'], 'string', 'max' => 30],
            [['invoicekey', 'typepaymentname', 'receiptnumber'], 'string', 'max' => 50],
            [['note'], 'string', 'max' => 1024],
            [['idorder'], 'unique'], 
            [['receiptnumber'], 'required', 'on' => 'inputreceiptnumber'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idorder' => 'Idorder',
            'orderdate' => 'Orderdate',
            'status' => 'Status',
            'idcustomer' => 'Idcustomer',
            'invoicekey' => 'Invoicekey',
            'totalweight' => 'Totalweight',
            'totalprice' => 'Totalprice',
            'typepayment' => 'Typepayment',
            'idpayment' => 'Idpayment',
            'paymentname' => 'Paymentname',
            'note' => 'Note',
            'orderprice' => 'Orderprice',
            'typepaymentname' => 'Typepaymentname',
            'receiptnumber' => 'Receiptnumber',
            'shipdate' => 'Shipdate',
            'namevarian' => 'Namevarian',
            'totalqty' => 'Totalqty',
        ];
    }

    /**
     * Gets query for [[Orderdetails]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrderdetails()
    {
        return $this->hasMany(Orderdetails::className(), ['idorder' => 'idorder']);
    }

    /**
     * Gets query for [[Orderlogs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrderlogs()
    {
        return $this->hasMany(Orderlog::className(), ['idorder' => 'idorder']);
    }

    /**
     * Gets query for [[Shippers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShippers()
    {
        return $this->hasMany(Shippers::className(), ['idorder' => 'idorder']);
    }

    /**
     * Gets query for [[Verifypayments]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVerifypayments()
    {
        return $this->hasMany(Verifypayment::className(), ['idorder' => 'idorder']);
    }
}
