<?php

namespace app\modules\apps\controllers;

use app\components\AdminController;
use app\components\Sendmail;
use app\helpers\Dlist;
use app\helpers\OrderUtils;
use app\helpers\Url;
use app\helpers\Utils;
use app\models\Addresses;
use app\models\Orderlog;
use app\models\Orders;
use app\models\User;
use app\models\Users;
use app\models\Verifypayment;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * Default controller for the `apps` module
 */
class CustomerController extends AdminController {

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex() {
        $model = $this->findModel(User::me()->iduser);

        $waiting_payment = OrderUtils::getOrderData(User::me()->getId(), [OrderUtils::STAT_WAITING_PAYMENT, OrderUtils::STAT_PAYMENT_VERIFIED, OrderUtils::STAT_PAYMENT_VERIFICATION]);
        $onprocess = OrderUtils::getOrderData(User::me()->getId(), [OrderUtils::STAT_ONPROCESS]);
        $onshipping = OrderUtils::getOrderData(User::me()->getId(), [OrderUtils::STAT_ONSHIPPING]);
        $done = OrderUtils::getOrderData(User::me()->getId(), [OrderUtils::STAT_DONE]);
        $canceled = OrderUtils::getOrderData(User::me()->getId(), [OrderUtils::STAT_CANCELED]);

        return $this->render('index', [
                    'model' => $model,
                    'waiting_payment' => $waiting_payment,
                    'onprocess' => $onprocess,
                    'onshipping' => $onshipping,
                    'done' => $done,
                    'canceled' => $canceled
        ]);
    }

    public function actionVerifypayment() {
        $model = new Verifypayment();

        if ($model->load(Yii::$app->request->post())) {
            $transaction = \Yii::$app->db->beginTransaction();
            $msg = null;

            try {
                $img = UploadedFile::getInstance($model, 'img');
                if (strlen(trim($img)) > 0 && is_object($img)) {
                    $name = 'VP' . Utils::getID() . '.' . $img->extension;
                    $dataTime = date('Y-m-d H:i:s');
                    $path = Url::setupBaseImg($dataTime);
                    if ($img->saveAs($path . $name)) {
                        $model->date = $dataTime;
                        $model->img = $name;
                        if ($model->save()) {
                            $m_order = Orders::findOne($model->idorder);
                            if ($m_order) {
                                $m_order->status = OrderUtils::STAT_PAYMENT_VERIFICATION;
                                if (!$m_order->save()) {
                                    $msg = json_encode($m_order->getErrors());
                                } else {
                                    OrderUtils::saveOrderLog($m_order);
                                }
                            }

                            if (!$msg) {
                                $email = User::getStoreEmail();
                                if ($email) {
                                    Sendmail::sendMailConfirmPayment($email, $model->idorder);
                                }
                                $transaction->commit();
                                Utils::flashSuccess('Bukti Bembayaran berhasil dikirim.');
                                return $this->redirect(['index']);
                            }
                        }
                    }

                    $msg = json_encode($model->getErrors());
                    if ($msg) {
                        Utils::flashDanger('Gagal kirim bukti pembayaran. Silahkan Ulangi.' . $msg);
                        $pathImg = Url::getBaseImg($name, $dataTime, true);
                        if (is_file($pathImg)) {
                            unlink($pathImg);
                        }
                        $transaction->rollBack();
                    }
                }
            } catch (Exception $e) {
                Utils::flashDanger('Gagal disimpan');
                $transaction->rollBack();
            }
        }

        return $this->render('verifypayment', [
                    'model' => $model,
        ]);
    }

    public function actionAddress() {
        $model = Addresses::find()
                ->where(['iduser' => User::me()->getId()])
                ->one();

        return $this->render('address', [
                    'model' => $model,
                    'm_user' => User::me(),
        ]);
    }

    public function actionUpaddress($id) {
        $model = Addresses::findOne($id);
        $provinces = Dlist::dListProvince();
        $cities = Dlist::dListCity($model->province_id);
        $model->fullname = User::me()->name . ' ' . User::me()->lastname;

        if ($model->load(Yii::$app->request->post())) {
            $model->phone = trim($model->phone);
            $model->province_name = (isset($provinces[$model->province_id]) ? $provinces[$model->province_id] : null);
            $cities = Dlist::dListCity($model->province_id);
            $model->city_name = (isset($cities[$model->city_id]) ? $cities[$model->city_id] : null);

            if ($model->save()) {
                Utils::flashSuccess('Berhasil Update Alamat.');
                return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
            }
        }

        return $this->renderAjax('_upaddress', [
                    'model' => $model,
                    'provinces' => $provinces,
                    'cities' => $cities,
        ]);
    }

    public function actionProfile() {
        $model = $this->findModel(User::me()->getId());

        if ($model->load(Yii::$app->request->post())) {
            if ($model->password != $model->oldAttributes['password']) {
                $model->password = md5($model->password);
            }

            if ($model->save()) {
                Utils::flashSuccess('Profile berhasil disimpan');
            } else {
                $msg = json_encode($model->getErrors());
                Utils::flashDanger($msg);
            }
        }

        return $this->render('profile', [
                    'model' => $model,
        ]);
    }

    public function actionAjaxdetailorder($id) {
        $dataOrder = OrderUtils::getOrderData(User::me()->getId(), null, $id);
        $orderLog = Orderlog::find()->where(['idorder' => $id])->asArray()->all();

        return $this->renderAjax('_detailOrder', [
                    'dataOrder' => $dataOrder,
                    'orderLog' => $orderLog,
        ]);
    }

    public function actionDetailorder($id) {
        $dataOrder = OrderUtils::getOrderData(User::me()->getId(), null, $id);
        $orderLog = Orderlog::find()->where(['idorder' => $id])->asArray()->all();

        return $this->render('detailorder', [
                    'dataOrder' => $dataOrder,
                    'orderLog' => $orderLog,
        ]);
    }

    protected function findModel($id) {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
