<?php

namespace app\modules\apps\controllers;

use app\components\AdminController;
use app\components\Sendmail;
use app\helpers\Configtype;
use app\helpers\DateUtils;
use app\helpers\OrderUtils;
use app\helpers\Utils;
use app\models\Addresses;
use app\models\Checkout;
use app\models\Couriers;
use app\models\Orderdetails;
use app\models\Orders;
use app\models\Payments;
use app\models\Shippers;
use app\models\User;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `apps` module
 */
class CheckoutController extends AdminController {

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex() {
        $model = new Checkout();
        $storeInfo = Configtype::getStoreInfo();
        $user = User::me();
        $addressActive = Addresses::findOne(['iduser' => $user->iduser, 'status' => Utils::STAT_ACTIVE]);

        $model->name = $user->name;
        $model->lastname = $user->lastname;
        $model->email = $user->email;
        $model->phone = ($addressActive ? $addressActive->phone : $user->phone);
        $model->province_id = ($addressActive ? $addressActive->province_id : null);
        $model->city_id = ($addressActive ? $addressActive->city_id : null);
        $model->province = ($addressActive ? $addressActive->province_name : null);
        $model->city = ($addressActive ? $addressActive->city_name : null);
        $model->district = ($addressActive ? $addressActive->district : null);
        $model->address = ($addressActive ? $addressActive->address : null);
        $model->postal_code = ($addressActive ? $addressActive->postal_code : null);

        $dataCart = OrderUtils::checkQtyCart();
        if (!$dataCart) {
            Utils::flashWarning('Keranjang Belanja Kosong.');
            return $this->redirect(['/apps/']);
        }
        $dataTotal = OrderUtils::getTotalPriceCartNew($dataCart, false);
        $total = $dataTotal['total'];
        $totalDiscount = $dataTotal['totalDiscount'];
        $totalQty = $dataTotal['totalQty'];
        $totalWeight = OrderUtils::getTotalWeightCart($dataCart);
        $msg = null;

        if (Yii::$app->request->isPost) {

            $transaction = \Yii::$app->db->beginTransaction();
            $m_order = new Orders();
            try {
                $m_order->idorder = Utils::getID();
                $m_order->orderdate = DateUtils::datetimeNow();
                $m_order->status = 1;
                $m_order->idcustomer = $user->iduser;
                $m_order->invoicekey = 'INV' . $m_order->idorder;
                $m_order->note = $_POST['Checkout']['note'];
                $m_order->totalweight = $totalWeight;
                $m_order->totalprice = $total;
                $idcourier = $_POST['Checkout']['idcourier'];
                $m_order->idpayment = $_POST['idpayment'];
                $m_order->paymentname = $_POST['bankname'];
                $m_order->typepayment = '123124356';
                $m_typepayment = Payments::findOne($m_order->typepayment);
                $m_order->typepaymentname = $m_typepayment ? $m_typepayment->name : '-';

                if ($m_order->save()) {
                    OrderUtils::saveOrderLog($m_order);
                    $m_shipment = new Shippers();
                    $m_shipment->idshipping = Utils::getID();
                    $m_shipment->idorder = $m_order->idorder;
                    $m_shipment->idprovorigin = $model->province_id;
                    $m_shipment->nameprovorigin = $storeInfo->province_name;
                    $m_shipment->idcityorigin = $storeInfo->city_id;
                    $m_shipment->namecityorigin = $storeInfo->city_name;
                    $m_shipment->idprovdestination = $model->province_id;
                    $m_shipment->nameprovdestination = $model->province;
                    $m_shipment->idcitydestination = $model->city_id;
                    $m_shipment->namecitydestination = $model->city;
                    $m_shipment->idcourier = $idcourier;
                    $m_courier = Couriers::findOne($idcourier);
                    $m_shipment->namecourier = ($m_courier ? $m_courier->name : '-');
                    $m_shipment->nameservice = $_POST['servicename'];
                    $m_shipment->cost = $_POST['ongkir'];
                    $m_shipment->weight = $totalWeight;
                    $m_shipment->address = $model->address;
                    $m_shipment->postal_code = $model->postal_code;
                    $m_shipment->address_phone = $model->phone;

                    if (!$m_shipment->save()) {
                        $msg = json_encode($m_shipment->getErrors());
                    }

                    $collectDet = [];
                    foreach ($dataCart as $key => $perItem) {
                        $m_orderdet = new Orderdetails();
                        $m_orderdet->idorderdetail = Utils::getID();
                        $m_orderdet->idorder = $m_order->idorder;
                        $m_orderdet->idproduct = $perItem['idproduct'];
                        $m_orderdet->idvarian = $perItem['idvarian'];
                        $m_orderdet->namevarian = $collectDet[$key]['namevarian'] = $perItem['name'];
                        $m_orderdet->price = $perItem['price'];
                        $m_orderdet->size = $perItem['size'];
                        $m_orderdet->weight = $perItem['weight'];
                        $m_orderdet->qty = $collectDet[$key]['qty'] = $perItem['qty'];
                        $m_orderdet->finalprice = OrderUtils::measurePrice($perItem['price'], $perItem['qty']);
                        $m_orderdet->finalweight = OrderUtils::measureWeight($perItem['weight'], $perItem['qty']);
                        $m_orderdet->img = $perItem['img'];
                        $m_orderdet->dateimg = $perItem['date'];
                        $m_orderdet->percent = ($perItem['percent'] == 0 ? null : $perItem['percent']);
                        $m_orderdet->oriprice = $perItem['oriPrice'];
                        if (!$m_orderdet->save()) {
                            $msg = json_encode($m_orderdet->getErrors());
                        } else {
                            OrderUtils::cutQty($m_orderdet->idvarian, $m_orderdet->qty);
                        }
                    }

                    $namevarian = OrderUtils::textVarian($collectDet);
                    $tQty = OrderUtils::textVarian($collectDet, true);

                    $m_order_up = Orders::findOne($m_order->idorder);
                    Utils::isExist($m_order_up);

                    $m_order_up->namevarian = $collectDet ? $namevarian : null;
                    $m_order_up->totalqty = $collectDet ? $tQty : null;
                    $m_order_up->orderprice = $m_order_up->totalprice + $m_shipment->cost;
                    $m_order_up->save();

                    if ($msg) {
                        $transaction->rollBack();
                        Utils::flashDanger($msg);
                    } else {
                        $transaction->commit();
                        if ($user->email) {
                            Sendmail::sendMailPayment($user->email, $m_order->idorder);
                        }
                        OrderUtils::removeAllCart();
                        return $this->redirect(['paymentdet', 'ui' => $m_order->idorder]);
                    }
                } else {
                    $msg = json_encode($m_order->getErrors());
                    $transaction->rollBack();
                }
            } catch (Exception $ex) {
                $transaction->rollBack();
                Utils::flashDanger('Terjadi Kesalahan');
            }
        }

        return $this->render('index', [
                    'model' => $model,
                    'dataCart' => $dataCart,
                    'total' => $total,
                    'totalQty' => $totalQty,
                    'storeInfo' => $storeInfo,
                    'totalWeight' => $totalWeight,
        ]);
    }

    public function actionDelete() {
        if (Utils::req()->isPost) {
            OrderUtils::removeAllCart();
            return $this->redirect(['/apps/p']);
        }
        throw new NotFoundHttpException('Method Not Allowed.');
    }

    public function actionPaymentdet($ui) {
        $model = Orders::findOne($ui);
        Utils::isExist($model);
        $m_payment = Payments::findOne($model->idpayment);
        Utils::isExist($m_payment);
        $m_shipper = Shippers::findOne(['idorder' => $model->idorder]);
        Utils::isExist($m_shipper);

        return $this->render('paymentdet', [
                    'model' => $model,
                    'm_payment' => $m_payment,
                    'm_shipper' => $m_shipper,
        ]);
    }

}
