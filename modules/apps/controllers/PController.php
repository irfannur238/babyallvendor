<?php

namespace app\modules\apps\controllers;

use app\helpers\ProductHelper;
use app\helpers\Url;
use app\models\Categories;
use app\models\Products;
use Yii;
use yii\data\Pagination;
use yii\db\Expression;
use yii\db\Query;
use yii\web\Controller;

/**
 * Default controller for the `apps` module
 */
class PController extends Controller {

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex() {
        $product = Products::find()
                ->where(['status' => 2])
                ->andWhere(['isfeatured' => 2])
                ->andWhere(['IS', 'isarchived', new Expression('null')]);
        $categories = Categories::find()->limit(4)->all();
        $count = $product->count();
        $pagination = new Pagination(['totalCount' => $count, 'defaultPageSize' => 12]);
        $models = $product->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();

        return $this->render('i', [
                    'models' => $models,
                    'pagination' => $pagination,
                    'categories' => $categories,
        ]);
    }

    public function actionR() {
        $model = new Products();
        $post = \Yii::$app->request->post();

        $prodActive = (new Query())
                ->select(['*'])
                ->from(Products::tableName())
                ->where(['IS', 'isarchived', new Expression('null')]);

        $productPre = (new Query())
                ->select(['*'])
                ->from($prodActive);

        if (isset($post['search'])) {
            ProductHelper::session('_QSEARCH', $post['search']);
            $q = explode(' ', $post['search']);
            foreach ($q as $r) {
                $productPre->orFilterWhere(['like', 'UPPER(productname)', strtoupper($r)]);
            }
        }

        $product = $productPre;
        $count = $product->count();
        $pagination = new Pagination(['totalCount' => $count, 'defaultPageSize' => 20]);
        $models = $product->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();

        if (isset($post['search']) && $count == 0) {
            Yii::$app->session->setFlash("danger", "Hasil pencarian '<code>" . $post['search'] . "</code>' hasil tidak ditemukan");
        } else {
            Yii::$app->session->setFlash("success", "Hasil pencarian '<code>" . ProductHelper::session('_QSEARCH') . "</code>' terdapat <code>" . $count . "</code> hasil ditemukan");
        }

        return $this->render('r', [
                    'models' => $models,
                    'pagination' => $pagination,
                    'count' => $count,
        ]);
    }

    public function actionV($id) {
        $model = Products::findOne($id);
        if (!$model) {
            $idcryp = Url::decrypt($id);
            $model = Products::findOne($idcryp);
        }
        
        $model->discount = ProductHelper::getDiscountProduct($model->idproduct, true);
        Url::isExist($model);
        $related = Products::find()
                ->where(['NOT IN', 'idproduct', $model->idproduct])
                ->andWhere(['status' => 2])
                ->andWhere(['IS', 'isarchived', new Expression('null')])
                ->limit(4)
                ->all();
        $photos = ProductHelper::getVarianProduct($model->idproduct, ProductHelper::TYPE_IMG);
        $varian = ProductHelper::getVarianProduct($model->idproduct);

        return $this->render('v', [
                    'model' => $model,
                    'related' => $related,
                    'photos' => $photos,
                    'varian' => $varian,
        ]);
    }

    public function actionC($id) {
        $product = Products::find()
                ->where(['status' => 2, 'idcategory' => $id])
                ->andWhere(['isfeatured' => 2])
                ->andWhere(['IS', 'isarchived', new Expression('null')]);
        $category = Categories::findOne($id);

        $count = $product->count();
        $pagination = new Pagination(['totalCount' => $count, 'defaultPageSize' => 12]);
        $models = $product->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();

        return $this->render('c', [
                    'models' => $models,
                    'pagination' => $pagination,
                    'category' => $category,
        ]);
    }

}
