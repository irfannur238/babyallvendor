<?php

namespace app\modules\apps\controllers;

use app\helpers\OrderUtils;
use app\helpers\ProductHelper;
use app\helpers\Url;
use app\helpers\Utils;
use app\models\Cities;
use app\models\Payments;
use app\models\Products;
use app\models\Provinces;
use app\models\Varianimg;
use Yii;
use yii\db\Exception;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\Controller;

/**
 * Default controller for the `apps` module
 */
class ApiController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'logout'],
                'rules' => [
                    [
                        'actions' => ['index', 'logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    'ajax_varian' => ['post']
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionAjax_varian() {
        if (Yii::$app->request->isPost) {
            $getParam = Yii::$app->request->post();
            if ($getParam) {
                $idproduct = Utils::issetVarNull($getParam['idproduct']);
                $varian = Utils::issetVarNull($getParam['varian']);
                $size = Utils::issetVarNull($getParam['size']);

                $result = ProductHelper::getVarianByParam($idproduct, $varian, $size);


                if ($result) {
                    $price = isset($result['price']) ? ProductHelper::idrPrice($result['price']) : '-';
                    $qty = isset($result['qty']) ? $result['qty'] : 0;
                    $percent = isset($result['percent']) ? $result['percent'] : 0;
                    $oriPrice = isset($result['oriPrice']) ? ProductHelper::idrPrice($result['oriPrice']) : '-';

                    echo '<div class="product__details__price">' . ($percent ? '<span>' . $oriPrice . '</span> ' : null) . $price . ($percent ? '<span><div class="label">' . $percent . ' % OFF</div></span>' : null) . '</div>
                        <span class="qtystock">Stok terserdia: ' . $qty . '</span> ' . Html::hiddenInput('qtystock', $qty, ['id' => 'hi-qty-stock']);
                }
            }
        }
    }

    public function actionGetimgsel() {
        if (Utils::req()->isPost) {
            $getParam = Utils::req()->post();
            $idproduct = Utils::issetVarNull($getParam['idproduct']);
            $varian = Utils::issetVarNull($getParam['varian']);

            $result = ProductHelper::getImgByParam($idproduct, $varian, null);
            if ($result) {
                echo '<img class="pic0" src="' . Url::getBaseImg($result['img'], $result['datecreated']) . '">';
            }
        }
    }

    public function actionDepsize() {
        if (Yii::$app->request->isPost) {
            $getParam = Yii::$app->request->post();
            if ($getParam) {
                $idproduct = Utils::issetVarNull($getParam['idproduct']);
                $varian = Utils::issetVarNull($getParam['varian']);

                $posts = Varianimg::find()
                        ->select(["varianimg.*", new Expression("UPPER(varianimg.size) labelsize")])
                        ->where(['type' => ProductHelper::TYPE_VARIAN])
                        ->andFilterWhere(['idproduct' => $idproduct])
                        ->andFilterWhere(['TRIM(UPPER(namevarian))' => trim(strtoupper($varian))])
                        ->asArray()
                        ->all();

                if (!empty($posts)) {
                    foreach ($posts as $post) {
                        if ($post['labelsize']) {
                            echo "<option value='" . $post['labelsize'] . "'>" . $post['labelsize'] . "</option>";
                        }
                    }
                }
            }
        }
    }

    public function actionCheckqty() {
        if (Yii::$app->request->isPost) {
            $getData = Yii::$app->request->post();
            $idproduct = isset($getData['idproduct']) ? $getData['idproduct'] : null;
            $varian = isset($getData['varian']) ? $getData['varian'] : null;
            $size = isset($getData['size']) ? $getData['size'] : null;
            $dataVarian = ProductHelper::getVarianByParam($idproduct, $varian, $size);
            $qty = $dataVarian ? $dataVarian['qty'] : 0;
            echo $qty;
        }
    }

    public function actionAddtocart() {
        if (Yii::$app->request->isPost) {
            $getData = Yii::$app->request->post();
            $idproduct = Utils::issetVarNull($getData['idproduct']);
            $varian = Utils::issetVarNull($getData['varian']);
            $size = Utils::issetVarNull($getData['size']);
            $qty = Utils::issetVarNull($getData['qty']);
            $model = Products::findOne($idproduct);

            if ($model && $idproduct && $varian) {
                $img = null;
                $date = null;
                $weight = $model->weight;
                $dataVarian = ProductHelper::getVarianByParam($idproduct, $varian, $size);

                if ($dataVarian) {
                    $imgvarian = Varianimg::findOne(['parent' => $dataVarian['idvarian']]);
                    if ($imgvarian) {
                        $img = $imgvarian->img;
                        $date = $imgvarian->datecreated;
                    } else {
                        $imgproduct = Varianimg::findOne(['idproduct' => $idproduct, 'type' => ProductHelper::TYPE_IMG]);
                        if ($imgproduct) {
                            $img = $imgproduct->img;
                            $date = $imgproduct->datecreated;
                        }
                    }

                    $sessName = OrderUtils::_SESS_CART_NAME;
                    $items = [];
                    $newItem = [
                        'idproduct' => $idproduct,
                        'idvarian' => $dataVarian['idvarian'],
                        'varian' => $varian,
                        'price' => $dataVarian['price'],
                        'size' => $size,
                        'name' => $model->productname . ' - ' . $varian . ', ' . $size,
                        'qty' => $qty,
                        'img' => $img,
                        'date' => $date,
                        'weight' => $weight,
                        'percent' => $dataVarian['percent'],
                        'oriPrice' => $dataVarian['oriPrice'],
                        'discountPrice' => $dataVarian['discountPrice'],
                        'outofstock' => null,
                    ];

                    $same = false;
                    $oldItem = Utils::session($sessName);
                    if ($oldItem) {
                        $oldItem = json_decode($oldItem, true);
                        foreach ($oldItem as $key => $perItem) {
                            if ($perItem['idvarian'] == $newItem['idvarian']) {
                                $items[] = [
                                    'idproduct' => $perItem['idproduct'],
                                    'idvarian' => $perItem['idvarian'],
                                    'varian' => $perItem['varian'],
                                    'price' => $perItem['price'],
                                    'size' => $perItem['size'],
                                    'name' => $perItem['name'],
                                    'qty' => $perItem['qty'] + $newItem['qty'],
                                    'img' => $perItem['img'],
                                    'date' => $perItem['date'],
                                    'weight' => $perItem['weight'],
                                    'percent' => $perItem['percent'],
                                    'oriPrice' => $perItem['oriPrice'],
                                    'discountPrice' => $perItem['discountPrice'],
                                    'outofstock' => null,
                                ];
                                $same = true;
                            } else {
                                $items[] = $perItem;
                            }
                        }
                    }

                    if (!$same) {
                        array_push($items, $newItem);
                    }

                    Utils::removeSession($sessName);
                    Utils::session($sessName, Json::encode($items));

                    return $this->renderAjax('msgCart', [
                                'model' => $model,
                                'newItem' => $newItem,
                    ]);
                }
            } else {
                return false;
            }
        }
    }

    public function actionCartheader() {
        if (Utils::req()->isPost) {
            $getData = Utils::session(OrderUtils::_SESS_CART_NAME);
            $data = json_decode($getData, true);
            return $this->renderAjax('cartheader', [
                        'data' => $data,
            ]);
        }
    }

    public function actionAjax_update_cart() {
        if (Yii::$app->request->isPost) {
            $getData = Yii::$app->request->post();
            if ($getData) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $upIdvarian = Utils::issetVarNull($getData['idvarian']);
                    $upQty = Utils::issetVarNull($getData['qty']);
                    $upDetitem = Utils::issetVarNull($getData['delitem']);

                    $sessName = OrderUtils::_SESS_CART_NAME;
                    $items = [];
                    $oldItem = Utils::session($sessName);
                    if ($oldItem) {
                        $oldItem = json_decode($oldItem, true);
                        foreach ($oldItem as $key => $perItem) {
                            if ($perItem['idvarian'] == $upIdvarian) {
                                if ($upDetitem == 1) {
                                    unset($oldItem[$key]);
                                } else {
                                    $items[] = [
                                        'idproduct' => $perItem['idproduct'],
                                        'idvarian' => $perItem['idvarian'],
                                        'varian' => $perItem['varian'],
                                        'price' => $perItem['price'],
                                        'size' => $perItem['size'],
                                        'name' => $perItem['name'],
                                        'qty' => $upQty,
                                        'img' => $perItem['img'],
                                        'date' => $perItem['date'],
                                        'weight' => $perItem['weight'],
                                        'percent' => $perItem['percent'],
                                        'oriPrice' => $perItem['oriPrice'],
                                        'discountPrice' => $perItem['discountPrice'],
                                        'outofstock' => null,
                                    ];
                                }
                            } else {
                                $items[] = $perItem;
                            }
                        }
                    }

                    if ($items) {
                        $dataTotal = OrderUtils::getTotalPriceCartNew($items);
                        Utils::removeSession($sessName);
                        Utils::session($sessName, Json::encode($items));

                        echo '
                            <h6>Keranjang total</h6>
                            <ul>
                                <li>Total Harga (' . $dataTotal['totalQty'] . ' Barang) <span>' . $dataTotal['totalOriPrice'] . '</span></li>
                                <li>Total Diskon Barang<span> - ' . $dataTotal['totalDiscount'] . '</span></li>
                            </ul>
                            <hr>
                            <ul>
                                <li>Total Harga <span>' . $dataTotal['total'] . '</span></li>
                            </ul>' . Html::a('Checkout', ['checkout/index'], ['class' => 'primary-btn site-btn']);
                    }
                    $transaction->commit();
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }
    }

    public function actionAjax_delete_cart() {
        if (Yii::$app->request->isPost) {
            $getData = Yii::$app->request->post();
            if ($getData) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $upIdvarian = Utils::issetVarNull($getData['idvarian']);
                    $upDetitem = Utils::issetVarNull($getData['delitem']);

                    $sessName = OrderUtils::_SESS_CART_NAME;
                    $items = [];
                    $onlyOne = false;
                    $oldItem = Utils::session($sessName);
                    if ($oldItem) {
                        $oldItem = json_decode($oldItem, true);
                        foreach ($oldItem as $key => $perItem) {
                            if ($perItem['idvarian'] == $upIdvarian) {
                                if ($upDetitem == 1) {
                                    if (count($oldItem) == 1) {
                                        OrderUtils::removeAllCart();
                                        $onlyOne = true;
                                    } else {
                                        unset($oldItem[$key]);
                                    }
                                }
                            } else {
                                $items[] = $perItem;
                            }
                        }
                    }

                    if ($items) {
                        $total = OrderUtils::getTotalPriceCartNew($items);
                        $total = [
                            'total' => $total['total'],
                            'totalOriPrice' => $total['totalOriPrice'],
                            'totalDiscount' => $total['totalDiscount'],
                            'totalQty' => $total['totalQty'],
                        ];
                        Utils::removeSession($sessName);
                        Utils::session($sessName, Json::encode($items));

                        return $this->renderAjax('_resDelCart', [
                                    'dataCart' => $items,
                                    'dataTotal' => $total,
                        ]);
                    } else {
                        return $this->renderAjax('_resDelCart', [
                                    'dataCart' => [],
                                    'total' => 0,
                        ]);
                    }
                    $transaction->commit();
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }
    }

    public function actionAjax_total_qty_bag() {
        echo OrderUtils::getTotalQtyCart();
    }

    public function actionFinalprice() {
        if (Yii::$app->request->isPost) {
            $getData = Yii::$app->request->post();
            $total = (int) Utils::issetVarNull($getData['total']);
            $ongkir = (int) Utils::issetVarNull($getData['ongkir']);

            $finalOngkir = ProductHelper::idrPrice($ongkir);
            $finalTotal = $total + $ongkir;
            $finalTotal = ProductHelper::idrPrice($finalTotal);
            echo $finalOngkir . ';' . $finalTotal;
        }
    }

    public function actionDescpayment() {
        if (Yii::$app->request->isPost) {
            $getData = Yii::$app->request->post();
            $idpayment = (int) Utils::issetVarNull($getData['idpayment']);

            $desc = Payments::find()
                    ->where(['status' => Utils::STAT_ACTIVE, 'idlevel' => 2])
                    ->andWhere(['idpayment' => $idpayment])
                    ->asArray()
                    ->one();

            if ($desc) {
                echo '
                    <div class="card border-dark mb-3" style="max-width: 100rem;">
                        <div class="card-body text-dark">
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="card-text">No Rek. </p>
                                    <h6 class="card-title"><strong>' . $desc['accountnumber'] . '</strong></h6>
                                    <p class="card-text">' . $desc['accountname'] . '</p>
                                </div>
                                <div class="col-md-6">
                                    <img class="card-img-top float-right" data-src="holder.js/100px180/" alt="100%x180" style="max-width: 50%; display: block;" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22286%22%20height%3D%22180%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20286%20180%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_17755d811d7%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A14pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_17755d811d7%22%3E%3Crect%20width%3D%22286%22%20height%3D%22180%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22107.1953125%22%20y%3D%2296.3%22%3E286x180%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" data-holder-rendered="true">
                                </div>
                            </div>
                        </div>
                    </div>
                ';
            }
        }
    }

    public function actionCheckshipment() {
        if (Yii::$app->request->isPost) {
            $getData = Yii::$app->request->post();
            $courier = Utils::issetVarNull($getData['idcourier']);
            $origin = Utils::issetVarNull($getData['origin']);
            $dest = Utils::issetVarNull($getData['dest']);
            $weight = Utils::issetVarNull($getData['weight']);

            $curl = curl_init();
            $param = "origin=$origin&destination=$dest&weight=$weight&courier=$courier";

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.rajaongkir.com/starter/cost",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $param,
                CURLOPT_HTTPHEADER => array(
                    "content-type: application/x-www-form-urlencoded",
                    "key: e1ce5cc62929c688cb4b06498d818104"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                $shipmentRes = json_decode($response, true);
                $resStatus = $shipmentRes['rajaongkir']['status'];
                if ($resStatus['code'] == 200) {
                    $result = $shipmentRes['rajaongkir']['results'];
                    foreach ($result as $perResult) {
                        $arrService = [];
                        foreach ($perResult['costs'] as $perCost) {
                            $cost = isset($perCost['cost'][0]) ? $perCost['cost'][0]['value'] : 0;
                            $etd = isset($perCost['cost'][0]) ? $etd = str_replace('HARI', '', $perCost['cost'][0]['etd']) : 0;
                            $note = isset($perCost['cost'][0]) ? $perCost['cost'][0]['note'] : '';
                            $arrService[$perCost['service']] = [
                                'label' => ProductHelper::idrPrice($cost) . ' &nbsp;<small class="text-muted">(Estimasi ' . $etd . ' Hari)</small>&nbsp;&nbsp;&nbsp;&nbsp;<strong>' . $perCost['service'] . '</strong>',
                                'cost' => $cost,
                                'etd' => $etd,
                                'note' => $note,
                            ];
                        }
                    }
                    echo '<div class="list-group" id="idservice" style="font-size: 14px;">';
                    if ($arrService) {
                        foreach ($arrService as $i => $perService) {
                            echo '
                            <input type="radio" name="RadioOngkir" value="' . $perService['cost'] . '" id="ser' . $i . '" servicename="' . $i . '" />
                            <label class="list-group-item" for="ser' . $i . '">' . $perService['label'] . '</label> 
                        ';
                        }
                    } else {
                        echo '<code>Sedang Gangguan. Silahkan Pilih Ekspedisi Lainnya.</code>';
                    }
                    echo '</div><hr>';
                } else {
                    echo '<code>Sedang Gangguan. Silahkan Pilih Ekspedisi Lainnya.</code>';
                }
            }
        } else {
            echo 'Method Not Allowed';
        }
    }

    public function actionInit_city($province = false) {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);
        $url = $province == 1 ? 'https://api.rajaongkir.com/starter/province' : 'https://api.rajaongkir.com/starter/city';
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "$url",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "key: e1ce5cc62929c688cb4b06498d818104"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $data = json_decode($response, true);
            $data = $data['rajaongkir']['results'];
            echo '<pre>';
            print_r($data);
            die;

            $transaction = \Yii::$app->db->beginTransaction();
            $msg = null;

            try {
                if ($province) {
                    Provinces::deleteAll();
                } else {
                    Cities::deleteAll();
                }
                foreach ($data as $perData) {
                    $model = $province ? new Provinces() : new Cities();
                    $attr = $model->getAttributes();
                    foreach ($attr as $key => $per) {
                        $model->$key = $perData[$key];
                    }
                    if (!$model->save()) {
                        $msg = json_encode($model->getErrors());
                        break;
                    }
                }
                if ($msg) {
                    $transaction->rollBack();
                    echo '<pre>';
                    print_r($msg);
                }
                $transaction->commit();
            } catch (Exception $ex) {
                $transaction->rollBack();
            }
        }
    }

    public function actionCheckcost() {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.rajaongkir.com/starter/cost",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "origin=135&destination=497&weight=100&courier=jne",
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
                "key: e1ce5cc62929c688cb4b06498d818104"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }
    }

    public function actionDepcity() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $ids = $_POST['depdrop_parents'];
            $province_id = empty($ids[0]) ? null : $ids[0];
            $list = Cities::find()
                            ->andFilterWhere(['province_id' => $ids])
                            ->orderBy(['city_name' => SORT_ASC])
                            ->asArray()->all();
            $selected = null;
            if ($ids != null && count($list) > 0) {
                $selected = '';
                foreach ($list as $i => $account) {
                    $out[] = ['id' => $account['city_id'], 'name' => $account['city_name']];
                    if ($i == 0) {
                        $selected = $account['city_id'];
                    }
                }
                return Json::encode(['output' => $out, 'selected' => $selected]);
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

}
