<?php

namespace app\modules\apps\controllers;

use app\helpers\Configtype;
use app\helpers\OrderUtils;
use app\helpers\Rolename;
use app\helpers\Utils;
use app\models\Addresses;
use app\models\Cities;
use app\models\Configweb;
use app\models\ContactForm;
use app\models\LoginForm;
use app\models\Provinces;
use app\models\User;
use app\models\Users;
use Yii;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\Response;
use const YII_ENV_TEST;

class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        if (User::me()->role == Rolename::SUPER_ADMIN) {
            return $this->redirect(['/admins/products']);
        } else {
            return $this->redirect(['p']);
        }
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            if (User::me()->role == Rolename::SUPER_ADMIN) {
                return $this->redirect(['index']);
            }
            return $this->goBack();
        }

        //$model->password = '';
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout();
        OrderUtils::removeAllCart();
        return $this->goHome();
    }

    public function actionRegister() {
        $model = new Users(['scenario' => 'register']);
        $provinces = ArrayHelper::map(Provinces::find()->all(), 'province_id', 'province');
        $cities = ArrayHelper::map(Cities::find()->all(), 'city_id', 'city_name');

        $msg = null;
        $transaction = \Yii::$app->db->beginTransaction();
        if ($model->load(Yii::$app->request->post())) {
            try {
                $tmpPassword = $model->password;
                $model->iduser = Utils::getNextID('iduser', Users::tableName());
                $model->username = Utils::getCharBeforeFind($model->email, '@') . Utils::randDigit();
                $model->userkey = Utils::getID();
                $model->status = Utils::STAT_ACTIVE;
                $model->role = Rolename::CUSTOMER;
                $model->password = md5($model->password);

                if ($model->save()) {
                    $prov = Provinces::findOne($model->province_id);
                    $city = Cities::findOne($model->city_id);
                    $m_addr = new Addresses();
                    $m_addr->idaddress = Utils::getNextID('idaddress', Addresses::tableName());
                    $m_addr->iduser = $model->iduser;
                    $m_addr->province_id = $model->province_id;
                    $m_addr->city_id = $model->city_id;
                    $m_addr->province_name = ($prov ? $prov->province : null);
                    $m_addr->city_name = ($city ? $city->city_name : null);
                    $m_addr->address = $model->address;
                    $m_addr->postal_code = $model->postal_code;
                    $m_addr->status = Utils::STAT_ACTIVE;
                    $m_addr->phone = $model->phone;
                    if (!$m_addr->save()) {
                        $msg = json_encode($m_addr->getErrors());
                        Utils::flashDanger($msg);
                        $transaction->rollBack();
                    }
                } else {
                    $msg = json_encode($model->getErrors());
                    Utils::flashDanger($msg);
                    $transaction->rollBack();
                }
                $model->password = $tmpPassword;
                
                $transaction->commit();
                Utils::flashSuccess('Berhasil Registrasi, email <code>' . $model->email . '</code>, username <code>' . $model->username . '</code>');
                return $this->goBack();
            } catch (Exception $e) {
                $transaction->rollBack();
            }
        }

        return $this->render('register', [
                    'model' => $model,
                    'provinces' => $provinces,
                    'cities' => $cities,
        ]);
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
                    'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionFindus() {
        return $this->render('findus', [
                    //'data' => $data
        ]);
    }

    public function actionTest() {
        $curDay = date('Y-m-d H:i:s');
        \app\components\Sendmail::sendTest('qyaraofficial2020@gmail.com');
        echo '<pre>';
        print_r($curDay);
        die;
        $order = \app\models\Orders::find()
                ->where(['status' => OrderUtils::STAT_ONSHIPPING])
                ->orderBy(['orderdate' => SORT_DESC])
                ->all();

        foreach ($order as $perOrder) {
            $howDayShip = strtotime('+14 day', strtotime($perOrder->shipdate));
            $curDay = strtotime(date('Y-m-d'));
            if ($curDay >= $howDayShip) {
                $m_order = \app\models\Orders::findOne(['idorder' => $perOrder->idorder]);
                if ($m_order) {
                    $m_order->status = OrderUtils::STAT_DONE;
                }
            }
        }
    }

}
