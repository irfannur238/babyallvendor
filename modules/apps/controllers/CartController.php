<?php

namespace app\modules\apps\controllers;

use app\helpers\OrderUtils;
use app\helpers\Utils;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `apps` module
 */
class CartController extends Controller {

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex() {
        $dataCart = OrderUtils::checkQtyCart();
        $total = OrderUtils::getTotalPriceCartNew($dataCart);
        $dataTotal = [
            'total' => $total['total'],
            'totalOriPrice' => $total['totalOriPrice'],
            'totalDiscount' => $total['totalDiscount'],
            'totalQty' => $total['totalQty'],
        ];

        return $this->render('index', [
                    'dataCart' => $dataCart,
                    'dataTotal' => $dataTotal,
        ]);
    }

    public function actionDelete() {
        if (Utils::req()->isPost) {
            OrderUtils::removeAllCart();
            return $this->redirect(['/apps/p']);
        }
        throw new NotFoundHttpException('Method Not Allowed.');
    }

}
