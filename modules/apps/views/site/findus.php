<?php
/* @var $this View */

use app\helpers\Configtype;
use app\helpers\Url;
use app\models\Configwebs;
use yii\web\View;

$this->title = 'Temukan Kami';
?>

<div class="breadcrumb-option">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb__links">
                    <a href="<?= Url::base() . '/apps' ?>"><i class="fa fa-home"></i> Beranda</a>
                    <span><?= $this->title ?></span>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="blog-details spad" style="padding-top: 10px;">
    <div class="container">
        <div class="blog__details__content">
            <div class="links__item">
                <?php
                $findUs = Configwebs::find()
                        ->where(['type' => Configtype::FIND_US])
                        ->orderBy(['idconfig' => SORT_ASC])
                        ->all();
                
                foreach ($findUs as $perData) {
                    $color = $perData->note ? 'color:' . $perData->note . ';' : null;
                    $link = $perData->name ? $perData->name : '#';
                    echo '<a href="' . $link . '" target="_blank">
                    <div class="card mb-12 a-link" style="max-width: 540px;">
                        <div class="row no-gutters">
                            <div class="col-md-9">
                                <div class="card-body">
                                    <h5 style="font-weight:bold;' . $color . '">' . $perData->description . '</h5>
                                </div>
                            </div>
                            <div class="col-md-3 pull-right">
                            </div>
                        </div>
                    </div>
                    </a></br>';
                }
                ?>

            </div>
        </div>
    </div>
</section>
