<?php
/* @var $this View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use app\helpers\Url;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

$this->title = 'Registrasi';
$css = '.site-login{margin-top: 150px;
    margin-left: auto;
    margin-right: auto;
    width: 300px;
    margin-bottom: 100px;
    }';
//$this->registerCss($css);
?>
<div class="site-login">

    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb__links">
                        <a href="./index.html"><i class="fa fa-home"></i> Home</a>
                        <span>Daftar</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!--    <h3><?= Html::encode($this->title) ?></h3>-->
    <section class="checkout spad">
        <div class="container">
            <?php $form = ActiveForm::begin(['options' => ['class' => 'checkout__form']]); ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="checkout__form__input">
                                <?= $form->field($model, 'name') ?>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="checkout__form__input">
                                <?= $form->field($model, 'lastname') ?>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="checkout__form__input">
                                <?= $form->field($model, 'email') ?>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="checkout__form__input">
                                <?= $form->field($model, 'phone') ?>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="checkout__form__input">
                                <?= $form->field($model, 'password')->passwordInput() ?>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="checkout__form__input">
                                <?= $form->field($model, 'passconfirm')->passwordInput() ?>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="checkout__form__input">
                                <?php
                                echo $form->field($model, 'province_id')->widget(Select2::classname(), [
                                    'data' => $provinces,
                                    'options' => [
                                        'placeholder' => '-- Pilih Provinsi --',
                                        'id' => 'province_id',
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ])->label('Provinsi');
                                ?>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="checkout__form__input">
                                <?=
                                $form->field($model, 'city_id')->widget(DepDrop::classname(), [
                                    'data' => $cities,
                                    'type' => DepDrop::TYPE_SELECT2,
                                    'options' =>
                                    [
                                        'id' => 'city_id',
                                        'placeholder' => Yii::t('app', '--Pilih Kabupaten / Kota--'),
                                    ],
                                    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                                    'pluginOptions' => [
                                        'depends' => ['province_id'],
                                        'url' => Url::to(['/apps/api/depcity']),
                                        'placeholder' => Yii::t('app', '--Pilih Semua--'),
                                        'allowClear' => true,
                                    ]
                                ])->label('Kabupaten / Kota');
                                ?>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="checkout__form__input">
                                <?= $form->field($model, 'district') ?>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="checkout__form__input">
                                <?= $form->field($model, 'postal_code')->label('Kode Pos') ?>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="checkout__form__input">
                                <?= $form->field($model, 'address')->textarea(['rows' => '6'])->label('Alamat') ?>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="checkout__form__checkbox">
                                <label for="acc">
                                    Create an acount?
                                    <input type="checkbox" id="acc">
                                    <span class="checkmark"></span>
                                </label>
                                <p>Create am acount by entering the information below. If you are a returing
                                    customer login at the <br />top of the page</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="form-group">
                <?=
                Html::submitButton('Daftar', [
                    'class' => 'btn btn-primary',
                    'data' => [
                        'confirm' => 'Apakah anda yakin data anda sudah benar ?',
                    ],
                ])
                ?>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </section>
</div>
