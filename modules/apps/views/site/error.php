<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-index error-page" style="padding-bottom: 170px"> 

    <h1 style="font-size: 30px;"><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger" style="margin-top:30px;">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <p>
        The above error occurred while the Web server was processing your request.
    </p>
    <p>
        Please contact us if you think this is a server error. Thank you.
    </p>
    
</div>
