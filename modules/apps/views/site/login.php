<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Login';
$css = '.site-login{margin-top: 150px;
    margin-left: auto;
    margin-right: auto;
    width: 300px;
    margin-bottom: 100px;
    }';
$this->registerCss($css);
?>
<div class="site-login">
    <h3><?= Html::encode($this->title) ?></h3>
    <br>
    <?php
    $form = ActiveForm::begin([
                'id' => 'login-form',
    ]);
    ?>

    <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label('Username / email') ?>

    <?= $form->field($model, 'password')->passwordInput() ?>

    <?= $form->field($model, 'rememberMe')->checkbox([]) ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11" style="padding: 0px;">
            <?= Html::submitButton('Login', [
                'class' => 'btn btn-primary', 
                'name' => 'login-button',
                'style' => 'width:300px;'
                ]) ?>
        </div>
    </div>
    <hr>
    <p class="text-center">Belum punya akun, Silahkan <?= Html::a('Daftar', ['site/register'])?></p>
    <?php ActiveForm::end(); ?>
</div>
