<?php

use app\helpers\ProductHelper;
use app\helpers\Url;
use richardfan\widget\JSRegister;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */

$this->title = 'Keranjang Belanja';
?>

<div class="site-index">
    <!-- Breadcrumb Begin -->
    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb__links">
                        <a href="./index.html"><i class="fa fa-home"></i> Home</a>
                        <span>Keranjang Belanja</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->
    <section class="shop-cart spad" id="res-del-cart">
        <div class="container">
            <?php if ($dataCart) { ?>
                <div id="loading-total-table" style="display: none">
                    <div class="d-flex justify-content-center">
                        <div class="spinner-border" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                    </div>
                </div>
                <div id="table-cart-item" style="display:block">
                    <div class="shop__cart__table">
                        <table style="table-layout: fixed;width: 100%;">
                            <thead>
                                <tr>
                                    <th width="70%">Produk</th>
                                    <th width="20%">Harga</th>
                                    <th width="20%">Jumlah</th>
                                    <th width="10%"></th>
                                </tr>
                            </thead>
                            <tbody class="form-wrapper">
                                <?php
                                foreach ($dataCart as $i => $row) {
                                    if ($row['outofstock'] == app\helpers\OrderUtils::OUTOFSTOCK) {
                                        $csTr = 'style="background-color: #efefef;"';
                                        $csImg = 'style="opacity: 40%;"';
                                        $csName = 'style="color: darkgrey;"';
                                        $cPrc = 'style="color: darkgrey;"';
                                        $dis = true;
                                        $txAlrt = '<p style="color: #ca1515;">Stok Habis</p>';
                                    } else {
                                        $csTr = $csImg = $csName = $cPrc = $txAlrt = '';
                                        $dis = false;
                                    }
                                    ?>
                                    <tr class="tr" id="tr-<?= $row['idvarian'] ?>" <?= $csTr ?>">
                                        <td class="cart__product__item">
                                            <img src="<?= Url::getBaseImg($row['img'], $row['date']) ?>"  alt="" class="img-fcart" <?= $csImg ?>>
                                            <div class="cart__product__item__title">
                                                <?= Html::a('<p ' . $csName . '><b>' . $row['name'] . '</b></p>', ['p/v', 'id' => $row['idproduct']]) ?>
                                            </div>
                                            <?= $txAlrt ?>
                                        </td>
                                        <td class="cart__price" width="60%" <?= $cPrc ?>>
                                            <?php
                                            echo ProductHelper::idrPrice($row['price']);
                                            echo Html::hiddenInput('idproduct', $row['idproduct'], ['class' => 'hi-idproduct']);
                                            echo Html::hiddenInput('varian', $row['varian'], ['class' => 'hi-varian']);
                                            echo Html::hiddenInput('idvarian', $row['idvarian'], ['class' => 'hi-idvarian']);
                                            echo Html::hiddenInput('size', $row['size'], ['class' => 'hi-size']);
                                            if ($row['percent']) {
                                                echo '<p>
                                                        <span class="badge badge-primary">' . $row['percent'] . '%</span>
                                                        <span style="text-decoration: line-through;color: #97a09d;">' . $row['oriPrice'] . '</span>
                                                        </p>';
                                            }
                                            ?>
                                        </td>
                                        <td class="cart__quantity" width="15%" style="width:100px;">
                                            <?php
                                            echo Html::textInput('qty-item', $row['qty'], ['disabled' => $dis, 'type' => 'number', 'id' => $row['idvarian'], 'min' => 1, 'max' => 999, 'class' => 'input-qty form-control']);
                                            echo Html::hiddenInput('delitem', NULL, ['class' => 'del-me', 'id' => 'del-' . $row['idvarian']]);
                                            ?>
                                            <p class="alert-qty" style="color:red;text-align: center;"></p>
                                        </td>
                                        <td class="cart__close"><span style="margin: 0px 20px 10px 20px;" class="icon_close btn-del-item" id="btn-del-<?= $row['idvarian'] ?>" data-name="<?= $row['name'] ?>" data-idvarian="<?= $row['idvarian'] ?>"></span></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="discount__content">
                            <h6></h6>
                            <!--                            <form action="#">
                                                            <input type="text" placeholder="Enter your coupon code">
                                                            <button type="submit" class="site-btn">Apply</button>
                                                        </form>-->
                        </div>
                    </div>
                    <div class="col-lg-6 offset-lg-6">
                        <div class="cart__total__procced">
                            <div id="loading-total" style="display: none">
                                <div class="d-flex justify-content-center">
                                    <div class="spinner-border" role="status">
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                </div>
                            </div>
                            <div id="item-total" style="display: block">
                                <h6>Keranjang total</h6>
                                <ul>
                                    <li>Total Harga (<?= $dataTotal['totalQty'] ?> Barang) <span><?= $dataTotal['totalOriPrice'] ?></span></li>
                                    <li>Total Diskon Barang<span> - <?= $dataTotal['totalDiscount'] ?></span></li>
                                </ul>
                                <hr>
                                <ul>
                                    <li>Total Harga <span><?= $dataTotal['total'] ?></span></li>
                                </ul>
                                <?= Html::a('Checkout', ['checkout/index'], ['class' => 'primary-btn site-btn']) ?>
                            </div>
                        </div>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">

                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <?= Html::a('Lanjut Belanja', ['/apps/p'], ['class' => 'btn btn-sm btn-primary pull-right', 'style' => 'margin-left: 5px;']) ?>
                        <?=
                        Html::a('Hapus Semua', ['delete'], [
                            'class' => 'btn btn-sm btn-danger pull-right',
                            'data' => [
                                'confirm' => 'Apakah anda yakin untuk hapus semua isi keranjang ?',
                                'method' => 'post',
                            ],
                        ])
                        ?>
                    </div>
                </div>
            <?php } else { ?>
                <div class="card text-center">
                    <div class="card-header">
                        Keranjang Belanja
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Opss Kosong !!!</h5>
                        <?= Html::a('Lanjut Belanja', ['/apps'], ['class' => 'btn btn-sm btn-primary']) ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </section>

</div>
<?php JSRegister::begin() ?>
<script>
    $(document).ready(function (e) {
        var _csrf = '<?= Yii::$app->request->csrfToken ?>';
        var loading_total = document.getElementById("loading-total");
        var loading_total_table = document.getElementById("loading-total-table");
        var item_total = document.getElementById("item-total");
        var table_cart_item = document.getElementById("table-cart-item");

        function initUpdate() {
            $('.form-wrapper').on('change', '.input-qty', function (e) {
                e.preventDefault();
                var selector = $(this).closest('.tr');
                var varian = selector.find('.hi-varian').val();
                var idvarian = selector.find('.hi-idvarian').val();
                var size = selector.find('.hi-size').val();
                var idproduct = selector.find('.hi-idproduct').val();
                var qty = selector.find('.input-qty').val();
                var delitem = document.getElementById('del-'.iditem);

                loading_total.style.display = 'block';
                item_total.style.display = 'none';

                if (qty < 1) {
                    document.getElementById(iditem).value = 1;
                    var qty = 1;
                }

                ajaxCheckQty(idproduct, varian, idvarian, size, qty, delitem, selector);
            });
        }

        function ajaxCheckQty(idproduct, varian, idvarian, size, qty, delitem, selector) {
            var qtyParam = qty;
            var datas = {idproduct, varian, size};

            $.ajax({
                url: "<?= Url::to(['api/checkqty']) ?>",
                type: "POST",
                dataType: 'html',
                data: datas,
                success: function (data) {
                    var stockQty = parseInt(data);

                    if (parseInt(qtyParam) > stockQty) {
                        selector.find('.input-qty').val(stockQty);
                        selector.find('.alert-qty').text('Stok tinggal ' + stockQty);

                        var qty = stockQty;
                        var datasUp = {idvarian, qty, delitem, _csrf};

                        ajaxUpdateCart(datasUp);
                        if (qty == 0) {
                            location.reload()
                        }
                    } else {
                        var qty = qtyParam;
                        var datasUp = {idvarian, qty, delitem, _csrf};

                        selector.find('.alert-qty').text('');
                        ajaxUpdateCart(datasUp);
                    }
                },
                error: function (xhr, error) {
                    alert('Gagal, silakan coba kembali.');
                }
            });
        }

//        function initUpdate() {
//            $('.input-qty').change(function () {
//                var iditem = this.id;
//                var qty = parseInt(document.getElementById(iditem).value);
//                var idvarian = iditem;
//                var delitem = document.getElementById('del-'.iditem);
//                loading_total.style.display = 'block';
//                item_total.style.display = 'none';
//
//                if (qty < 1) {
//                    document.getElementById(iditem).value = 1;
//                    var qty = 1;
//                }
//                var datas = {idvarian, qty, delitem, _csrf};
//                if (idvarian != '' && qty != '') {
//                    ajaxUpdateCart(datas);
//                }
//            });
//        }

        initUpdate();
        initDelete();
        ajaxTotalQtyBag();

        function ajaxUpdateCart(datas) {
            $.ajax({
                url: "<?= Url::to(['api/ajax_update_cart']) ?>",
                type: "POST",
                dataType: 'html',
                data: datas,
                success: function (data) {
                    loading_total.style.display = 'none';
                    item_total.style.display = 'block';
                    $('#item-total').html(data);
                    ajaxTotalQtyBag();
                },
                error: function (xhr, error) {
                    alert('Gagal, silakan coba kembali.');
                }
            });
        }

        function initDelete() {
            $('.btn-del-item').on('click', function (event) {
                event.preventDefault();
                var iditem = this.id;
                var idvarian = document.getElementById(iditem).getAttribute('data-idvarian');
                var name = document.getElementById(iditem).getAttribute('data-name');
                var delitem = 1;
                var datas = {iditem, idvarian, delitem, name, _csrf};

                var c = confirm(name + ' Anda yakin untuk di hapus?');
                console.log(datas);

                if (c) {
                    if (idvarian != '' && delitem != '') {
                        loading_total_table.style.display = 'block';
                        table_cart_item.style.display = 'none';
                        ajaxDeleteCart(datas);
                    }
                }
            });
        }

        function ajaxDeleteCart(datas) {
            $.ajax({
                url: "<?= Url::to(['api/ajax_delete_cart']) ?>",
                type: "POST",
                dataType: 'html',
                data: datas,
                success: function (data) {
                    loading_total_table.style.display = 'none';
                    table_cart_item.style.display = 'block';
                    $('#res-del-cart').html(data);
                    initUpdate();
                    initDelete();
                    ajaxTotalQtyBag();
                    location.reload() //initUPdate tidak jalan
                },
                error: function (xhr, error) {
                    alert('Gagal, silakan coba kembali.');
                }
            });
        }

        //deprecated
        $('.inc').click(function () {
            var qty = $(this).siblings('.input-qty').val();
            var num = 1;
            var final_qty = parseInt(qty) + num;
            console.log(final_qty);
        });

        $('.dec').click(function () {
            var qty = $(this).siblings('.input-qty').val();
            var num = 1;
            if (qty <= 1) {
                var final_qty = parseInt(qty);
                $(this).siblings('.input-qty').val(1);
                document.getElementById('input-qty-2').innerHTML = '1';

            } else {
                var final_qty = parseInt(qty) - num;
            }
            console.log(final_qty);
        });
    });
</script>
<?php
JSRegister::end()?>