<?php
/* @var $this View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model LoginForm */

use app\helpers\Dlist;
use app\helpers\OrderUtils;
use app\helpers\ProductHelper;
use app\helpers\Url;
use app\helpers\Utils;
use app\models\LoginForm;
use app\models\Payments;
use richardfan\widget\JSRegister;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

$this->title = 'Checkout';
$css = '.site-login{margin-top: 150px;
    margin-left: auto;
    margin-right: auto;
    width: 300px;
    margin-bottom: 100px;
    }';
?>
<div class="site-login">

    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb__links">
                        <a href="./index.html"><i class="fa fa-home"></i> Home</a>
                        <span>Checkout</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!--    <h3><?= Html::encode($this->title) ?></h3>-->
    <section class="checkout spad" style="padding-top: 40px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <?php
                    $form = ActiveForm::begin([
                                'options' => ['class' => 'checkout__form'],
                    ]);
                    ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-12">
                                    <?php
                                    echo '<b>Alamat Pengiriman</b>';
                                    echo '<hr>';
                                    echo '<div class="box-main-content">
                                        <div>
                                            <div class="box-content-parag">
                                                <b class="receiver-name">' . $model->name . ' ' . $model->lastname . '</b>&nbsp;<span class="address-title">(Rumah)</span>&nbsp;
                                                   
                                            </div>
                                            <div class="box-content-parag phone">' . $model->phone . '</div>
                                            <div class="box-content-parag"><div class="address-desc">' . $model->address . '</div>
                                                <div class="address-desc--dis-cit-pos">' . $model->district . ', Kab. ' . $model->city . ', ' . $model->province . ' ' . $model->postal_code . '</div>
                                            </div>
                                        </div>
                                    </div><br>';
                                    //echo Html::a('Edit Alamat', '#', ['url' => Url::to(['customer/upaddress?id=' . $model->phone], true), 'class' => 'btn btn-sm btn-primary btn-upaddress']);
                                    echo '<hr>';
                                    ?>
                                </div>
                                <div class="col-lg-12">
                                    <b>Belanja Anda</b><hr>
                                    <div class="card-body" style="padding:0px;">
                                        <table>
                                            <tbody>
                                                <?php
                                                $tWeight = [];
                                                if ($dataCart) {
                                                    foreach ($dataCart as $perData) {
                                                        if ($perData['outofstock'] != OrderUtils::OUTOFSTOCK) {
                                                            $weightKg = OrderUtils::totalWeightPerItem($perData['weight'], $perData['qty']);
                                                            $weight = OrderUtils::totalWeightPerItem($perData['weight'], $perData['qty'], false, false);
                                                            echo '<tr>
                                                                <td width="10%">
                                                                    <img class="card-img" src="' . Url::getBaseImg($perData['img'], $perData['date']) . '" alt="Card image cap" style="width: 50px;">
                                                                </td>
                                                                <td width="60%">
                                                                    ' . Html::a('<small class="text-muted">&nbsp;&nbsp;' . (strlen($perData['name']) > 40 ? substr($perData['name'], 0, 40) . ' .....' : $perData['name']) . '</small>', ['p/v', 'id' => $perData['idproduct']]) . '
                                                                    
                                                                        <p>
                                                                        &nbsp;&nbsp;' . $perData['qty'] . 'x (' . $weightKg . ') 
                                                                        &nbsp;<strong>' . ProductHelper::idrPrice($perData['price']) . '</strong>';
                                                            if ($perData['percent']) {
                                                                echo '&nbsp;<span class="badge badge-primary">' . $perData['percent'] . '%</span>
                                                                        &nbsp;<span style="text-decoration: line-through;color: #97a09d;">' . ProductHelper::idrPrice($perData['oriPrice']) . '</span>';
                                                            }
                                                            echo '</p>
                                                                </td>
                                                            </tr>';
                                                            $tWeight[] = $weight;
                                                        }
                                                    }
                                                    $totalWeight = array_sum($tWeight);
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <hr>
                                </div>
                                <div class="col-lg-12">
                                    <b>Pengiriman</b>
                                    <br><br>
                                    <div class="checkout__form__input">
                                        <?php
                                        echo $form->field($model, 'idcourier')->inline()->radioList(Dlist::dListCourier())->label(false);
                                        ?>
                                    </div>
                                    <hr>
                                </div>
                                <div class="col-lg-12">
                                    <div id="loading-total" style="display: none">
                                        <div class="d-flex justify-content-center">
                                            <div class="spinner-border" role="status">
                                                <span class="sr-only">Loading...</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="service-courier" style="display: block">
                                    </div>
                                </div>
                                <?php
                                echo Html::hiddenInput('ongkir', null, ['id' => 'input-ongkir', 'readonly' => true]);
                                echo Html::hiddenInput('servicename', null, ['id' => 'input-servicename']);
                                echo Html::hiddenInput('idpayment', null, ['id' => 'input-idpayment']);
                                echo Html::hiddenInput('bankname', null, ['id' => 'input-bankname']);
                                ?>
                                <div class="col-lg-12">
                                    <div class="checkout__form__input">
                                        <?= $form->field($model, 'note')->textarea(['rows' => '3'])->label('Catatan') ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="checkout__order">
                        <div class="checkout__order__product">
                            <ul>
                                <li>
                                    <span class="top__text">Ringkasan Belanja</span>
                                </li>
                                <li>Total Berat<span class="weight"><?= OrderUtils::satuanKg($totalWeight) ?></span></li>
                                <li>Total Harga (<?= $totalQty ?> Barang) <span><?= ProductHelper::idrPrice($total) ?></span></li>
                                <li>Total Ongkos Kirim<span class="ongkir">-</span></li>
                            </ul>
                        </div>
                        <div class="checkout__order__total">
                            <ul>
                                <li>Total Tagihan <span class="total-tagihan"><?= ProductHelper::idrPrice($total) ?></span></li>
                            </ul>
                        </div>
                        <div class="checkout__order__widget">
                            <label for="o-acc">
                                Sudah Benar 
                                <input type="checkbox" id="o-acc" checked>
                                <span class="checkmark"></span>
                            </label>
                            <p>Apakah tagihan dan barang belanja anda sudah benar ?</p>
                        </div>
                        <?=
                        Html::button('Pilih Pembayaran', [
                            'class' => 'btn site-btn',
                            'data-toggle' => 'modal',
                            'data-target' => '.bd-example-modal-lg',
                            'disabled' => true,
                            'style' => 'background: #aba9a9;',
                            'id' => 'btn-modal-payment',
                        ])
                        ?>
                    </div>
                </div>
                <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Pilih Pembayaran</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="card border-dark mb-3" style="max-width: 100rem;">
                                    <div class="card-body text-dark">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p class="card-text">Total Tagihan</p>
                                                <h6 class="card-title desc-total"></h6>
                                            </div>
                                            <div class="col-md-6">
                                                <p class="float-right" style="margin-top: 25px;"><strong>Detail</strong></p>
                                            </div>
                                        </div>
                                    </div>
                                </div><hr>
                                <div id="modal-list-payment">
                                    <p>Transfer Bank</p>
                                    <?php
                                    $list = Payments::find()
                                            ->where(['status' => Utils::STAT_ACTIVE, 'idlevel' => 2])
                                            ->all();

                                    if ($list) {
                                        echo '<div class="list-group" id="list-payment" style="font-size: 14px;">';
                                        foreach ($list as $i => $perList) {
                                            echo '
                                                    <input type="radio" name="RadioPayment" value="' . $perList['idpayment'] . '" id="pay' . $i . '" bankname="' . $perList['name'] . '"/>
                                                    <label class="list-group-item" for="pay' . $i . '">' . $perList['name'] . '</label>';
                                        }
                                        echo '</div>';
                                    }
                                    ?>
                                </div>
                                <div class="alert alert-warning alert-dismissible fade show" id="alert-chose-payment" role="alert" style="margin-top: 1px;">
                                    <small><strong>Silahkan Pilih Bank di atas!</strong> lalu klik tombol bayar di bawah.</small>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="alert alert-info alert-dismissible fade show" id="alert-click-bayar" role="alert" style="margin-top: 1px;display: none;">
                                    <small>Anda memilih 
                                        <strong><div id="bank"></div></strong> 
                                        untuk melanjutkan pembayaran silahkan klik tombol Bayar.</small>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <?=
                                Html::submitButton('Bayar', [
                                    'class' => 'site-btn',
                                    'disabled' => true,
                                    //'style' => 'background: #aba9a9;',
                                    'id' => 'btn-payment',
                                ])
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </section>
</div>

<?php JSRegister::begin() ?>
<script>
    $(document).ready(function (e) {
        var _csrf = '<?= Yii::$app->request->csrfToken ?>';
        var loading_total = document.getElementById("loading-total");
        var service_courier = document.getElementById("service-courier");
        var btn_payment = document.getElementById("btn-payment");
        var btn_modal_payment = document.getElementById("btn-modal-payment");

        $('#checkout-idcourier').on('change', function (event) {
            var idcourier = document.querySelector("input[name='Checkout[idcourier]']:checked").value;
            var origin = '<?= $storeInfo->city_id ?>';
            var dest = '<?= $model->city_id ?>';
            var weight = '<?= $totalWeight ?>';
            var datas = {idcourier, origin, dest, weight, _csrf};

            loading_total.style.display = 'block';
            service_courier.style.display = 'none';
            ajaxCheckshipment(datas);
        });

        function ajaxCheckshipment(datas) {
            $.ajax({
                url: "<?= Url::to(['api/checkshipment']) ?>",
                type: "POST",
                dataType: 'html',
                data: datas,
                success: function (data) {
                    loading_total.style.display = 'none';
                    service_courier.style.display = 'block';
                    $('#service-courier').html(data);
                    $('#payment-method').html(data);
                    serviceId();
                },
                error: function (xhr, error) {
                    alert('Gagal, silakan coba kembali.');
                }
            });
        }

        function serviceId() {
            $('#idservice').on('change', function (event) {
                var ongkir = document.querySelector("input[name='RadioOngkir']:checked").value;
                var serviceid = document.querySelector("input[name='RadioOngkir']:checked").getAttribute("servicename");
                var total = '<?= $total ?>';
                var datas = {total, ongkir, _csrf};
                disableBtn(btn_modal_payment);

                document.getElementById("input-ongkir").value = ongkir;
                document.getElementById("input-servicename").value = serviceid;

                ajaxFinalprice(datas);
            });
        }

        function ajaxFinalprice(datas) {
            $.ajax({
                url: "<?= Url::to(['api/finalprice']) ?>",
                type: "POST",
                dataType: 'html',
                data: datas,
                success: function (data) {
                    var array_data = String(data).split(";");
                    var finalOngkir = array_data[0];
                    var finalTotal = array_data[1];

                    $('.ongkir').html(finalOngkir);
                    $('.total-tagihan').html(finalTotal);
                    $('.desc-total').html(finalTotal);
                    enableBtn(btn_modal_payment);
                },
                error: function (xhr, error) {
                    alert('Gagal, silakan coba kembali.');
                }
            });
        }

        chosePayment();
        function chosePayment() {
            $('#list-payment').on('change', function (event) {
                var idpayment = document.querySelector("input[name='RadioPayment']:checked").value;
                var bankname = document.querySelector("input[name='RadioPayment']:checked").getAttribute("bankname");
                var alert_chose_payment = document.getElementById("alert-chose-payment");
                var alert_click_bayar = document.getElementById("alert-click-bayar");

                alert_chose_payment.style.display = 'none';
                alert_click_bayar.style.display = 'block';
                enableBtn(btn_payment);

                document.getElementById("input-idpayment").value = idpayment;
                document.getElementById("input-bankname").value = bankname;
                $('#bank').text(bankname);
                console.log(bankname);

            });
        }

        //deprecated
        function ajaxDescpayment(datas) {
            $.ajax({
                url: "<?= Url::to(['api/descpayment']) ?>",
                type: "POST",
                dataType: 'html',
                data: datas,
                success: function (data) {
                    $('#desc-payment').html(data);
                },
                error: function (xhr, error) {
                    alert('Gagal, silakan coba kembali.');
                }
            });
        }

        function validateSubmit() {
            $('.checkout__form').on('beforeSubmit', function (e) {
                if (!confirm("Everything is correct. Submit?")) {
                    return false;
                }
                return true;
            });
        }

        function enableBtn(btn_element, color) {
            var fcolor,
                    pcolor = color;
            if (pcolor != null) {
                fcolor = color;
            } else {
                fcolor = '#f3acac';
            }

            btn_element.disabled = false;
            btn_element.style.background = fcolor;
        }

        function disableBtn(btn_element) {
            btn_element.disabled = true;
            btn_element.style.background = '#aba9a9';
        }

        $('.btn-upaddress').click(function (e) {
            e.preventDefault();
            $('#modal-content').html('Mohon tunggu ...');
            $('.modal-title').text('');
            $('#modal-form').modal('show');
            ajaxTotalQtyBag();

            $.ajax({
                url: $(this).attr('url'),
                method: 'post',
                dataType: 'html',
                data: datas,
                success: function (data) {
                    $('#modal-content').html(data);
                },
                error: function (xhr, error) {
                    $('#modal-form').modal('hide');
                    alert('Gagal, silakan coba kembali.');
                }

            });

        });
    });
</script>
<?php
JSRegister::end();

$cssRL = '.list-group-item {
  user-select: none;
}


.list-group input[type="radio"] {
  display: none;
}

.list-group input[type="radio"] + .list-group-item {
  background-color:#f5f5f5;
  cursor: pointer;
}

.list-group input[type="radio"] + .list-group-item:before {
  content: "\2022";
  color: transparent;
  font-weight: bold;
  margin-right: 1em;
}

.list-group input[type="radio"]:checked + .list-group-item {
  background-color: #f3acac;
  color: #FFF;
}

.list-group input[type="radio"]:checked + .list-group-item > small.text-muted {
  color: #FFF !important;
}

.list-group input[type="radio"]:checked + .list-group-item:before {
  color: inherit;
}';
$this->registerCss($cssRL);
?>