<?php
/* @var $this View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model LoginForm */

use app\helpers\OrderUtils;
use app\helpers\ProductHelper;
use app\helpers\Url;
use app\helpers\Utils;
use app\models\LoginForm;
use app\models\Orderdetails;
use app\models\Shippers;
use yii\web\View;
?>

<table width="100%">
    <tbody>
        <?php
        $dataOrder = Orderdetails::find()
                ->where(['idorder' => $model->idorder])
                ->asArray()
                ->all();

        $m_shipper = Shippers::findOne(['idorder' => $model->idorder]);
        Utils::isExist($m_shipper);

        $tWeight = [];
        $tPrice = [];
        if ($dataOrder) {
            foreach ($dataOrder as $perData) {
                $weightKg = OrderUtils::satuanKg($perData['finalweight']);
                $weight = $perData['finalweight'];
                echo '<tr style=" border-bottom: 1px solid rgba(0,0,0,.1);">
                        <td width="10%">
                            <img class="card-img" src="' . Url::getBaseImg($perData['img'], $perData['dateimg']) . '" alt="Card image cap" style="width: 75px;">
                        </td>
                        <td width="60%">
                            <small class="text-muted">&nbsp;&nbsp;' . (strlen($perData['namevarian']) > 40 ? substr($perData['namevarian'], 0, 40) . ' .....' : $perData['namevarian']) . '</small>
                            <p><small>&nbsp;&nbsp;' . $perData['qty'] . 'x (' . $weightKg . ') 
                                      &nbsp;<strong>' . ProductHelper::idrPrice($perData['price']) . '</strong>
                                </small>
                            </p>
                        </td>
                        <td width="30%" class="text-lg-right">
                            <span class="text-lg-right" style="padding-right: 20px;">' . ProductHelper::idrPrice($perData['finalprice']) . '</span>
                        </td>
                    </tr>';
                $tWeight[] = $weight;
                $tPrice[] = $perData['finalprice'];
            }
            $totalWeight = array_sum($tWeight);
            $totalPrice = array_sum($tPrice);
        }
        ?>
    </tbody>
</table>
<hr>
<div class="row">
    <div class="col-md-6"><p>Jasa Pengiriman : JNE REG (<?= OrderUtils::satuanKg($totalWeight) ?>)</p></div>
    <div class="col-md-6 text-lg-right"><span style="padding-right: 20px;"><?= ProductHelper::idrPrice($m_shipper->cost) ?></span></div>
</div>
<hr>
<hr>
<div class="row">
    <div class="col-md-6"><p><b>Total Tagihan</b></p></div>
    <div class="col-md-6 text-lg-right"><span style="padding-right: 20px;"><b><?= ProductHelper::idrPrice(OrderUtils::summaryPriceTotal($model->totalprice, $m_shipper->cost)) ?></b></span></div>
</div>

