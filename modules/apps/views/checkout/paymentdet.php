<?php
/* @var $this View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model LoginForm */

use app\helpers\OrderUtils;
use app\helpers\ProductHelper;
use app\helpers\Url;
use app\models\LoginForm;
use yii\helpers\Html;
use yii\web\View;

$this->title = 'Pembayaran';
?>
<div class="site-login">

    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb__links">
                        <a href="./index.html"><i class="fa fa-home"></i> Home</a>
                        <span>Pembayaran</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="checkout spad" style="padding-top: 40px;">
        <div class="container">
            <h6>Transfer Bank</h6>
            <div class="bd-callout bd-callout-warning">
                <h5 id="conveying-meaning-to-assistive-technologies"><?= $m_payment->name ?></h5>
                <hr>
                <p>
                    <img class="card-img float-right" src="<?= Url::getImg($m_payment->img, '/img/payment/') ?>" alt="image cap" style="max-width: 80px;">
                <p><small>No. Rekening</small></p>
                <h4 id="conveying-meaning-to-assistive-technologies">
                    <?= $m_payment->accountnumber ?>
                </h4>
                </p>
                <hr>
                <h5><?= $m_payment->accountname ?></h5>
            </div>
            <br>
            <h6>Detail Order</h6>
            <a href="#" data-toggle="collapse" data-target="#detailpayment">
                <div class="bd-callout bd-callout-qyara">
                    <h5 id="conveying-meaning-to-assistive-technologies">Total Tagihan <span class="caret"></span> <span class="float-right"><?= ProductHelper::idrPrice(OrderUtils::summaryPriceTotal($model->totalprice, $m_shipper->cost)) ?></span></h5>
                </div>
            </a>
            <div id="detailpayment" class="collapse" style="background-color: #f9f9f9;padding: 20px;">
                <?php
                echo $this->render('_detailOrder', [
                    'model' => $model,
                ]);
                ?>
            </div><br>

            <div class="alert alert-info" role="alert" style="margin-top: 5px;">
                <p>Silahkan transfer tagihan ke Nomor rekening di atas.</p>
                <hr>
                <p class="mb-0">Upload bukti transfer, ke menu <b><?= Html::a('Bukti Pembayaran', ['customer/verifypayment']) ?></b> untuk memverifikasi pembayaran anda.</p>
            </div>
        </div>
    </section>

</div>

