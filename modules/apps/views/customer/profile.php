<?php
/* @var $this View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model LoginForm */

use app\models\LoginForm;
use richardfan\widget\JSRegister;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

$this->title = 'profile';
?>
<div class="site-login">

    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb__links">
                        <a href="./index.html"><i class="fa fa-home"></i> Home</a>
                        <span>Profile</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!--    <h3><?= Html::encode($this->title) ?></h3>-->
    <section class="checkout spad" style="padding-top: 40px;">
        <div class="container">

            <div class="row">
                <div class="col-lg-3">
                    <?php
                    echo $this->render('_sideBar')
                    ?>
                </div>

                <div class="col-lg-9">
                    <?php $form = ActiveForm::begin(['options' => ['class' => 'checkout__form']]); ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="checkout__form__input">
                                        <?= $form->field($model, 'name')->textInput(['disabled' => true, 'id' => 'form-name']) ?>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="checkout__form__input">
                                        <?= $form->field($model, 'lastname')->textInput(['disabled' => true, 'id' => 'form-lastname']) ?>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="checkout__form__input">
                                        <?= $form->field($model, 'email')->textInput(['disabled' => true, 'id' => 'form-email']) ?>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="checkout__form__input">
                                        <?= $form->field($model, 'phone')->textInput(['disabled' => true, 'id' => 'form-phone']) ?>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="checkout__form__input">
                                        <?= $form->field($model, 'password')->passwordInput(['disabled' => true, 'id' => 'form-password']) ?>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="checkout__form__input">
                                        <?= $form->field($model, 'passconfirm')->passwordInput(['disabled' => true, 'id' => 'form-passconfirm']) ?>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="checkout__form__checkbox">
                                        <p>Create am acount by entering the information below. If you are a returing
                                            customer login at the <br />top of the page</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="form-group">
                        <?php
                        echo ' ' . Html::button('Edit', ['class' => 'btn btn-primary btn-edit']);
                        echo ' ' . Html::button('Cancel', [
                            'class' => 'btn btn-warning btn-cancel',
                            'style' => 'display:none;',
                        ]);
                        echo ' ' . Html::submitButton('Simpan', [
                            'class' => 'btn btn-success btn-save',
                            'style' => 'display:none;',
                            'data' => [
                                'confirm' => 'Apakah anda yakin data anda sudah benar ?',
                            ],
                        ]);
                        ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
    </section>
</div>

<?php JSRegister::begin() ?>
<script>
    $(document).ready(function (e) {
        var idName = 'form-name';
        var idLastname = 'form-lastname';
        var idEmail = 'form-email';
        var idTelp = 'form-phone';
        var idPass = 'form-password';
        var idConfrmPass = 'form-passconfirm';
        var btnSave = '.btn-save';
        var btnEdit = '.btn-edit';
        var btnCancel = '.btn-cancel';

        $('.btn-edit').on('click', function (event) {
            disableForm(idName, false);
            disableForm(idLastname, false);
            disableForm(idEmail, false);
            disableForm(idTelp, false);
            disableForm(idPass, false);
            disableForm(idConfrmPass, false);

            $(btnSave).show();
            $(btnCancel).show();
            $(btnEdit).hide();
        });

        $('.btn-cancel').on('click', function (event) {
            disableForm(idName, true);
            disableForm(idLastname, true);
            disableForm(idEmail, true);
            disableForm(idTelp, true);
            disableForm(idPass, true);
            disableForm(idConfrmPass, true);

            $(btnSave).hide();
            $(btnCancel).hide();
            $(btnEdit).show();
        });

        function disableForm(id, status) {
            document.getElementById(id).disabled = status;
        }

    });
</script>
<?php JSRegister::end(); ?>