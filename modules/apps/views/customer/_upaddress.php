<?php

use app\helpers\Url;
use app\models\Verifypayment;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $model Verifypayment */
/* @var $form ActiveForm */
?>

<div class="site-login">
    <?php $form = ActiveForm::begin(['options' => ['class' => 'checkout__form']]); ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-12">
                    <div class="checkout__form__input">
                        <?= $form->field($model, 'name')->textInput(['placeholder' => 'contoh : Rumah, Kantor, dll']) ?>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="checkout__form__input">
                        <?= $form->field($model, 'fullname')->textInput(['disabled' => true])->label('Nama Lengkap') ?>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="checkout__form__input">
                        <?= $form->field($model, 'phone')->textInput(['type' => 'text', 'placeholder' => 'contoh : 081222333777'])->label('Telepon') ?>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="checkout__form__input">
                        <?php
                        echo $form->field($model, 'province_id')->widget(Select2::classname(), [
                            'data' => $provinces,
                            'options' => [
                                'placeholder' => '-- Pilih Provinsi --',
                                'id' => 'province_id',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Provinsi');
                        ?>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="checkout__form__input">
                        <?=
                        $form->field($model, 'city_id')->widget(DepDrop::classname(), [
                            'data' => $cities,
                            'type' => DepDrop::TYPE_SELECT2,
                            'options' =>
                            [
                                'id' => 'city_id',
                                'placeholder' => Yii::t('app', '--Pilih Kabupaten / Kota--'),
                            ],
                            'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                            'pluginOptions' => [
                                'depends' => ['province_id'],
                                'url' => Url::to(['/apps/api/depcity']),
                                'placeholder' => Yii::t('app', '--Pilih Semua--'),
                                'allowClear' => true,
                            ]
                        ])->label('Kabupaten / Kota');
                        ?>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="checkout__form__input">
                        <?= $form->field($model, 'district') ?>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="checkout__form__input">
                        <?= $form->field($model, 'postal_code')->label('Kode Pos') ?>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="checkout__form__input">
                        <?= $form->field($model, 'address')->textarea(['rows' => '6'])->label('Alamat') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="alert alert-warning" role="alert" style="margin-top: 0px;">
        Pastikan data alamat yang anda isikan benar.
    </div>

    <div class="form-group">
        <?=
        Html::submitButton('Simpan', [
            'class' => 'btn btn-primary',
            'data' => [
                'confirm' => 'Apakah anda yakin data sudah benar ?',
            ]
        ])
        ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>