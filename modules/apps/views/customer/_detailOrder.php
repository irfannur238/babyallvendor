<?php

use app\helpers\OrderUtils;
use app\helpers\ProductHelper;
use app\helpers\Url;
use app\models\User;
use yii\helpers\Html;

//echo '<pre>';
//print_r($dataOrder);
//die;
?>

<div class="detail-body" style="padding: 10px;">
    <div class="row">
        <div class="col-md-6 border-right">
            <p><small>Invoice : </small></p>
            <p id="conveying-meaning-to-assistive-technologies"><?= $dataOrder['invoicekey'] ?></p><br>

            <p><small>Status : </small></p>
            <p><?= OrderUtils::labelStatusOrder($dataOrder['status'], true) ?></p>
        </div>
        <div class="col-md-6">
            <p><small>Nama Toko : </small></p>
            <p><b><?= User::getStoreName() ?></b>  (<small><?= User::getStoreProvince() . ', ' . User::getStoreCity() ?></small>)</p><br>

            <p><small>Tanggal Pembelian</small></p>
            <p><?= $dataOrder['orderdate'] ?></p>
        </div>
    </div>
    <hr>

    <h6>Daftar Produk</h6><br>
    <?php foreach ($dataOrder['orderdetail'] as $key => $perDet) { ?>
        <div class="row">
            <div class="col-md-6 border-right">
                <a href="">
                    <img class="card-img float-left" src="<?= Url::getBaseImg($perDet['img'], $perDet['dateimg']) ?>" alt="image cap" style="width: 50px;margin-right: 15px;">
                    <h6 id="conveying-meaning-to-assistive-technologies" style="margin-bottom: 0px;"><?= $perDet['namevarian'] ?></h6>
                    <p>
                        <b><?= ProductHelper::idrPrice($perDet['price']) ?></b> 
                        <?php
                        echo $perDet['qty'] . ' Produk (' . OrderUtils::satuanKg($perDet['finalweight']) . ')';
                        if ($perDet['percent']) {
                            echo '&nbsp;<span class="badge badge-primary">' . $perDet['percent'] . '%</span>
                                  &nbsp;<span style="text-decoration: line-through;color: #97a09d;">' . ProductHelper::idrPrice($perDet['oriprice']) . '</span>';
                        }
                        ?>
                    </p>
                </a>
            </div>
            <div class="col-md-6">
                <div class="float-left">
                    <p><small>Total Harga Produk</small></p>
                    <p><?= ProductHelper::idrPrice($perDet['finalprice']) ?></p>
                </div>
                <?= Html::a('Beli Lagi', ['p/v', 'id' => $perDet['idproduct']], ['class' => 'btn btn-sm btn-success float-right']); ?>
            </div>
        </div><hr>
    <?php } ?>

    <h6>Catatan Pembeli :</h6>
    <p><?= $dataOrder['note'] ?></p><hr>


    <h6>Pengiriman</h6><br>
    <div class="row">
        <div class="col-md-12">
            <p><?= $dataOrder['namecourier'] . ' (' . $dataOrder['nameservice'] . ')' ?></p>
            <p>No. Resi: <?= $dataOrder['receiptnumber'] ?></p><br>

            <div class="row">
                <div class="col-md-6">
                    <p>Dikirim kepada <b><?= $dataOrder['cus_name'] . ' ' . $dataOrder['cus_lastname'] ?></b></p>
                    <p><?= $dataOrder['address'] ?></p>
                    <p><?= $dataOrder['namecitydestination'] . ', ' . $dataOrder['nameprovdestination'] . ', ' . $dataOrder['postal_code'] ?></p>
                    <p>Telp: <?= $dataOrder['address_phone'] ?></p>
                </div>
            </div>
        </div>
    </div>
    <hr>

    <h6>Pembayaran</h6><br>
    <div class="row">
        <div class="col-md-6 border-right">
            <p>Total Harga <?= '(' . $dataOrder['totaldetail'] . ' Barang)' ?></p>
            <p>Total Ongkos Kirim <?= '(' . OrderUtils::satuanKg($dataOrder['totalweight']) . ')' ?></p>
            <p>Total Bayar</p>
            <p>Metode Pembayaran</p>
        </div>
        <div class="col-md-6">
            <p><b><?= ProductHelper::idrPrice($dataOrder['totalprice']) ?></b></p>
            <p><b><?= ProductHelper::idrPrice($dataOrder['cost']) ?></b></p>
            <p><b><?= ProductHelper::idrPrice($dataOrder['orderprice']) ?></b></p>
            <p><b><?= $dataOrder['typepaymentname'] . ' (' . $dataOrder['paymentname'] . ') ' ?></b></p>
        </div>
    </div>
    <hr>

    <h6>Status Pesanan</h6><br>
    <?php
    if ($orderLog) {
        foreach ($orderLog as $perLog) {
            echo '
                    <div class="row">
                        <div class="col-md-3 border-right">
                            ' . OrderUtils::labelStatusOrder($perLog['status'], true, 'float-right') . '
                            <p class="float-right">' . $perLog['datelog'] . '</p>
                        </div>
                        <div class="col-md-8">
                            <p>' . $perLog['note'] . '</p>
                        </div>
                    </div><hr>
                ';
        }
    }
    ?>

</div>

