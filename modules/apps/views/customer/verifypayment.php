<?php

use app\helpers\OrderUtils;
use app\models\Verifypayment;
use kartik\file\FileInput;
use kartik\number\NumberControl;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $model Verifypayment */
/* @var $form ActiveForm */
$this->title = 'Konfirmasi Bayar';
?>

<div class="site-login">

    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb__links">
                        <a href="./index.html"><i class="fa fa-home"></i> Home</a>
                        <span>Konfirmasi Bayar</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="checkout spad" style="padding-top: 40px;">
        <div class="container">

            <div class="row">
                <div class="col-lg-3">
                    <?php
                    echo $this->render('_sideBar')
                    ?>
                </div>

                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-md-12">
                            <?php
                            $form = ActiveForm::begin([
                                        'id' => 'verifypayment',
                                        'options' => ['enctype' => 'multipart/form-data']
                            ]);
                            ?>

                            <?=
                            $form->field($model, 'idorder')->widget(Select2::classname(), [
                                'data' => OrderUtils::dropListMyOrder(OrderUtils::STAT_WAITING_PAYMENT),
                                'options' => [
                                    //'placeholder' => '-- Pilih Provinsi --',
                                    'id' => 'province_id',
                                ],
                                'pluginOptions' => [
                                    'allowClear' => false
                                ],
                            ])->label('Pilih Transaksi');
                            ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <?= $form->field($model, 'accountname')->textInput()->label('Atas Nama <code>(a/n)</code>') ?>
                                </div>
                                <div class="col-md-6">
                                    <?php
                                    $dispOptions = ['class' => 'form-control kv-monospace', 'required' => true];
                                    $saveOptions = [
                                        'type' => 'text',
                                        //'label' => '<label>Saved Value: </label>',
                                        'class' => 'kv-saved',
                                        'readonly' => true,
                                        'tabindex' => 1000,
                                        'style' => 'display:none;'
                                    ];

                                    $saveCont = ['class' => 'kv-saved-cont'];

                                    echo $form->field($model, 'nominal')->widget(NumberControl::classname(), [
                                        'maskedInputOptions' => [
                                            'prefix' => 'IDR ',
                                            //'suffix' => ' ¢',
                                            'allowMinus' => false
                                        ],
                                        'options' => $saveOptions,
                                        'displayOptions' => $dispOptions,
                                        'saveInputContainer' => $saveCont
                                    ])->label('Nominal Transfer');
                                    ?>
                                </div>
                            </div>
                            <?= $form->field($model, 'note')->textarea() ?>
                            <?=
                            $form->field($model, 'img')->widget(FileInput::classname(), [
                                'pluginOptions' => [
                                    //'showCaption' => false,
                                    //'showRemove' => false,
                                    'showUpload' => false,
                                //'browseClass' => 'btn btn-primary btn-block',
                                //'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                                //'browseLabel' => 'Select Photo'
                                ],
                                'options' => ['accept' => 'image/*'],
                            ]);
                            ?>

                            <div class="form-group">
                                <?= Html::submitButton('Kirim', ['class' => 'btn btn-success']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

