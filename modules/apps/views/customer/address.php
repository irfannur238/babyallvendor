<?php

use app\helpers\Url;
use app\models\Verifypayment;
use richardfan\widget\JSRegister;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $model Verifypayment */
/* @var $form ActiveForm */
$this->title = 'Alamat';
?>

<div class="site-login">

    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb__links">
                        <a href="./index.html"><i class="fa fa-home"></i> Home</a>
                        <span>Alamat</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="checkout spad" style="padding-top: 40px;">
        <div class="container">

            <div class="row">
                <div class="col-lg-3">
                    <?php
                    echo $this->render('_sideBar')
                    ?>
                </div>

                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-md-12">
                            <?php
                            $nameAddr = $model->name ? '(' . ucwords(strtolower($model->name)) . ')' : '';
                            echo '<b>Alamat Pengiriman</b>';
                            echo '<hr>';
                            echo '<div class="box-main-content">
                                        <div>
                                            <div class="box-content-parag">
                                                <b class="receiver-name">' . $m_user->name . ' ' . $m_user->lastname . '</b>&nbsp;<span class="address-title">' . $nameAddr . '</span>&nbsp;
                                                   
                                            </div>
                                            <div class="box-content-parag phone">' . $model->phone . '</div>
                                            <div class="box-content-parag"><div class="address-desc">' . $model->address . '</div>
                                                <div class="address-desc--dis-cit-pos">' . $model->district . ', Kab. ' . $model->city_name . ', ' . $model->province_name . ' ' . $model->postal_code . '</div>
                                            </div>
                                        </div>
                                    </div><br>';
                            echo Html::a('Edit Alamat', '#', ['url' => Url::to(['customer/upaddress?id=' . $model->idaddress], true), 'class' => 'btn btn-sm btn-primary btn-upaddress']);
                            echo '<hr>';
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>

<?php JSRegister::begin(); ?>
<script>
    $('.btn-upaddress').click(function (e) {
        e.preventDefault();
        $('#modal-content').html('Mohon tunggu ...');
        $('.modal-title').text('');
        $('#modal-form').modal('show');
        ajaxTotalQtyBag();

        $.ajax({
            url: $(this).attr('url'),
            method: 'post',
            dataType: 'html',
            data: datas,
            success: function (data) {
                $('#modal-content').html(data);
            },
            error: function (xhr, error) {
                $('#modal-form').modal('hide');
                alert('Gagal, silakan coba kembali.');
            }

        });

    });
</script>
<?php JSRegister::end(); ?>