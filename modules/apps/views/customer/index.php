<?php
/* @var $this View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model LoginForm */

use app\models\LoginForm;
use yii\web\View;

$this->title = 'Transaksi';
$cssTab = '
    section {
    padding: 60px 0;
}

section .section-title {
    text-align: center;
    color: #007b5e;
    margin-bottom: 50px;
    text-transform: uppercase;
}
#tabs{
    background: #007b5e;
    color: #eee;
}
#tabs h6.section-title{
    color: #eee;
}

#tabs .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
    color: #f9f8fb;
    background-color: #f3acac;
    border-color: transparent transparent #f3f3f3;
    /*border-bottom: 4px solid !important;*/
    /*font-size: 20px;*/
    font-weight: bold;
}
#tabs .nav-tabs .nav-link {
    border: 1px solid transparent;
    border-top-left-radius: .25rem;
    border-top-right-radius: .25rem;
    color: #eee;
    font-size: 20px;
}

.tab-content .tab-pane h6 {
    margin-bottom: 0px;
}

p {
    margin: 0 0 1px 0;
}

@media only screen and (max-width: 790px) {
    .border-right {
        border-right: 1px solid #ffffff!important;
    }
}
        ';
$this->registerCss($cssTab);
?>
<div class="site-login">

    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb__links">
                        <a href="./index.html"><i class="fa fa-home"></i> Home</a>
                        <span>Dashboard</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="checkout spad" style="padding-top: 40px;">
        <div class="container">

            <div class="row">
                <div class="col-lg-3">
                    <?php
                    echo $this->render('_sideBar')
                    ?>
                </div>

                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-md-12">
                            <nav>
                                <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Pembayaran <span class="badge badge-light"><?= count($waiting_payment) ?></span></a>
                                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Diproses <span class="badge badge-light"><?= count($onprocess) ?></span></a>
                                    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Dikirim <span class="badge badge-light"><?= count($onshipping) ?></span></a>
                                    <a class="nav-item nav-link" id="nav-about-tab" data-toggle="tab" href="#nav-about" role="tab" aria-controls="nav-about" aria-selected="false">Selesai <span class="badge badge-light"><?= count($done) ?></span></a>
                                    <a class="nav-item nav-link" id="nav-about-tab" data-toggle="tab" href="#nav-canceled" role="tab" aria-controls="nav-about" aria-selected="false">Dibatalkan <span class="badge badge-light"><?= count($canceled) ?></span></a>
                                </div>
                            </nav>
                            <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                    <?php
                                    if ($waiting_payment) {
                                        echo $this->render('_listOrder', [
                                            'dataOrder' => $waiting_payment,
                                        ]);
                                    }
                                    ?>
                                </div>
                                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                    <?php
                                    if ($onprocess) {
                                        echo $this->render('_listOrder', [
                                            'dataOrder' => $onprocess,
                                        ]);
                                    }
                                    ?>
                                </div>
                                <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                                    <?php
                                    if ($onshipping) {
                                        echo $this->render('_listOrder', [
                                            'dataOrder' => $onshipping,
                                        ]);
                                    }
                                    ?>
                                </div>
                                <div class="tab-pane fade" id="nav-about" role="tabpanel" aria-labelledby="nav-about-tab">
                                    <?php
                                    if ($done) {
                                        echo $this->render('_listOrder', [
                                            'dataOrder' => $done,
                                        ]);
                                    }
                                    ?>
                                </div>
                                <div class="tab-pane fade" id="nav-canceled" role="tabpanel" aria-labelledby="nav-c-tab">
                                    <?php
                                    if ($canceled) {
                                        echo $this->render('_listOrder', [
                                            'dataOrder' => $canceled,
                                        ]);
                                    }
                                    ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>