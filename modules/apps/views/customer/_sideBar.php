<?php

use yii\helpers\Html;

$cssSide = " 
.sidebar-header {
    background-color: #f3acac;
}

.sidebar-title {
    color:#fff;
}

.sidebar-card {
    box-shadow: 0 16px 38px -12px rgb(255 255 255 / 56%), 0 4px 25px 0 rgb(255 255 255 / 12%), 0 8px 10px -5px rgb(43 43 43 / 20%);
    border: none;
    margin-bottom: 30px;
}";

$this->registerCss($cssSide);
?>
<div class="card sidebar-card">
    <article class="card-group-item">
        <header class="card-header sidebar-header"><h6 class="title sidebar-title">Menu </h6></header>
        <div class="filter-content">
            <div class="list-group list-group-flush">
                <?php
                echo Html::a('Transaksi', ['index'], ['class' => 'list-group-item']);
                echo Html::a('Konfirmasi Bayar', ['verifypayment'], ['class' => 'list-group-item']);
                ?>
            </div>  <!-- list-group .// -->
        </div>
    </article> <!-- card-group-item.// -->
    <article class="card-group-item">
        <header class="card-header sidebar-header"><h6 class="title sidebar-title">Pengaturan</h6></header>
        <div class="filter-content">
            <div class="list-group list-group-flush">
                <?php
                echo Html::a('Profile', ['profile'], ['class' => 'list-group-item']);
                echo Html::a('Alamat', ['address'], ['class' => 'list-group-item']);
                ?>
            </div>  <!-- list-group .// -->
        </div>
    </article> <!-- card-group-item.// -->
</div> <!-- card.// -->