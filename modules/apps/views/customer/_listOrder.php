<?php
/* @var $this View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model LoginForm */

use app\helpers\OrderUtils;
use app\helpers\ProductHelper;
use app\helpers\Url;
use app\models\LoginForm;
use richardfan\widget\JSRegister;
use yii\helpers\Html;
use yii\web\View;

foreach ($dataOrder as $perData) {
    ?>
    <div class="bd-callout bd-callout-qyara">
        <div class="row">
            <div class="col-md-4 border-right">
                <p><small>Invoice : </small></p>
                <p id="conveying-meaning-to-assistive-technologies"><?= $perData['invoicekey'] ?></p>
            </div>
            <div class="col-md-4 border-right">
                <p><small>Status : </small></p>
                <p><?= OrderUtils::labelStatusOrder($perData['status'], true) ?></p>
            </div>
            <div class="col-md-4">
                <p><small>Total Belanja</small></p>
                <h6><?= $perData['orderprice'] ? ProductHelper::idrPrice($perData['orderprice']) : ProductHelper::idrPrice(($perData['totalprice'] + $perData['cost'])) ?></h6>
            </div>
        </div>
        <hr>

        <?php
        if (isset($perData['orderdetail'][0])) {
            ?>
            <div class="row">
                <div class="col-md-6 border-right">
                    <a href="">
                        <img class="card-img float-left" src="<?= Url::getBaseImg($perData['orderdetail'][0]['img'], $perData['orderdetail'][0]['dateimg']) ?>" alt="image cap" style="width: 50px;margin-right: 15px;">
                        <h6 id="conveying-meaning-to-assistive-technologies" style="margin-bottom: 0px;"><?= $perData['orderdetail'][0]['namevarian'] ?></h6>
                        <p>
                            <b><?= ProductHelper::idrPrice($perData['orderdetail'][0]['price']) ?></b> 
                            <?php
                            echo $perData['orderdetail'][0]['qty'] . ' Produk (' . OrderUtils::satuanKg($perData['orderdetail'][0]['finalweight']) . ')';
                            if ($perData['orderdetail'][0]['percent']) {
                                echo '&nbsp;<span class="badge badge-primary">' . $perData['orderdetail'][0]['percent'] . '%</span>
                                     &nbsp;<span style="text-decoration: line-through;color: #97a09d;">' . ProductHelper::idrPrice($perData['orderdetail'][0]['oriprice']) . '</span>';
                            }
                            ?>
                        </p>
                    </a>
                </div>
                <div class="col-md-6">
                    <div class="float-left">
                        <p><small>Total Harga Produk</small></p>
                        <h6><?= ProductHelper::idrPrice($perData['orderdetail'][0]['finalprice']) ?></h6>
                    </div>
                    <?= Html::a('Beli Lagi', ['p/v', 'id' => $perData['orderdetail'][0]['idproduct']], ['class' => 'btn btn-sm btn-success float-right']); ?>
                </div>
            </div>
            <?php
        }

        if ($perData['totaldetail'] > 1) {
            $numOther = $perData['totaldetail'] - 1;
            ?>

            <hr>
            <div class="row">
                <div class="col-md-12 text-center">
                    <p><small><a href="#" data-toggle="collapse" data-target="#other-<?= $perData['idorder'] ?>">Lihat <?= $numOther ?> Produk Lainnya</a></small></p>
                </div>
            </div>

            <?php
            foreach ($perData['orderdetail'] as $key => $perDet) {
                if ($key > 0) {
                    ?>
                    <div id="other-<?= $perData['idorder'] ?>" class="collapse">
                        <hr><div class="row">
                            <div class="col-md-6 border-right">
                                <a href="">
                                    <img class="card-img float-left" src="<?= Url::getBaseImg($perDet['img'], $perDet['dateimg']) ?>" alt="image cap" style="width: 50px;margin-right: 15px;">
                                    <h6 id="conveying-meaning-to-assistive-technologies" style="margin-bottom: 0px;"><?= $perDet['namevarian'] ?></h6>
                                    <p>
                                        <b><?= ProductHelper::idrPrice($perDet['price']) ?></b> 
                                        <?php
                                        echo $perDet['qty'] . ' Produk (' . OrderUtils::satuanKg($perDet['finalweight']) . ')';
                                        if ($perDet['percent']) {
                                            echo '&nbsp;<span class="badge badge-primary">' . $perDet['percent'] . '%</span>
                                                  &nbsp;<span style="text-decoration: line-through;color: #97a09d;">' . ProductHelper::idrPrice($perDet['oriprice']) . '</span>';
                                        }
                                        ?>
                                    </p>
                                </a>
                            </div>
                            <div class="col-md-6">
                                <div class="float-left">
                                    <p><small>Total Harga Produk</small></p>
                                    <h6><?= ProductHelper::idrPrice($perDet['finalprice']) ?></h6>
                                </div>
                                <?= Html::a('Beli Lagi', ['p/v', 'id' => $perDet['idproduct']], ['class' => 'btn btn-sm btn-success float-right']); ?>
                            </div>
                        </div>
                    </div>

                    <?php
                }
            }
        }
        ?>

        <hr>
        <div class="row">
            <div class="col-md-6">
                <p id="conveying-meaning-to-assistive-technologies"><?= $perData['orderdate'] ?></p>
            </div>
            <div class="col-md-6">
                <?= Html::a('Detail Order', '#', ['url' => Url::to(['customer/ajaxdetailorder', 'id' => $perData['idorder']], true), 'class' => 'btn btn-sm btn-primary float-right btn-detail-order', 'style' => 'margin-left:3px;']); ?>
            </div>
        </div>
    </div>
<?php } ?>

<?php JSRegister::begin(); ?>
<script>
    $('.btn-detail-order').click(function (e) {
        e.preventDefault();
        $('#modal-content').html('Mohon tunggu ...');
        $('.modal-title').text('');
        $('#modal-form').modal('show');
        ajaxTotalQtyBag();

        $.ajax({
            url: $(this).attr('url'),
            method: 'post',
            dataType: 'html',
            data: datas,
            success: function (data) {
                $('#modal-content').html(data);
            },
            error: function (xhr, error) {
                $('#modal-form').modal('hide');
                alert('Gagal, silakan coba kembali.');
            }

        });

    });
</script>
<?php JSRegister::end(); ?>

