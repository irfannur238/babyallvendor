<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->title = 'Detail Order';
?>
<div class="site-login">

    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb__links">
                        <a href="./index.html"><i class="fa fa-home"></i> Home</a>
                        <span>Detail Order</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="checkout spad" style="padding-top: 40px;">
        <div class="container">
            <?php
            echo $this->render('_detailOrder', [
                'dataOrder' => $dataOrder,
                'orderLog' => $orderLog,
            ])
            ?>
        </div>
    </section>
</div>
