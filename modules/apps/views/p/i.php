<?php

use app\helpers\ProductHelper;
use app\helpers\Url;
use app\models\User;
use yii\bootstrap4\LinkPager;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */

$this->title = User::getStoreName();
?>

<div class="site-index">
    <div class="original-image" style="object-fit: cover;">
        <img src="<?= Url::getImg('banner5.png') ?>"> 
    </div>
    <!-- Categories Section Begin -->
<!--    <section class="categories">
        <div class="container-fluid">

            <div class="row">
                                <div class="col-lg-12 p-0">
                                    <div class="categories__item categories__large__item set-bg zoom"
                                         data-setbg="<?= Url::getImg('banner5.png') ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-12 p-0">
                                            <div class="categories__item set-bg" data-setbg="<?= Url::getImg('category-2.jpg') ?>">
                                                <div class="categories__text">
                                                    <h4>Men’s fashion</h4>
                                                    <p>358 items</p>
                                                    <a href="#">Shop now</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 p-0">
                                            <div class="categories__item set-bg" data-setbg="<?= Url::getImg('category-4.jpg') ?>">
                                                <div class="categories__text">
                                                    <h4>Cosmetics</h4>
                                                    <p>159 items</p>
                                                    <a href="#">Shop now</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
            </div>
        </div>
    </section>-->
    <!-- Categories Section End -->

    <!-- Product Section Begin -->
    <section class="product spad" style="margin-top: 50px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <div class="section-title">
                        <h4>Kategori</h4>
                    </div>
                </div>
            </div>
            <div class="row property__gallery">
                <?php foreach ($categories as $perCat) { ?>
                    <div class="col-lg-3 col-6 col-md-4">
                        <a href="<?= Url::base() . '/apps/p/c?id=' . $perCat->idcategory ?>"><img class="rounded-circle z-depth-2" alt="100x100" src="<?= Url::getBaseImg($perCat->img, $perCat->datecreated) ?>" data-holder-rendered="true">
                            <p><h5 class="text-center"><?= $perCat->name ?></h5></p>
                        </a>
                    </div>
                <?php } ?>
            </div>
            <hr><br>
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <div class="section-title">
                        <h4>Produk Baru</h4>
                    </div>
                </div>

            </div>
            <div class="row property__gallery">
                <?php
                foreach ($models as $model) {
                    $photo = isset($model->img) ? $model->img : '';
                    ?>
                    <div class="col-lg-3 col-6 col-md-4">
                        <div class="product__item sale">
                            <div class="product__item__pic set-bg" data-setbg="<?= Url::getBaseImg($photo, $model->imgcreated) ?>">
                                <?php echo ($model->discount ? '<div class="label">Sale ' . $model->discount . ' %</div>' : null) ?>
                                <ul class="product__hover">
                                    <li><a href="<?= Url::getBaseImg($photo, $model->imgcreated) ?>" class="image-popup"><span class="arrow_expand"></span></a></li>
    <!--                                    <li><a href="#"><span class="icon_heart_alt"></span></a></li>-->
                                    <li><?= Html::a('<span class="icon_bag_alt"></span>', ['v', 'id' => Url::encrypt($model->idproduct)]); ?></li>
                                </ul>
                            </div>
                            <div class="product__item__text">
                                <h6><?= Html::a($model->productname, ['v', 'id' => Url::encrypt($model->idproduct)]) ?></h6>
                                <!--                                <div class="rating">
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                </div>-->
    <!--                                <div class="product__price"><?= $model->price ?></div>-->
                                <?php
                                $disc = $model->discount ? ' <span>' . ProductHelper::idrPrice($model->amount) . '</span>' : '';
                                echo Html::a($model->price . $disc, ['v', 'id' => Url::encrypt($model->idproduct)], ['class' => 'product__price'])
                                ?>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                echo '<div class="col-lg-12 text-center">';
                echo LinkPager::widget([
                    'pagination' => $pagination,
                ]);
                echo '</div>';
                ?>
            </div>

        </div>
    </section>
    <!-- Product Section End -->

    <!-- Banner Section Begin -->
<!--    <section class="banner set-bg" data-setbg="img/banner/banner-1.jpg">
        <div class="container">
            <div class="row">
                <div class="col-xl-7 col-lg-8 m-auto">
                    <div class="banner__slider owl-carousel">
                        <div class="banner__item">
                            <div class="banner__text">
                                <span>The Chloe Collection</span>
                                <h1>The Project Jacket</h1>
                                <a href="#">Shop now</a>
                            </div>
                        </div>
                        <div class="banner__item">
                            <div class="banner__text">
                                <span>The Chloe Collection</span>
                                <h1>The Project Jacket</h1>
                                <a href="#">Shop now</a>
                            </div>
                        </div>
                        <div class="banner__item">
                            <div class="banner__text">
                                <span>The Chloe Collection</span>
                                <h1>The Project Jacket</h1>
                                <a href="#">Shop now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>-->
    <!-- Banner Section End -->



    <!-- Discount Section Begin -->
<!--    <section class="discount">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 p-0">
                    <div class="discount__pic">
                        <img src="img/discount.jpg" alt="">
                    </div>
                </div>
                <div class="col-lg-6 p-0">
                    <div class="discount__text">
                        <div class="discount__text__title">
                            <span>Discount</span>
                            <h2>Summer 2019</h2>
                            <h5><span>Sale</span> 50%</h5>
                        </div>
                        <div class="discount__countdown" id="countdown-time">
                            <div class="countdown__item">
                                <span>22</span>
                                <p>Days</p>
                            </div>
                            <div class="countdown__item">
                                <span>18</span>
                                <p>Hour</p>
                            </div>
                            <div class="countdown__item">
                                <span>46</span>
                                <p>Min</p>
                            </div>
                            <div class="countdown__item">
                                <span>05</span>
                                <p>Sec</p>
                            </div>
                        </div>
                        <a href="#">Shop now</a>
                    </div>
                </div>
            </div>
        </div>
    </section>-->
    <!-- Discount Section End -->

    <!-- Services Section Begin 
    <section class="services spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="services__item">
                        <i class="fa fa-money"></i>
                        <h6>Cashback</h6>
                        <p>For all oder over $99</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="services__item">
                        <i class="fa fa-support"></i>
                        <h6>Online Support 24/7</h6>
                        <p>Dedicated support</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="services__item">
                        <i class="fa fa-headphones"></i>
                        <h6>Payment Secure</h6>
                        <p>100% secure payment</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="services__item">
                        <i class="fa fa-money"></i>
                        <h6>Money Back Guarantee</h6>
                        <p>If good have Problems</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Services Section End -->

    <!-- Instagram Begin -->
    <!--    <div class="instagram">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-2 col-md-4 col-sm-4 p-0">
                        <div class="instagram__item set-bg" data-setbg="img/instagram/insta-1.jpg">
                            <div class="instagram__text">
                                <i class="fa fa-instagram"></i>
                                <a href="#">@ ashion_shop</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-4 p-0">
                        <div class="instagram__item set-bg" data-setbg="img/instagram/insta-2.jpg">
                            <div class="instagram__text">
                                <i class="fa fa-instagram"></i>
                                <a href="#">@ ashion_shop</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-4 p-0">
                        <div class="instagram__item set-bg" data-setbg="img/instagram/insta-3.jpg">
                            <div class="instagram__text">
                                <i class="fa fa-instagram"></i>
                                <a href="#">@ ashion_shop</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-4 p-0">
                        <div class="instagram__item set-bg" data-setbg="img/instagram/insta-4.jpg">
                            <div class="instagram__text">
                                <i class="fa fa-instagram"></i>
                                <a href="#">@ ashion_shop</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-4 p-0">
                        <div class="instagram__item set-bg" data-setbg="img/instagram/insta-5.jpg">
                            <div class="instagram__text">
                                <i class="fa fa-instagram"></i>
                                <a href="#">@ ashion_shop</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-4 p-0">
                        <div class="instagram__item set-bg" data-setbg="img/instagram/insta-6.jpg">
                            <div class="instagram__text">
                                <i class="fa fa-instagram"></i>
                                <a href="#">@ ashion_shop</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
    <!-- Instagram End -->
</div>
