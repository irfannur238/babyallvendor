<?php

use app\helpers\Dlist;
use app\helpers\ProductHelper;
use app\helpers\Url;
use richardfan\widget\JSRegister;
use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->productname;
$getPrice = ProductHelper::getPrice($varian);
//echo date('Y-m-d H:i');
?>

<!-- Breadcrumb Begin -->
<div class="breadcrumb-option">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb__links">
                    <a href="<?= Url::base() . '/apps' ?>"><i class="fa fa-home"></i> Beranda</a>
                    <a href="#">Produk </a>
                    <span><?= $this->title ?></span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Breadcrumb End -->

<!-- Product Details Section Begin -->
<section class="product-details spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="container-fluid px-sm-1 py-5 mx-auto">
                    <div class="row justify-content-center">
                        <div class="d-flex">
                            <div class="card">
                                <div class="d-flex flex-column thumbnails">
                                    <?php
                                    foreach ($photos as $k => $value) {
                                        $i = $k + 1;
                                        $act = '';
                                        if ($i == 1) {
                                            $act = 'active';
                                        }
                                        echo '<div id="f' . $i . '" class="tb tb-' . $act . '"> <img class="thumbnail-img fit-image" src="' . Url::getBaseImg($value['img'], $value['datecreated']) . '"> </div>';
                                    }
                                    ?>
                                </div>
                                <?php
                                foreach ($photos as $k => $value) {
                                    $j = $k + 1;
                                    $active = $j == 1 ? 'active' : '';
                                    echo '<fieldset id="f' . $j . '1" class="' . $active . '">
                                    <div class="product-pic"> <img class="pic0" src="' . Url::getBaseImg($value['img'], $value['datecreated']) . '"> </div>
                                    </fieldset>';
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="product__details__text">
                    <h4><?= $model->productname ?></h4> 
                    <h3><span>Brand: <?= $model->sku ?></span></h3>
                    <!--                    <div class="rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <span>( 138 reviews )</span>
                                        </div>-->

                    <div class="ajxprice">
                        <div class="product__details__price"><?= $getPrice['formatPrice'] ?> <?php echo ($model->discount ? '<span>' . ProductHelper::idrPrice($getPrice['higher']) . '</span><span><div class="label">' . $model->discount . ' % OFF</div></span>' : null) ?></div>
                        <span class="qtystock">Stok terserdia: <?= $getPrice['totalQty'] ?></span> <?= Html::hiddenInput('qtystock', $getPrice['totalQty'], ['id' => 'hi-qty-stock']); ?>
                    </div>

                    <div class="product__details__widget">
                        <ul>
                            <li class="li-varian">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <span style="margin-bottom: 10px;">Pilih Varian:</span>
                                        <?php
                                        $options = [
                                            'class' => 'form-control',
                                            'id' => 'variansel',
                                            'required' => true,
                                        ];
                                        $options = array_merge($options, ['prompt' => '--Pilih Varian--']);
                                        echo Html::dropDownList('variansel', null, Dlist::dListVarian($model->idproduct), $options)
                                        ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <p class="alert-varian" style="margin: 30px 0px 0px 1px;color:red;"></p>
                                    </div>
                                </div>
                            </li>
                            <li class="li-size">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <span style="margin-bottom: 10px;">Pilih Ukuran:</span>
                                        <?=
                                        Html::dropDownList('size', null, Dlist::dListVarian($model->idproduct, true), [
                                            'id' => 'sizesel',
                                            'class' => 'form-control',
                                            'prompt' => '--Pilih Ukuran--',
                                        ])
                                        ?>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <span style="margin-bottom: 10px;">Jumlah:</span>
                                        <?php
                                        echo Html::textInput('qtyinput', 1, ['type' => 'number', 'id' => 'qty-input', 'min' => 1, 'max' => 999, 'class' => 'form-control'])
                                        ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <p class="alert-qty" style="margin: 30px 0px 0px 1px;color:red;"></p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div><hr>
                    <div class="product__details__button">
                        <?=
                        Html::button('<span class="icon_bag_alt"></span> Beli', [
                            'class' => 'cart-btn',
                            'id' => 'btn-addtocart',
                            'style' => 'border:none'
                        ])
                        ?>
                        <ul>
                            <li><a href="#"><span class="icon_heart_alt"></span></a></li>
                            <li><p class="alert-btn" style="margin-left: 10px;color: #111111;"></p></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="product__details__tab">
                    <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 20px;">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab"><b>Deskripsi</b></a>
                        </li>
                        <!--                        <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab">Cara Order</a>
                                                </li>-->
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tabs-1" role="tabpanel">
<!--                            <h6>Deskripsi</h6>-->
                            <?= $model->description ?><br>
                            <h6>Spesifikasi</h6>
                            <?=
                            DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    [
                                        'label' => 'Berat',
                                        'attribute' => 'weight',
                                        'format' => 'html',
                                        'value' => function ($model) {
                                            return $model->weight . ' gram';
                                        }
                                    ],
                                    [
                                        'label' => 'Lebar',
                                        'attribute' => 'width',
                                        'format' => 'html',
                                        'value' => function ($model) {
                                            return $model->width . ' cm';
                                        }
                                    ],
                                    [
                                        'label' => 'Tinggi',
                                        'attribute' => 'height',
                                        'format' => 'html',
                                        'value' => function ($model) {
                                            return $model->height . ' cm';
                                        }
                                    ],
                                ],
                            ])
                            ?>
                        </div>
                        <div class="tab-pane" id="tabs-2" role="tabpanel">
                            <h6>Cara Order</h6>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
                                dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,
                                nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium
                                quis, sem.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="related__title">
                    <h5>Produk Terkait</h5>
                </div>
            </div>
            <?php
            foreach ($related as $val) {
                $photo = isset($val->img) ? $val->img : '';
                ?>
                <div class="col-lg-3 col-6 col-md-4">
                    <div class="product__item sale">
                        <div class="product__item__pic set-bg" data-setbg="<?= Url::getBaseImg($photo, $val->imgcreated) ?>">
                            <?php echo ($val->discount ? '<div class="label">Sale ' . $model->discount . ' %</div>' : null) ?>
                            <ul class="product__hover">
                                <li><a href="<?= Url::getBaseImg($photo, $val->imgcreated) ?>" class="image-popup"><span class="arrow_expand"></span></a></li>
    <!--                                    <li><a href="#"><span class="icon_heart_alt"></span></a></li>-->
                                <li><?= Html::a('<span class="icon_bag_alt"></span>', ['v', 'id' => Url::encrypt($val->idproduct)]); ?></li>
                            </ul>
                        </div>
                        <div class="product__item__text">
                            <h6><a href="#"><?= Html::a($val->productname, ['v', 'id' => Url::encrypt($val->idproduct)]) ?></a></h6>
                            <!--                                <div class="rating">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                            </div>-->
                            <?php
                            $disc = $val->discount ? ' <span>' . ProductHelper::idrPrice($val->amount) . '</span>' : '';
                            echo Html::a($val->price . $disc, ['v', 'id' => Url::encrypt($val->idproduct)], ['class' => 'product__price']);
                            ?>
                        </div>
                    </div>
                </div>
            <?php }
            ?>
        </div>
    </div>
</section>
<!-- Product Details Section End -->

<?php JSRegister::begin() ?>
<script>
    $(document).ready(function (e) {
        var _csrf = '<?= Yii::$app->request->csrfToken ?>';
        var btn_addtocart = document.getElementById("btn-addtocart");

        $('#variansel').on('change', function (e) {
            e.preventDefault();
            var idproduct = '<?= $model->idproduct ?>';
            var varian = this.value;
            var size = document.getElementById('sizesel').value;
            var datas = {idproduct, varian, size, _csrf};

            $(".alert-varian").text('');
            $('.alert-qty').text('');
            $('#qty-input').val(1);

            if (idproduct != '' && varian != '') {
                ajaxGetVarian(datas);
                ajaxDepsize(datas, idproduct, varian);
                ajaxGetImgSel(datas);
            }
        });

        function ajaxGetVarian(datas) {
            $.ajax({
                url: "<?= Url::to(['api/ajax_varian']) ?>",
                type: "POST",
                dataType: 'html',
                data: datas,
                success: function (data) {
                    $('.ajxprice').html(data);
                    var qty_stock = issetNullValue(document.getElementById('hi-qty-stock'));
                    if (qty_stock < 1) {
                        disableBtn(btn_addtocart);
                        $(".alert-btn").text('Produk lagi kosong.');
                    } else {
                        enableBtn(btn_addtocart);
                        $(".alert-btn").text('');
                    }
                },
                error: function (xhr, error) {
                    alert('Gagal, silakan coba kembali.');
                }
            });
        }

        function ajaxGetImgSel(datas) {
            $.ajax({
                url: "<?= Url::to(['api/getimgsel']) ?>",
                type: "POST",
                dataType: 'html',
                data: datas,
                success: function (data) {
                    if (data != '') {
                        $("fieldset.active").html(data);
                    }
                },
                error: function (xhr, error) {
                    alert('Gagal, silakan coba kembali.');
                }
            });
        }

        function ajaxDepsize(datas, idproduct, varian) {
            $.ajax({
                url: "<?= Url::to(['api/depsize']) ?>",
                type: "POST",
                dataType: 'html',
                data: datas,
                success: function (data) {
                    $("select#sizesel").html(data);
                    var size = document.getElementById('sizesel').value;
                    var datas = {idproduct, varian, size, _csrf};

                    $('#qty-input').val(1);
                    $('.alert-qty').text('');

                    ajaxGetVarian(datas);
                    if (data == "") {
                        $('.li-size').slideUp();
                    } else {
                        $('.li-size').slideDown();
                    }
                },
                error: function (xhr, error) {
                    alert('Gagal, silakan coba kembali.');
                }
            });
        }

        $('#sizesel').on('change', function (e) {
            e.preventDefault();
            var idproduct = '<?= $model->idproduct ?>';
            var varian = document.getElementById('variansel').value;
            var size = this.value;
            var datas = {idproduct, varian, size, _csrf};

            if (idproduct != '' && varian != '') {
                ajaxGetVarian(datas);
            }
        });

        $('#qty-input').on('change', function (e) {
            e.preventDefault();
            var idproduct = '<?= $model->idproduct ?>';
            var varian = document.getElementById('variansel').value;
            var size = document.getElementById('sizesel').value;
            var qty = this.value;
            var datas = {idproduct, varian, size, _csrf};

            if (idproduct != '' && varian != '') {
                enableBtn(btn_addtocart);
                ajaxCheckQty(datas, qty);
            } else {
                disableBtn(btn_addtocart);
                $(".alert-varian").text('Varian harus dipilih.');
            }
        });

        $('#qty-input').on('click', function (e) {
            //disableBtn(btn_addtocart);
        });

        function ajaxCheckQty(datas, qty) {
            $.ajax({
                url: "<?= Url::to(['api/checkqty']) ?>",
                type: "POST",
                dataType: 'html',
                data: datas,
                success: function (data) {
                    var stockQty = parseInt(data);
                    if (parseInt(qty) > stockQty) {
                        $('#qty-input').val(stockQty);
                        $('.alert-qty').text('Stok tinggal ' + stockQty);
                        enableBtn(btn_addtocart);
                    } else {
                        $('.alert-qty').text('');
                        enableBtn(btn_addtocart);
                    }
                },
                error: function (xhr, error) {
                    alert('Gagal, silakan coba kembali.');
                }
            });
        }

        $('#btn-addtocart').on('click', function (e) {
            e.preventDefault();
            var idproduct = '<?= $model->idproduct ?>';
            var varian = document.getElementById('variansel').value;
            var size = document.getElementById('sizesel').value;
            var qty = document.getElementById('qty-input').value;
            var datas = {idproduct, varian, size, qty, _csrf};
            if (idproduct != '' && varian != '') {
                enableBtn(btn_addtocart);
                ajaxAddToCart(datas);
            } else {
                disableBtn(btn_addtocart);
                $(".alert-varian").text('Varian harus dipilih.');
            }

        });

        function ajaxAddToCart(datas) {
            $('#modal-content').html('Mohon tunggu ...');
            $.ajax({
                url: "<?= Url::to(['api/addtocart']) ?>",
                type: "POST",
                dataType: 'html',
                data: datas,
                success: function (data) {
                    $('#modal-form').modal('show');
                    $('#modal-content').html(data);
                    $('.modal-title').text('Berhasil Ditambahkan Keranjang');
                    $('#modal-form-label').hide();
                    ajaxTotalQtyBag();
                },
                error: function (xhr, error) {
                    alert('Gagal, silakan coba kembali.');
                }
            });
        }

        function disableBtn(btn_element) {
            btn_element.disabled = true;
            btn_element.style.background = '#ababab';
        }

        function enableBtn(btn_element, color) {
            var fcolor,
                    pcolor = color;
            if (pcolor != null) {
                fcolor = color;
            } else {
                fcolor = '#f3acac';
            }

            btn_element.disabled = false;
            btn_element.style.background = fcolor;
        }

        function issetNullValue(elementid) {
            var res,
                    element = elementid;
            if (element != null) {
                res = element.value;
            } else {
                res = null;
            }
            return res;
        }

        $(".tb").hover(function () {
            $(".tb").removeClass("tb-active");
            $(this).addClass("tb-active");

            current_fs = $(".active");

            next_fs = $(this).attr('id');
            next_fs = "#" + next_fs + "1";

            $("fieldset").removeClass("active");
            $(next_fs).addClass("active");

            current_fs.animate({}, {
                step: function () {
                    current_fs.css({
                        'display': 'none',
                        'position': 'relative'
                    });
                    next_fs.css({
                        'display': 'block'
                    });
                }
            });
        });
    });
</script>
<?php
JSRegister::end();
$csss = "
    body {
    overflow-x: hidden
}

.d-flex > .card {
    margin: auto;
    padding: 10px;
    margin-top: -50px;
    margin-bottom: 50px;
    overflow-x: auto;
}

fieldset.active {
    display: block !important
}

fieldset {
    display: none
}

.pic0 {
    width: 400px;
    height: 500px;
    margin-left: 75px;
    margin-right: auto;
    display: block
}

.product-pic {
    padding-left: auto;
    padding-right: auto;
    width: 100%
}

.thumbnails {
    position: absolute
}

.fit-image {
    width: 100%;
    object-fit: cover
}

.tb {
    width: 62px;
    height: 62px;
    border: 1px solid grey;
    margin: 2px;
    opacity: 0.4;
    cursor: pointer
}

.tb-active {
    opacity: 1
}

.thumbnail-img {
    width: 60px;
    height: 60px
}

@media screen and (max-width: 768px) {
    .pic0 {
        width: 250px;
        height: 350px
    }
}
        ";
$this->registerCss($csss);
?>

