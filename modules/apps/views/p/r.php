<?php

use app\helpers\ProductHelper;
use app\helpers\Url;
use app\models\User;
use yii\bootstrap4\LinkPager;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */

$this->title = User::getStoreName();
?>

<div class="site-index">
    <!-- Product Section Begin -->
    <section class="product spad">
        <div class="container">
            <div class="row" style="margin-top: 120px;">
                <div class="col-lg-4 col-md-4">
                    <div class="section-title">
                        <h4>Hasil Pencarian </h4>
                    </div>
                </div>
            </div>
            <div class="row property__gallery">
                <?php
                foreach ($models as $model) {
                    $photo = isset($model['img']) ? $model['img'] : '';
                    ?>
                    <div class="col-lg-3 col-6 col-md-4">
                        <div class="product__item sale">
                            <div class="product__item__pic set-bg" data-setbg="<?= Url::getBaseImg($photo, $model['imgcreated']) ?>">
                                <?php echo ($model['discount'] ? '<div class="label">Sale ' . $model['discount'] . ' %</div>' : null) ?>
                                <ul class="product__hover">
                                    <li><a href="<?= Url::getBaseImg($photo, $model['imgcreated']) ?>" class="image-popup"><span class="arrow_expand"></span></a></li>
    <!--                                    <li><a href="#"><span class="icon_heart_alt"></span></a></li>-->
                                    <li><?= Html::a('<span class="icon_bag_alt"></span>', ['v', 'id' => Url::encrypt($model['idproduct'])]); ?></li>
                                </ul>
                            </div>
                            <div class="product__item__text">
                                <h6><?= Html::a($model['productname'], ['v', 'id' => Url::encrypt($model['idproduct'])]) ?></h6>
                                <!--                                <div class="rating">
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                </div>-->
                                <?= $disc = $model['discount'] ? ' <span>' . ProductHelper::idrPrice($model['amount']) . '</span>' : ''; ?>
                                <?= Html::a($model['price'], ['v', 'id' => Url::encrypt($model['idproduct'])], ['class' => 'product__price']) ?>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                echo '<div class="col-lg-12 text-center">';
                echo LinkPager::widget([
                    'pagination' => $pagination,
                ]);
                echo '</div>';
                ?>

            </div>
        </div>
    </section>

</div>
