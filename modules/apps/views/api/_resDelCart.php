<?php

use app\helpers\ProductHelper;
use app\helpers\Url;
use yii\helpers\Html;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="container">
    <?php if ($dataCart) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div id="loading-total-table" style="display: none">
                    <div class="d-flex justify-content-center">
                        <div class="spinner-border" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                    </div>
                </div>
                <div id="table-cart-item" style="display:block">
                    <div class="shop__cart__table">
                        <table>
                            <thead>
                                <tr>
                                    <th>Produk</th>
                                    <th>Harga</th>
                                    <th>Jumlah</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody class="form-wrapper">
                                <?php
                                foreach ($dataCart as $i => $row) {
                                    if ($row['outofstock'] == app\helpers\OrderUtils::OUTOFSTOCK) {
                                        $csTr = 'style="background-color: #efefef;"';
                                        $csImg = 'style="opacity: 40%;"';
                                        $csName = 'style="color: darkgrey;"';
                                        $cPrc = 'style="color: darkgrey;"';
                                        $dis = true;
                                        $txAlrt = '<p style="color: #ca1515;">Stok Habis</p>';
                                    } else {
                                        $csTr = $csImg = $csName = $cPrc = $txAlrt = '';
                                        $dis = false;
                                    }
                                    ?>
                                    <tr class="tr" id="tr-<?= $row['idvarian'] ?>" <?= $csTr ?>">
                                        <td class="cart__product__item">
                                            <img src="<?= Url::getBaseImg($row['img'], $row['date']) ?>"  alt="" class="img-fcart" <?= $csImg ?>>
                                            <div class="cart__product__item__title">
                                                <?= Html::a('<p ' . $csName . '><b>' . $row['name'] . '</b></p>', ['p/v', 'id' => $row['idproduct']]) ?>
                                            </div>
                                            <?= $txAlrt ?>
                                        </td>
                                        <td class="cart__price" width="60%" <?= $cPrc ?>>
                                            <?php
                                            echo ProductHelper::idrPrice($row['price']);
                                            echo Html::hiddenInput('idproduct', $row['idproduct'], ['class' => 'hi-idproduct']);
                                            echo Html::hiddenInput('varian', $row['varian'], ['class' => 'hi-varian']);
                                            echo Html::hiddenInput('idvarian', $row['idvarian'], ['class' => 'hi-idvarian']);
                                            echo Html::hiddenInput('size', $row['size'], ['class' => 'hi-size']);
                                            if ($row['percent']) {
                                                echo '<p>
                                                        <span class="badge badge-primary">' . $row['percent'] . '%</span>
                                                        <span style="text-decoration: line-through;color: #97a09d;">' . $row['oriPrice'] . '</span>
                                                        </p>';
                                            }
                                            ?>
                                        </td>
                                        <td class="cart__quantity" width="15%" style="width:100px;">
                                            <?php
                                            echo Html::textInput('qty-item', $row['qty'], ['disabled' => $dis, 'type' => 'number', 'id' => $row['idvarian'], 'min' => 1, 'max' => 999, 'class' => 'input-qty form-control']);
                                            echo Html::hiddenInput('delitem', NULL, ['class' => 'del-me', 'id' => 'del-' . $row['idvarian']]);
                                            ?>
                                            <p class="alert-qty" style="color:red;text-align: center;"></p>
                                        </td>
                                        <td class="cart__close"><span style="margin: 0px 20px 10px 20px;" class="icon_close btn-del-item" id="btn-del-<?= $row['idvarian'] ?>" data-name="<?= $row['name'] ?>" data-idvarian="<?= $row['idvarian'] ?>"></span></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="cart__btn">
                    <?= Html::a('Lanjut Belanja', ['/apps/p'], ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="cart__btn update__btn">
                    <?=
                    Html::a('Hapus Semua', ['delete'], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Apakah anda yakin untuk hapus semua isi keranjang ?',
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="discount__content">
                    <!--                    <h6>Discount codes</h6>
                                        <form action="#">
                                            <input type="text" placeholder="Enter your coupon code">
                                            <button type="submit" class="site-btn">Apply</button>
                                        </form>-->
                </div>
            </div>
            <div class="col-lg-6 offset-lg-6">
                <div class="cart__total__procced">
                    <div id="loading-total" style="display: none">
                        <div class="d-flex justify-content-center">
                            <div class="spinner-border" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                    </div>
                    <div id="item-total" style="display: block">
                        <h6>Keranjang total</h6>
                        <ul>
                            <li>Total Harga (<?= $dataTotal['totalQty'] ?> Barang) <span><?= $dataTotal['totalOriPrice'] ?></span></li>
                            <li>Total Diskon Barang<span> - <?= $dataTotal['totalDiscount'] ?></span></li>
                        </ul>
                        <hr>
                        <ul>
                            <li>Total Harga <span><?= $dataTotal['total'] ?></span></li>
                        </ul>
                        <?= Html::a('Checkout', ['checkout/index'], ['class' => 'primary-btn site-btn']) ?>
                    </div>
                </div>
            </div>
        </div>
    <?php } else { ?>
        <div class="card text-center">
            <div class="card-header">
                Keranjang Belanja
            </div>
            <div class="card-body">
                <h5 class="card-title">Opss Kosong !!!</h5>
                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                <a href="#" class="btn btn-primary">Lanjut Belanja</a>
            </div>
            <div class="card-footer text-muted">
                di Qyara
            </div>
        </div>
    <?php } ?>
</div>

