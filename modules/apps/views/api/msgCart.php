<?php

use app\helpers\Url;
use yii\helpers\Html;
?>

<div class="site-index">
    <!--    <h5 class="text-center">Berhasil Ditambahkan</h5><br>-->
    <div class="card" style="border: 0px;">
        <div class="card-body">
            <img class="card-img" src="<?= Url::getBaseImg($newItem['img'], $newItem['date']) ?>" alt="Card image cap" style="width: 50px;">
            <small class="text-muted">&nbsp;&nbsp;<?= $newItem['name'] ?></small>
            <?= Html::a('Lihat Keranjang', ['/apps/cart'], ['class' => 'btn btn-sm btn-primary float-right']) ?>
        </div>
    </div>
</div>

