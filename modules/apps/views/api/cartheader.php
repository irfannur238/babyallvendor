<?php

use app\helpers\OrderUtils;
use app\helpers\ProductHelper;
use app\helpers\Url;
use yii\helpers\Html;

echo '<div class="site-index">';
if ($data) {
    foreach ($data as $perData) {
        $txAlrt = $perData['outofstock'] == OrderUtils::OUTOFSTOCK ? '<p style="color: #ca1515;">Stok Habis</p>' : '';
        $opct = $perData['outofstock'] == OrderUtils::OUTOFSTOCK ? 'opacity:40%;' : '';
        echo '
        <div class="card-body" style="padding:0px;">
          <table>
            <tr>
                <td width="10%">
                    <img class="card-img" src="' . Url::getBaseImg($perData['img'], $perData['date']) . '" alt="Card image cap" style="width: 50px;' . $opct . '">
                </td>
                <td width="60%">
                    <small class="text-muted">&nbsp;&nbsp;' . (strlen($perData['name']) > 40 ? substr($perData['name'], 0, 40) . ' .....' : $perData['name']) . '</small>
                </td>
                <td width="20%">
                    <small class="text-muted">(' . $perData['qty'] . 'x)</small>
                        ' . $txAlrt . '
                </td>
                <td width="20%">
                    <small class="text-muted">' . ProductHelper::idrPrice($perData['price']) . '</small>
                </td>
            </tr>
          </table>
        </div><hr>';
    }
    echo Html::a('Lihat Sekarang  <span class="badge badge-light">' . OrderUtils::getTotalQtyCart() . '</span>', ['/apps/cart'], ['class' => 'btn btn-sm btn-primary float-right']);
    echo '</div>';
} else {
    echo '<h5 class="text-center">Keranjang Belanja Kosong !!</h5>';
}
