<?php

namespace app\modules\admins\controllers;

use app\components\AdminController;
use app\helpers\Dlist;
use app\helpers\Utils;
use app\models\Store;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class StoresController extends AdminController {

    public function actionMystore() {
        $model = Store::find()->one();
        if (!$model) {
            $model = new Store();
        }

        $provinces = Dlist::dListProvince();
        $cities = Dlist::dListCity($model->province_id);

        if ($model->load(Yii::$app->request->post())) {
            $model->status = Utils::STAT_ACTIVE;
            $model->province_name = (isset($provinces[$model->province_id]) ? $provinces[$model->province_id] : '-');
            $model->city_name = (isset($cities[$model->city_id]) ? $cities[$model->city_id] : '-');

            if ($model->save()) {
                Utils::flashSuccess('Data berhasil disimpan.');
                return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
            } else {
                $msg = json_encode($model->getErrors());
                Utils::flashDanger($msg);
            }
        }

        return $this->render('mystore', [
                    'model' => $model,
                    'provinces' => $provinces,
                    'cities' => $cities,
        ]);
    }

    protected function findModel($id) {
        if (($model = Store::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
