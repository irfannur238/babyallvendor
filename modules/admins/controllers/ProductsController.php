<?php

namespace app\modules\admins\controllers;

use app\components\AdminController;
use app\helpers\ProductHelper;
use app\helpers\Url;
use app\helpers\Utils;
use app\models\Discount;
use app\models\Products;
use app\models\Varianimg;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;

/**
 * ProductsController implements the CRUD actions for Products model.
 */
class ProductsController extends AdminController {

    public function actionIndex() {
        $model = new Products();
        $model->load(Yii::$app->request->queryParams);

        $query = Products::find()
                ->where(['IS', 'isarchived', new \yii\db\Expression('null')])
                ->andFilterWhere(['like', 'UPPER(productname)', strtoupper($model->productname)])
                ->andFilterWhere(['like', 'UPPER(sku)', strtoupper($model->sku)])
                ->andFilterWhere(['like', 'UPPER(price)', strtoupper($model->price)]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('index', [
                    'model' => $model,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Products model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Products model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Products();
        $post = \Yii::$app->request->post();

        if ($model->load(Yii::$app->request->post())) {
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                $model->idproduct = Utils::getID();
                $model->datecreated = date('Y-m-d H:i:s');
                $model->status = ProductHelper::STAT_NONACTIVE;
                $model->sku = 'SKU' . $model->idproduct;
                $model->weight = 100;
                if ($model->save()) {
                    $transaction->commit();
                    return $this->redirect(['optionprice', 'id' => $model->idproduct]);
                } else {
                    Utils::flashDanger(json_encode($model->getErrors()));
                    return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
                }
            } catch (Exception $e) {
                Utils::flashDanger('Gagal disimpan');
                $transaction->rollBack();
            }
            return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    public function actionOptionprice($id) {
        $model = $this->findModel($id);
        if (Yii::$app->request->isPost) {
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                $this->saveOptionprice($model, $msg);
                if (!$msg) {
                    $model->isvarian = ProductHelper::ISVARIAN;
                    if ($model->save()) {
                        $transaction->commit();
                        return $this->redirect(['imgs', 'id' => $model->idproduct]);
                    } else {
                        Utils::flashDanger(json_encode($model->getErrors()));
                        $transaction->rollBack();
                        //return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
                    }
                } else {
                    Utils::flashDanger($msg);
                    //return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
                }
            } catch (Exception $e) {
                Utils::flashDanger('Gagal disimpan');
                $transaction->rollBack();
            }
            return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
        }

        return $this->render('optionprice', [
                    'model' => $model,
        ]);
    }

    protected function saveOptionprice($model, &$msg = null) {
        $flag = true;
        foreach ($_POST['idvarian'] as $i => $idoption) {
            if (empty($idoption)) {
                $modelopt = new Varianimg();
                $modelopt->idvarian = (string) Utils::getID();
            } else {
                $modelopt = Varianimg::findOne($idoption);
            }

            if (isset($_POST['delvarian'][$i]) && $_POST['delvarian'][$i] == 1) {
                $modelopt->delete();
            } else {
                $size = $_POST['size'][$i] == '' ? NULL : $_POST['size'][$i];
                $modelopt->idvarian = (string) $modelopt->idvarian;
                $modelopt->idproduct = (string) $model->idproduct;
                $modelopt->namevarian = strtoupper($_POST['namevarian'][$i]);
                $modelopt->qty = $_POST['qty'][$i];
                $modelopt->price = $_POST['price'][$i];
                $modelopt->size = strtoupper($size);
                $modelopt->uniquekey = ProductHelper::genUniqueKey($modelopt->idproduct, $modelopt->namevarian, $modelopt->size);
                $modelopt->type = ProductHelper::TYPE_VARIAN;
                if (!$modelopt->save()) {
                    $msg = Json::encode($modelopt->errors);
                    $flag = false;
                    break;
                }
            }
        }
    }

    public function actionImgs($id) {
        $model = $this->findModel($id);
        if (Yii::$app->request->isPost) {
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                $this->saveImgs($model, $msg);
                if (!$msg) {
                    $photos = ProductHelper::getVarianProduct($model->idproduct, ProductHelper::TYPE_IMG, ['varianimg.ordering' => SORT_ASC]);
                    $varian = ProductHelper::getVarianProduct($model->idproduct);
                    $getPrice = ProductHelper::getPrice($varian);

                    $model->price = $getPrice['formatPrice'];
                    $model->img = isset($photos[0]['img']) ? $photos[0]['img'] : null;
                    $model->imgcreated = isset($photos[0]['datecreated']) ? $photos[0]['datecreated'] : null;

                    if ($model->save()) {
                        $transaction->commit();
                        return $this->redirect(['specification', 'id' => $model->idproduct]);
                    }
                } else {
                    Utils::flashDanger($msg);
                    return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
                }
            } catch (Exception $e) {
                Utils::flashDanger('Gagal disimpan');
                $transaction->rollBack();
            }
            return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
        }

        return $this->render('imgs', [
                    'model' => $model,
        ]);
    }

    protected function saveImgs($model, &$msg = null) {
        $flag = true;
        $filesPool = $_FILES['img'];
        if (!$filesPool) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        foreach ($_POST['idvarian'] as $i => $idoption) {
            if (empty($idoption)) {
                $modelopt = new Varianimg();
                $modelopt->idvarian = (string) Utils::getID();
                if (isset($_FILES['img']) && !$_FILES['img']['tmp_name'][$i]) {
                    continue;
                }
            } else {
                $modelopt = Varianimg::findOne($idoption);
            }

            if (isset($_POST['delvarian'][$i]) && $_POST['delvarian'][$i] == 1) {
                if ($modelopt->delete()) {
                    $fileTar = Url::getBaseImg($modelopt->img, $modelopt->datecreated, true);
                    if (is_file($fileTar))
                        unlink($fileTar);
                }
            } else {
                $date = date('Y-m-d H:i:s');
                $imgName = 'img-' . $modelopt->idvarian;
                $dir = Url::setupBaseImg($date);
                $tmpImg = $filesPool['tmp_name'][$i];
                if ($modelopt->isNewRecord) {
                    $exeUpload = move_uploaded_file($tmpImg, $dir . $imgName);
                    $modelopt->idvarian = (string) $modelopt->idvarian;
                    $modelopt->idproduct = (string) $model->idproduct;
                    $modelopt->img = $imgName;
                    $modelopt->datecreated = $date;
                    $modelopt->namevarian = 'img';
                    $modelopt->qty = 1;
                    $modelopt->price = 1;
                    $modelopt->type = ProductHelper::TYPE_IMG;
                } else {
                    if ($tmpImg) {
                        $upImgName = 'img-' . Utils::getID();
                        $upDir = Url::getBaseImg($modelopt->img, $modelopt->datecreated, true);
                        if (is_file($upDir))
                            unlink($upDir);
                        $exeUpload = move_uploaded_file($tmpImg, $dir . $upImgName);
                        $modelopt->img = $upImgName;
                        $modelopt->datecreated = $date;
                    }
                }
                $modelopt->parent = $_POST['parent'][$i];
                $modelopt->ordering = $_POST['ordering'][$i];
                if (!$modelopt->save()) {
                    $msg = Json::encode($modelopt->errors);
                    $flag = false;
                    break;
                }
            }
        }
    }

    public function actionSpecification($id) {
        $model = $this->findModel($id);
        $post = \Yii::$app->request->post();

        if ($model->load(Yii::$app->request->post())) {
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                $model->width = $model->width;
                $model->height = $model->height;
                $model->weight = $model->weight;
                if ($model->save()) {
                    $transaction->commit();
                    return $this->redirect(['discount', 'id' => $model->idproduct]);
                }
            } catch (Exception $e) {
                Utils::flashDanger('Gagal disimpan');
                $transaction->rollBack();
            }
            return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
        }

        return $this->render('specification', [
                    'model' => $model,
        ]);
    }

    public function actionDiscount($id) {
        $model = $this->findModel($id);
        if (Yii::$app->request->isPost) {
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                $this->saveDiscount($model, $msg);
                if (!$msg) {
                    $varian = ProductHelper::getVarianProduct($model->idproduct);
                    $getPrice = ProductHelper::getPrice($varian);

                    $model->price = $getPrice['formatPrice'];
                    $model->discount = $getPrice['discount'];
                    $model->amount = (int) $getPrice['higher']; //save max price for disc

                    if ($model->save()) {
                        Utils::flashSuccess('Diskon berhasil disimpan.');
                        $transaction->commit();
                        return $this->redirect(['setting', 'id' => $model->idproduct]);
                    }
                } else {
                    Utils::flashDanger($msg);
                    return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
                }
            } catch (Exception $e) {
                Utils::flashDanger('Gagal disimpan');
                $transaction->rollBack();
            }
            return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
        }

        return $this->render('discount', [
                    'model' => $model,
        ]);
    }

    protected function saveDiscount($model, &$msg = null) {
        $flag = true;
        foreach ($_POST['iddiscount'] as $i => $idoption) {
            if (empty($idoption)) {
                $modelopt = new Discount();
                $modelopt->iddiscount = Utils::getNextID('iddiscount', Discount::tableName());
            } else {
                $modelopt = Discount::findOne($idoption);
            }

            if (isset($_POST['deldiscount'][$i]) && $_POST['deldiscount'][$i] == 1) {
                $modelopt->delete();
            } else {
                if (empty($_POST['percent'][$i])) {
                    continue;
                }
                $startdate = $_POST['startdate'][$i] ? date('Y-m-d H:i:s', strtotime($_POST['startdate'][$i])) : null;
                $enddate = $_POST['enddate'][$i] ? date('Y-m-d H:i:s', strtotime($_POST['enddate'][$i])) : null;
                $modelopt->idproduct = (string) $model->idproduct;
                $modelopt->percent = $_POST['percent'][$i];
                $modelopt->startdate = $startdate;
                $modelopt->enddate = $enddate;
                $modelopt->status = $_POST['status'][$i];
                if (!$modelopt->save()) {
                    $msg = Json::encode($modelopt->errors);
                    $flag = false;
                    break;
                }
            }
        }
    }

    public function actionSetting($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                $model->isfeatured = $model->isfeatured;
                $model->status = $model->status;
                if ($model->save()) {
                    $transaction->commit();
                    Utils::flashSuccess('Produk berhasil di simpan');
                    return $this->redirect(['resume', 'id' => $model->idproduct]);
                }
            } catch (Exception $e) {
                Utils::flashDanger('Gagal disimpan');
                $transaction->rollBack();
            }
            return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
        }

        return $this->render('setting', [
                    'model' => $model,
        ]);
    }

    public function actionResume($id) {
        $model = $this->findModel($id);
        return $this->render('resume', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Products model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $post = \Yii::$app->request->post();

        if ($model->load(Yii::$app->request->post())) {
            $model->datecreated = ($model->oldAttributes['datecreated'] ? $model->oldAttributes['datecreated'] : date('Y-m-d H:i:s'));
            $model->status = ($model->oldAttributes['status'] ? $model->oldAttributes['status'] : ProductHelper::STAT_NONACTIVE);
            $model->sku = ($model->oldAttributes['sku'] ? $model->oldAttributes['sku'] : 'SKU' . $model->idproduct);
            $model->weight = ($model->oldAttributes['weight'] ? $model->oldAttributes['weight'] : 100);
            if ($model->save()) {
                return $this->redirect(['optionprice', 'id' => $model->idproduct]);
            } else {
                Utils::flashDanger(json_encode($model->getErrors()));
                return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
            }
        }
        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Products model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        $model->isarchived = ProductHelper::ISARCHIVED;

        if ($model->save()) {
            Utils::flashSuccess('Produk berhasil diarchive');
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Products model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
