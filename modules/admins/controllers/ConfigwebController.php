<?php

namespace app\modules\admins\controllers;

use app\components\AdminController;
use app\helpers\Configtype;
use app\helpers\Url;
use app\helpers\Utils;
use app\models\Configweb;
use app\models\Configwebs;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * ConfigwebController implements the CRUD actions for Configweb model.
 */
class ConfigwebController extends AdminController {

    /**
     * Lists all Configweb models.
     * @return mixed
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Configwebs::find()->where(['type' => Configtype::FIND_US]),
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Configweb model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Configweb model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Configwebs();

        if ($model->load(Yii::$app->request->post())) {
            $model->datecreated = date('Y-m-d H:i:s');
            $model->type = Configtype::FIND_US;
            if ($model->save()) {
                return $this->redirect(['index']);
            }
        }

        return $this->render('_formFindus', [
                    'model' => $model,
        ]);
    }
    
    public function actionCfindus() {
        $model = new Configwebs();

        if ($model->load(Yii::$app->request->post())) {
            $model->type = Configtype::FIND_US;
            
            $img = UploadedFile::getInstance($model, 'img');
            $name = 'LINK' . Utils::getID() . '.' . $img->extension;
            $dataTime = date('Y-m-d H:i:s');
            $path = Url::setupBaseImg($dataTime);
            $model->datecreated = $dataTime;
            $model->img = $name;

            if ($model->save()) {
                $img->saveAs($path . $name);
                return $this->redirect(['view', 'id' => $model->idconfig]);
            }
//            if ($model->save()) {
//                return $this->redirect(['view', 'id' => $model->idconfig]);
//            }
        }

        return $this->render('_formFindus', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Configweb model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('_formFindus', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Configweb model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Configweb model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Configweb the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Configwebs::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
