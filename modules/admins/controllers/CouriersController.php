<?php

namespace app\modules\admins\controllers;

use app\components\AdminController;
use app\helpers\Url;
use app\helpers\Utils;
use app\models\Couriers;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * CouriersController implements the CRUD actions for Couriers model.
 */
class CouriersController extends AdminController {

    /**
     * Lists all Couriers models.
     * @return mixed
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Couriers::find(),
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Couriers model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Couriers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Couriers();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->idcourier]);
            }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Couriers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $tmpImg = $model->img;
        $uimg = false;
        if ($model->load(Yii::$app->request->post())) {
            if ($_FILES['Couriers']['error']['img'] == 0) {
                $img = UploadedFile::getInstance($model, 'img');
                $name = 'pay' . Utils::getID() . '.' . $img->extension;
                $path = 'img/payment/';
                $model->img = $name;
                $uimg = true;
            } else {
                $model->img = $tmpImg;
            }

            if ($model->save()) {
                if ($uimg) {
                    $img->saveAs($path . $name);
                    $pathImg = Url::getImg($tmpImg, $path, true);
                    if (is_file($pathImg)) {
                        unlink($pathImg);
                    }
                }
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Couriers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Couriers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Couriers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Couriers::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
