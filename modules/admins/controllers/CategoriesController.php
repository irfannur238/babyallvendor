<?php

namespace app\modules\admins\controllers;

use app\helpers\Url;
use app\helpers\Utils;
use app\models\Categories;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * CategoriesController implements the CRUD actions for Categories model.
 */
class CategoriesController extends \app\components\AdminController {

    /**
     * Lists all Categories models.
     * @return mixed
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Categories::find(),
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Categories model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Categories model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Categories();

        if ($model->load(Yii::$app->request->post())) {
            $img = UploadedFile::getInstance($model, 'img');
            $name = 'CAT' . Utils::getID() . '.' . $img->extension;
            $dataTime = date('Y-m-d H:i:s');
            $path = Url::setupBaseImg($dataTime);
            $model->datecreated = $dataTime;
            $model->img = $name;

            if ($model->save()) {
                $img->saveAs($path . $name);
                return $this->redirect(['view', 'id' => $model->idcategory]);
            }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Categories model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $tmpImg = $model->img;
        $tmpDate = $model->datecreated;

        $uimg = false;
        if ($model->load(Yii::$app->request->post())) {
            if ($_FILES['Categories']['error']['img'] == 0) {
                $img = UploadedFile::getInstance($model, 'img');
                $name = 'CAT' . Utils::getID() . '.' . $img->extension;
                $dataTime = date('Y-m-d H:i:s');
                $path = Url::setupBaseImg($dataTime);
                $model->datecreated = $dataTime;
                $model->img = $name;
                $uimg = true;
            } else {
                $model->img = $tmpImg;
            }

            if ($model->save()) {
                if ($uimg) {
                    $img->saveAs($path . $name);
                    $pathImg = Url::getBaseImg($tmpImg, $tmpDate, true);
                    if (is_file($pathImg)) {
                        unlink($pathImg);
                    }
                }
                Utils::flashSuccess('Berhasil update.');
                return $this->redirect(['view', 'id' => $model->idcategory]);
            }
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Categories model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        if ($model->delete()) {
            $pathImg = Url::getBaseImg($model->img, $model->datecreated, true);
            if (is_file($pathImg)) {
                unlink($pathImg);
            }
            Utils::flashSuccess('Berhasil dihapus.');
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Categories model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Categories the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Categories::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
