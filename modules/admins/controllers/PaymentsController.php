<?php

namespace app\modules\admins\controllers;

use app\components\AdminController;
use app\helpers\Url;
use app\helpers\Utils;
use app\models\Payments;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * PaymentsController implements the CRUD actions for Payments model.
 */
class PaymentsController extends AdminController {

    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Payments::find()->where(['idlevel' => 2]),
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Payments model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Payments model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Payments();

        if ($model->load(Yii::$app->request->post())) {
            $model->idpayment = Utils::getID();
            $model->parent = '123124356';
            $model->idlevel = 2;

            $img = UploadedFile::getInstance($model, 'img');
            $name = 'pay' . Utils::getID() . '.' . $img->extension;
            $path = 'img/payment/';
            $model->img = $name;
//            $model->save();
//            echo '<pre>';            print_r($model);die;
            if ($model->save()) {
                $img->saveAs($path . $name);
                return $this->redirect(['view', 'id' => $model->idpayment]);
            }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Payments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $tmpImg = $model->img;
        $uimg = false;
        if ($model->load(Yii::$app->request->post())) {
            if ($_FILES['Payments']['error']['img'] == 0) {
                $img = UploadedFile::getInstance($model, 'img');
                $name = 'pay' . Utils::getID() . '.' . $img->extension;
                $path = 'img/payment/';
                $model->img = $name;
                $uimg = true;
            } else {
                $model->img = $tmpImg;
            }

            if ($model->save()) {
                if ($uimg) {
                    $img->saveAs($path . $name);
                    $pathImg = Url::getImg($tmpImg, $path, true);
                    if (is_file($pathImg)) {
                        unlink($pathImg);
                    }
                }
                return $this->redirect(['view', 'id' => $model->idpayment]);
            }
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Payments model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        if ($model->delete()) {
            $path = 'img/payment/';
            $pathImg = Url::getImg($model->img, $path, true);
            if (is_file($pathImg)) {
                unlink($pathImg);
            }
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Payments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Payments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Payments::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
