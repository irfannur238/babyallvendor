<?php

namespace app\modules\admins\controllers;

use app\components\AdminController;
use app\helpers\DateUtils;
use app\helpers\OrderUtils;
use app\helpers\Utils;
use app\models\Orderlog;
use app\models\Orders;
use Yii;
use yii\data\ArrayDataProvider;
use yii\web\NotFoundHttpException;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class OrdersController extends AdminController {

    public function actionIndex() {
        $this->redirect(['dataorder']);
    }

    public function actionDetail($id) {
        $model = $this->findModel($id);
        if ($model->status == OrderUtils::STAT_ONPROCESS) {
            $model->scenario = 'inputreceiptnumber';
        }

        $dataOrder = OrderUtils::getOrderData(null, null, $id);
        $orderLog = Orderlog::find()->where(['idorder' => $id])->asArray()->all();

        $data = [];
        if ($model->status == OrderUtils::STAT_PAYMENT_VERIFICATION) {
            $data = OrderUtils::dataVerifypayment($id);
        }

        return $this->render('detail', [
                    'model' => $model,
                    'dataOrder' => $dataOrder,
                    'orderLog' => $orderLog,
                    'data' => $data,
        ]);
    }

    public function actionDataorder($status = null) {
        $model = new Orders();
        $model->load(Yii::$app->request->queryParams);
        $model->status = $status;
        $fstatus = $status ? [$status] : null;
        $data = OrderUtils::getOrderData(null, $fstatus, null, $model);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'key' => 'idorder',
            'pagination' => ['pageSize' => 10]
        ]);

        return $this->render('index', [
                    'model' => $model,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionApprove($id) {
        $model = $this->findModel($id);
        $curStatus = $model->status;

        if (Utils::req()->isPost) {
            $model->status = OrderUtils::setStatusApprove($curStatus);
            if (!$model->save()) {
                $msg = json_encode($model->getErrors());
                Utils::flashDanger($msg);
            } else {
                OrderUtils::saveOrderLog($model);
                $labelStatus = OrderUtils::labelStatusOrder($model->status, true);
                Utils::flashSuccess('Pesanan Berhasil di Setujui. Pesanan sekarang berstatus ' . $labelStatus);
            }
        }
        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }

    public function actionReject($id) {
        $model = $this->findModel($id);
        $curStatus = $model->status;

        if (Utils::req()->isPost) {
            $model->status = OrderUtils::setStatusReject($curStatus);

            if (!$model->save()) {
                $msg = json_encode($model->getErrors());
                Utils::flashDanger($msg);
            } else {
                if ($curStatus == OrderUtils::STAT_PAYMENT_VERIFICATION) {
                    $dataVerify = OrderUtils::dataVerifypayment($id);
                    if ($dataVerify) {
                        OrderUtils::archiveVerifypayment($dataVerify['idverify']);
                    }
                }
                OrderUtils::saveOrderLog($model);
                $labelStatus = OrderUtils::labelStatusOrder($model->status, true);
                Utils::flashSuccess('Pesanan Berhasil di Tolak. Pesanan sekarang berstatus ' . $labelStatus);
            }
        }
        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }

    public function actionInputreceiptnumber($id) {
        $model = $this->findModel($id);
        $model->scenario = 'inputreceiptnumber';
        $curStatus = $model->status;

        if ($model->load(Yii::$app->request->post())) {
            $model->receiptnumber = $model->receiptnumber;
            $model->status = OrderUtils::setStatusApprove($curStatus);
            $model->shipdate = DateUtils::datetimeNow();

            if (!$model->save()) {
                $msg = json_encode($model->getErrors());
                Utils::flashDanger($msg);
            } else {
                OrderUtils::saveOrderLog($model);
                OrderUtils::updateReceiptShipper($id, $model);
                $labelStatus = OrderUtils::labelStatusOrder($model->status, true);
                Utils::flashSuccess('Pesanan Berhasil Disetujui. Pesanan sekarang berstatus ' . $labelStatus);
            }
        }
        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }

    public function actionSetcanceled($id) {
        $model = $this->findModel($id);
        if ($model->status == OrderUtils::STAT_WAITING_PAYMENT) {
            $model->status = OrderUtils::STAT_CANCELED;
            if ($model->save()) {
                OrderUtils::returnQtyVarian($id);
                Utils::flashSuccess('Berhasil.');
                OrderUtils::saveOrderLog($model);
                return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
            } else {
                Utils::flashDanger('Gagal.');
            }
        } else {
            Utils::flashDanger('Tidak dapat Rubah');
        }
    }

    protected function findModel($id) {
        if (($model = Orders::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
