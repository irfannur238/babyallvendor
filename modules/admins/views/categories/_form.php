<?php

use app\helpers\Url;
use app\models\Categories;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $model Categories */
/* @var $form ActiveForm */
?>

<div class="categories-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Nama') ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6])->label('Deskripsi') ?>

    <?php
    if (!$model->isNewRecord) {
        echo '<img class="myImg" src="' . Url::getBaseImg($model->img, $model->datecreated) . '" style="max-height:100px">';
    }
    ?>
    <br><br>
    <?= $form->field($model, 'img')->fileInput()->label('Gambar'); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
