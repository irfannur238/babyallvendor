<?php

use app\helpers\Dlist;
use app\models\Products;
use wadeshuler\ckeditor\widgets\CKEditor;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $model Products */

$this->title = 'Tambah Produk';
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-create">

    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link" href="#">General</a>
        </li>
         <li class="nav-item">
            <a class="nav-link active" href="#">Pilihan & Harga</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Spesifikasi</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Pengaturan</a>
        </li>
    </ul></br>

    <div class="products-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'idcategory')->dropDownList(Dlist::dListCategory()) ?>

        <?= $form->field($model, 'productname')->textInput(['maxlength' => true]) ?>
        
        <?= $form->field($model, 'sku')->textInput(['maxlength' => true])->label('SKU') ?>

        <?= $form->field($model, 'isfeatured')->dropDownList(Dlist::dListIsfeatured()) ?>

        <?= $form->field($model, 'status')->dropDownList(Dlist::dListStatusActive()) ?>

        <?= $form->field($model, 'description')->widget(CKEditor::className()) ?>

        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
