<?php

use app\helpers\Dlist;
use app\helpers\ProductHelper;
use app\models\FRMPROBLEMS;
use richardfan\widget\JSRegister;
use yii\bootstrap4\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this View */
/* @var $model FRMPROBLEMS */
/* @var $form ActiveForm */
$this->title = ProductHelper::textProductname($model, 'Diskon');
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="products-create">

    <?php
    echo $this->render('_tab', [
        'model' => $model,
    ]);
    ?>

    <?php $form = ActiveForm::begin(['id' => 'frmproblem']); ?>
    <?php $this->registerCss('.th-isian{font-weight:bold;}'); ?>
    <div class="row">
        <div class="col-md-12">
            <div style="margin-top: 4px;" class="row">
                <div class="col-md-2 th-isian">Persen <code>%</code></div>
                <div class="col-md-2 th-isian">Tanggal Mulai</div>
                <div class="col-md-2 th-isian">Tanggal Selesai</div>
                <div class="col-md-1 th-isian">Status</div>
                <div class="col-md-1 th-isian">Action</div>
            </div>
            <div class="option-wrap">
                <?php
                if (!$model->isNewRecord) {
                    $discount = ProductHelper::getDiscountProduct($model->idproduct);
                    $countDis = count($discount);
                    if ($discount) {
                        foreach ($discount as $perVar) {
                            ?>
                            <div style="margin-top: 4px;border-bottom: 1px solid #e9ecef;padding: 10px 0px 10px 0px;" class="row item-builder">
                                <div class="col-md-2"><?= Html::input('text', 'percent[]', $perVar['percent'], ['class' => 'form-control', 'placeholder' => 'Persen', 'disabled' => false]) ?></div>
                                <div class="col-md-2">
                                    <?=
                                    MaskedInput::widget([
                                        'name' => 'startdate[]',
                                        'value' => $perVar['startdate'],
                                        'mask' => "1-2-y h:s",
                                        'clientOptions' => [
                                            'alias' => 'datetime',
                                            "placeholder" => "dd-mm-yyyy hh:mm",
                                            "separator" => "-",
                                        ]
                                    ]);
                                    ?>
                                </div>
                                <div class="col-md-2">
                                    <?=
                                    MaskedInput::widget([
                                        'name' => 'enddate[]',
                                        'value' => $perVar['enddate'],
                                        'mask' => "1-2-y h:s",
                                        'clientOptions' => [
                                            'alias' => 'datetime',
                                            "placeholder" => "dd-mm-yyyy hh:mm",
                                            "separator" => "-",
                                        ]
                                    ]);
                                    ?>
                                </div>
                                <div class="col-md-1"><?= Html::dropDownList('status[]', $perVar['status'], Dlist::dListStatusActive(), ['class' => 'form-control', 'disabled' => false]) ?></div>
                                <div class="col-md-1">
                                    <?= Html::hiddenInput('iddiscount[]', $perVar['iddiscount']) ?>
                                    <?= Html::hiddenInput('deldiscount[]', NULL, ['class' => 'del-me']) ?>
                                    <?php
                                    echo Html::a('Hapus', '#', ['class' => 'btn btn-sm btn-danger btn-del']);
                                    ?>
                                </div>
                            </div>
                            <?php
                        }
                    }
                }

                $max = 3 - $countDis;
                for ($i = 1; $i <= $max; $i++) {
                    ?>
                    <div style="margin-top: 4px;border-bottom: 1px solid #e9ecef;padding: 10px 0px 10px 0px;" class="row">
                        <div class="col-md-2"><?= Html::input('text', 'percent[]', NULL, ['class' => 'form-control', 'placeholder' => 'Persen', 'required' => false]) ?></div>
                        <div class="col-md-2">
                            <?=
                            MaskedInput::widget([
                                'name' => 'startdate[]',
                                'mask' => "1-2-y h:s",
                                'clientOptions' => [
                                    'alias' => 'datetime',
                                    "placeholder" => "dd-mm-yyyy hh:mm",
                                    "separator" => "-",
                                ]
                            ]);
                            ?>
                        </div>
                        <div class="col-md-2">
                            <?=
                            MaskedInput::widget([
                                'name' => 'enddate[]',
                                'value' => null,
                                'mask' => "1-2-y h:s",
                                'clientOptions' => [
                                    'alias' => 'datetime',
                                    "placeholder" => "dd-mm-yyyy hh:mm",
                                    "separator" => "-",
                                ]
                            ]);
                            ?>
                        </div>
                        <div class="col-md-1"><?= Html::dropDownList('status[]', null, Dlist::dListStatusActive(), ['class' => 'form-control', 'disabled' => false]) ?></div>
                        <div class="col-md-1">
                            <?= Html::hiddenInput('iddiscount[]', NULL) ?>
                            <?= Html::hiddenInput('deldiscount[]', NULL, ['class' => 'del-me']) ?>
                            <?= Html::a('Hapus', '#', ['class' => 'btn btn-sm btn-danger btn-del']) ?>
                        </div>
                    </div>
                <?php } ?>

            </div>
            <p>
                <br/>
                <?php //Html::button("+ Tambah", ['class' => 'btn btn-sm btn-primary btn-clone'])  ?>
            </p>
            <hr>
            <div class="form-group float-right">
                <?= Html::submitButton('Next > Pengaturan', ['class' => 'btn btn-success']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<div style="display: none;" class="to-clone">
    <div style="margin-top: 4px;border-bottom: 1px solid #e9ecef;padding: 10px 0px 10px 0px;" class="row">
        <div class="col-md-2"><?= Html::input('text', 'percent[]', NULL, ['class' => 'form-control', 'placeholder' => 'Persen', 'required' => false]) ?></div>
        <div class="col-md-2">                            <?=
            MaskedInput::widget([
                'name' => 'startdate[]',
                'mask' => "1-2-y h:s",
                'clientOptions' => [
                    'alias' => 'datetime',
                    "placeholder" => "dd-mm-yyyy hh:mm",
                    "separator" => "-"
                ]
            ]);
            ?></div>
        <div class="col-md-2"><?= Html::input('text', 'enddate[]', NULL, ['class' => 'form-control enddate', 'placeholder' => 'Tanggal Selesai']) ?></div>
        <div class="col-md-1"><?= Html::input('text', 'status[]', NULL, ['class' => 'form-control', 'placeholder' => 'Status']) ?></div>
        <div class="col-md-1">
            <?= Html::hiddenInput('iddiscount[]', NULL) ?>
            <?= Html::hiddenInput('deldiscount[]', NULL, ['class' => 'del-me']) ?>
            <?= Html::a('Hapus', '#', ['class' => 'btn btn-sm btn-danger btn-del']) ?>
        </div>
    </div>
</div>


<?php JSRegister::begin(); ?>
<script>
    $(document).ready(function () {
        function initDelete() {
            $('.btn-del').off().on('click', function (e) {
                e.preventDefault();
                console.log(this);
                $(this).siblings('.del-me').val(1);
                $(this).parent().parent('.row').hide();
            });
        }
        initDelete();
        $('.btn-clone').click(function (e) {
            console.log(this);
            e.preventDefault();
            var html = $('.to-clone').html();
            $('.option-wrap').append(html);
            initDelete();
        });
    });
</script>
<?php JSRegister::end(); ?>
