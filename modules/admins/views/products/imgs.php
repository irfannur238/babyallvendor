<?php

use app\helpers\ProductHelper;
use app\helpers\Url;
use app\models\FRMPROBLEMS;
use richardfan\widget\JSRegister;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $model FRMPROBLEMS */
/* @var $form ActiveForm */
$this->title = ProductHelper::textProductname($model, 'Gambar');
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="products-create">

    <?php
    echo $this->render('_tab', [
        'model' => $model,
    ]);
    ?>

    <?php
    $form = ActiveForm::begin([
                'id' => 'frmproblem',
                'options' => ['enctype' => 'multipart/form-data']
    ]);
    ?>
    <?php $this->registerCss('.th-isian{font-weight:bold;}'); ?>
    <div class="row">
        <div class="col-md-12">
            <div style="margin-top: 4px;" class="row">
                <div class="col-md-2 th-isian">Gambar</div>
                <div class="col-md-3 th-isian"></div>
                <div class="col-md-2 th-isian">Gambar dari Varian</div>
                <div class="col-md-1 th-isian">Urutan</div>
            </div>
            <div class="option-wrap">
                <?php
                $parent = ProductHelper::getParentVarian($model->idproduct);
                if (!$model->isNewRecord) {
                    $varians = ProductHelper::getVarianProduct($model->idproduct, ProductHelper::TYPE_IMG, ['varianimg.ordering' => SORT_ASC]);
                    foreach ($varians as $perVar) {
                        ?>
                        <div style="margin-top: 4px;border-bottom: 1px solid #e9ecef;padding: 10px 0px 10px 0px;"  class="row">
                            <div class="col-md-2"><img class="myImg" id="<?= $perVar['idvarian'] ?>" src="<?= Url::getBaseImg($perVar['img'], $perVar['datecreated']) ?>" style='max-height:50px'/></div>
                            <div class="col-md-3"><?php echo Html::fileInput('img[]', $perVar['img'], ['placeholder' => 'Gambar', 'disabled' => false]) ?></div>
                            <div class="col-md-2"><?= Html::dropDownList('parent[]', $perVar['parent'], $parent, ['class' => 'form-control', 'disabled' => false]) ?></div>
                            <div class="col-md-1"><?= Html::input('text', 'ordering[]', $perVar['ordering'], ['class' => 'form-control', 'placeholder' => 'Urutan', 'disabled' => false]) ?></div>
                            <div class="col-md-1">
                                <?= Html::hiddenInput('idvarian[]', $perVar['idvarian']) ?>
                                <?= Html::hiddenInput('delvarian[]', NULL, ['class' => 'del-me']) ?>
                                <?php
                                echo Html::a('Hapus', '#', ['class' => 'btn btn-sm btn-danger btn-del']);
                                ?>
                            </div>
                        </div>
                        <?php
                    }
                }
                if (empty($varians)) {
                    ?>
                    <div style="margin-top: 4px;border-bottom: 1px solid #e9ecef;padding: 10px 0px 10px 0px;"  class="row">
                        <div class="col-md-2"><i class="nav-icon fas fa-file-image"></i></div>
                        <div class="col-md-3"><?php echo Html::fileInput('img[]', NULL, ['placeholder' => 'Gambar', 'disabled' => false, 'required' => true]) ?></div>
                        <div class="col-md-2"><?= Html::dropDownList('parent[]', NULL, $parent, ['class' => 'form-control', 'disabled' => false]) ?></div>
                        <div class="col-md-1"><?= Html::input('text', 'ordering[]', NULL, ['class' => 'form-control', 'placeholder' => 'Urutan', 'disabled' => false]) ?></div>
                        <div class="col-md-1">
                            <?= Html::hiddenInput('idvarian[]', NULL) ?>
                            <?= Html::hiddenInput('delvarian[]', NULL, ['class' => 'del-me']) ?>
                            <?= Html::a('Hapus', '#', ['class' => 'btn btn-sm btn-danger btn-del']) ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <p>
                <br/>
                <?= Html::button("+ Tambah", ['class' => 'btn btn-sm btn-primary btn-clone']) ?>
            </p>
            <hr>
            <div class="form-group float-right">
                <?= Html::submitButton('Next > Spesifikasi', ['class' => 'btn btn-success']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<div style="display: none;" class="to-clone">
    <div style="margin-top: 4px;border-bottom: 1px solid #e9ecef;padding: 10px 0px 10px 0px;"  class="row">
        <div class="col-md-2"><i class="nav-icon fas fa-file-image"></i></div>
        <div class="col-md-3"><?= Html::fileInput('img[]', NULL, ['placeholder' => 'Gambar', 'disabled' => false]) ?></div>
        <div class="col-md-2"><?= Html::dropDownList('parent[]', NULL, $parent, ['class' => 'form-control', 'disabled' => false]) ?></div>
        <div class="col-md-1"><?= Html::input('text', 'ordering[]', NULL, ['class' => 'form-control', 'placeholder' => 'Urutan', 'disabled' => false]) ?></div>
        <div class="col-md-1">
            <?= Html::hiddenInput('idvarian[]', NULL) ?>
            <?= Html::hiddenInput('delvarian[]', NULL, ['class' => 'del-me']) ?>
            <?= Html::a('Hapus', '#', ['class' => 'btn btn-sm btn-danger btn-del']) ?>
        </div>
    </div>
</div>


<?php JSRegister::begin(); ?>
<script>
    function initDelete() {
        $('.btn-del').off().on('click', function (e) {
            e.preventDefault();
            console.log(this);
            $(this).siblings('.del-me').val(1);
            $(this).parent().parent('.row').hide();
        });
    }
    initDelete();
    $('.btn-clone').click(function (e) {
        console.log(this);
        e.preventDefault();
        var html = $('.to-clone').html();
        $('.option-wrap').append(html);
        initDelete();
    });
</script>
<?php JSRegister::end(); ?>

<!--
<div id="lightbox" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-lighbox">
                <img src="" alt="" />
            </div>
        </div>
    </div>
</div>
<div class="col-md-2"><a href='#' class='fancybox' data-toggle='modal' data-target='#lightbox'><img src='<?php //Url::getBaseImg($perVar['img'], $perVar['datecreated'])  ?>' style='height:150px'/></a></div>
-->

