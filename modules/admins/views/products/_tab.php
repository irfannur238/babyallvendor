<?php

use app\helpers\Url;
use yii\helpers\Html;

$curAct = Url::getAction();
$tabs = [
    [
        'action' => 'update',
        'name' => 'General'
    ],
    [
        'action' => 'optionprice',
        'name' => 'Pilihan & Harga'
    ],
    [
        'action' => 'imgs',
        'name' => 'Gambar'
    ],
    [
        'action' => 'specification',
        'name' => 'Spesifikasi'
    ],
    [
        'action' => 'discount',
        'name' => 'Diskon'
    ],
    [
        'action' => 'setting',
        'name' => 'Pengaturan'
    ], 
    [
        'action' => 'resume',
        'name' => 'Resume'
    ]
];
?>

<ul class="nav nav-tabs">
    <?php
    foreach ($tabs as $perTab) {
        $active = $curAct == $perTab['action'] ? 'active' : '';
        if ($curAct == 'create') {
            if (in_array($perTab['action'], ['create', 'update'])) {
                echo '<li class="nav-item">' . Html::a($perTab['name'], '#', ['class' => 'nav-link active']) . '</li>';
            } else {
                echo '<li class="nav-item">' . Html::a($perTab['name'], '#', ['class' => 'nav-link ' . $active]) . '</li>';
            }
        } else {
            echo '<li class="nav-item">' . Html::a($perTab['name'], [$perTab['action'], 'id' => $model->idproduct], ['class' => 'nav-link ' . $active]) . '</li>';
        }
    }
    ?>
</ul></br>

