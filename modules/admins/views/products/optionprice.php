<?php

use app\helpers\Dlist;
use app\helpers\ProductHelper;
use app\models\FRMPROBLEMS;
use richardfan\widget\JSRegister;
use yii\bootstrap4\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $model FRMPROBLEMS */
/* @var $form ActiveForm */
$this->title = ProductHelper::textProductname($model, 'Pilihan & Harga');
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="products-create">

    <?php
    echo $this->render('_tab', [
        'model' => $model,
    ]);
    ?>

    <?php $form = ActiveForm::begin(['id' => 'frmproblem']); ?>
    <div class="row">
        <!--        <div class="col-md-12">
        <?= $form->field($model, 'isvarian')->dropDownList(Dlist::dListIsvarian())->label('Pilihan') ?>
                </div>-->
    </div>
    <?php $this->registerCss('.th-isian{font-weight:bold;}'); ?>
    <div class="row">
        <div class="col-md-12">
            <div style="margin-top: 4px;" class="row">
                <div class="col-md-4 th-isian">Nama / Warna</div>
                <div class="col-md-1 th-isian">Stok</div>
                <div class="col-md-2 th-isian">Harga <code>(Rp)</code></div>
                <div class="col-md-1 th-isian">Size</div>
                <div class="col-md-1 th-isian">Action</div>
            </div>
            <div class="option-wrap">
                <?php
                if (!$model->isNewRecord) {
                    $varians = ProductHelper::getVarianProduct($model->idproduct);
                    foreach ($varians as $perVar) {
                        ?>
                        <div style="margin-top: 4px;border-bottom: 1px solid #e9ecef;padding: 10px 0px 10px 0px;" class="row item-builder">
                            <div class="col-md-4"><?= Html::input('text', 'namevarian[]', $perVar['namevarian'], ['class' => 'form-control c-name', 'placeholder' => 'Nama / Warna', 'disabled' => false, 'required' => true]) ?></div>
                            <div class="col-md-1"><?= Html::input('text', 'qty[]', $perVar['qty'], ['type' => 'number', 'class' => 'form-control c-qty', 'placeholder' => 'Stok', 'disabled' => false, 'required' => true]) ?></div>
                            <div class="col-md-2"><?= Html::input('text', 'price[]', $perVar['price'], ['type' => 'number', 'class' => 'form-control c-price', 'placeholder' => 'Harga', 'disabled' => false, 'required' => true]) ?></div>
                            <div class="col-md-1"><?= Html::input('text', 'size[]', $perVar['size'], ['class' => 'form-control', 'placeholder' => 'size', 'disabled' => false]) ?></div>
                            <div class="col-md-1">
                                <?= Html::hiddenInput('idvarian[]', $perVar['idvarian']) ?>
                                <?= Html::hiddenInput('delvarian[]', NULL, ['class' => 'del-me']) ?>
                                <?php
                                echo Html::a('Hapus', '#', ['class' => 'btn btn-sm btn-danger btn-del']);
                                ?>
                            </div>
                        </div>
                        <?php
                    }
                }
                if (empty($varians)) {
                    ?>
                    <div style="margin-top: 4px;border-bottom: 1px solid #e9ecef;padding: 10px 0px 10px 0px;" class="row">
                        <div class="col-md-4"><?= Html::input('text', 'namevarian[]', NULL, ['class' => 'form-control c-name', 'placeholder' => 'Nama / Warna', 'required' => true]) ?></div>
                        <div class="col-md-1"><?= Html::input('text', 'qty[]', NULL, ['type' => 'number', 'class' => 'form-control c-qty', 'placeholder' => 'Stok', 'required' => true]) ?></div>
                        <div class="col-md-2"><?= Html::input('text', 'price[]', NULL, ['type' => 'number', 'class' => 'form-control c-price', 'placeholder' => 'Harga', 'required' => true]) ?></div>
                        <div class="col-md-1"><?= Html::input('text', 'size[]', NULL, ['class' => 'form-control', 'placeholder' => 'size']) ?></div>
                        <div class="col-md-1">
                            <?= Html::hiddenInput('idvarian[]', NULL) ?>
                            <?= Html::hiddenInput('delvarian[]', NULL, ['class' => 'del-me']) ?>
                            <?= Html::a('Hapus', '#', ['class' => 'btn btn-sm btn-danger btn-del']) ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <p>
                <br/>
                <?= Html::button("+ Tambah", ['class' => 'btn btn-sm btn-primary btn-clone']) ?>
            </p>
            <hr>
            <div class="form-group float-right">
                <?= Html::submitButton('Next > Gambar', ['class' => 'btn btn-success btn-save']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<div style="display: none;" class="to-clone">
    <div style="margin-top: 4px;border-bottom: 1px solid #e9ecef;padding: 10px 0px 10px 0px;" class="row">
        <div class="col-md-4"><?= Html::input('text', 'namevarian[]', NULL, ['class' => 'form-control c-name', 'placeholder' => 'Nama / Warna', 'required' => true]) ?></div>
        <div class="col-md-1"><?= Html::input('text', 'qty[]', NULL, ['type' => 'number', 'class' => 'form-control c-qty', 'placeholder' => 'Stok', 'required' => true]) ?></div>
        <div class="col-md-2"><?= Html::input('text', 'price[]', NULL, ['type' => 'number', 'class' => 'form-control c-price', 'placeholder' => 'Harga', 'required' => true]) ?></div>
        <div class="col-md-1"><?= Html::input('text', 'size[]', NULL, ['class' => 'form-control', 'placeholder' => 'size']) ?></div>
        <div class="col-md-1">
            <?= Html::hiddenInput('idvarian[]', NULL) ?>
            <?= Html::hiddenInput('delvarian[]', NULL, ['class' => 'del-me']) ?>
            <?= Html::a('Hapus', '#', ['class' => 'btn btn-sm btn-danger btn-del']) ?>
        </div>
    </div>
</div>


<?php JSRegister::begin(); ?>
<script>
    function initDelete() {
        $('.btn-del').off().on('click', function (e) {
            e.preventDefault();
            //console.log(this);
            $(this).siblings('.del-me').val(1);
            $(this).parent().parent('.row').hide();

            var selector = $(this).parent().parent('.row');
            var c_name = selector.find('.c-name');
            var c_qty = selector.find('.c-qty');
            var c_price = selector.find('.c-price');

            requireForm(c_name, false);
            requireForm(c_qty, false);
            requireForm(c_price, false);
        });
    }
    initDelete();
    $('.btn-clone').click(function (e) {
        console.log(this);
        e.preventDefault();
        var html = $('.to-clone').html();
        $('.option-wrap').append(html);
        initDelete();
    });

    function requireForm(fclass, stat) {
        return fclass.prop("required", stat);
    }
</script>
<?php JSRegister::end(); ?>
