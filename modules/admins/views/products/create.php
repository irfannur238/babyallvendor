<?php

use app\helpers\Dlist;
use app\helpers\ProductHelper;
use app\models\Products;
use wadeshuler\ckeditor\widgets\CKEditor;
use yii\helpers\Html;
use yii\web\View;
use yii\bootstrap4\ActiveForm;

/* @var $this View */
/* @var $model Products */
$this->title = ProductHelper::textProductname($model, 'General');
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-create">

    <?php
    echo $this->render('_tab', [
        'model' => $model,
    ]);
    ?>

    <div class="products-form">

        <?php $form = ActiveForm::begin([
            'layout' => 'horizontal', 
        ]); ?>

        <?= $form->field($model, 'idcategory')->dropDownList(Dlist::dListCategory())->label('Kategori') ?>

        <?= $form->field($model, 'productname')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'description')->widget(CKEditor::className()) ?>

        <div class="form-group float-right">
            <?= Html::submitButton('Next > Pilihan & Harga', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
