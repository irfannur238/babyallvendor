<?php

use app\helpers\Dlist;
use app\helpers\ProductHelper;
use app\models\Products;
use yii\helpers\Html;
use yii\web\View;
use yii\bootstrap4\ActiveForm;

/* @var $this View */
/* @var $model Products */

$this->title = ProductHelper::textProductname($model, 'Pengaturan');
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-create">

    <?php
    echo $this->render('_tab', [
        'model' => $model,
    ]);
    ?>

    <div class="products-form">

        <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>

        <?= $form->field($model, 'status')->dropDownList(Dlist::dListStatusActive())->label() ?>

        <?= $form->field($model, 'isfeatured')->dropDownList(Dlist::dListIsfeatured())->label('Tampikan di halaman terbaru') ?>

        <div class="form-group float-right">
            <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
