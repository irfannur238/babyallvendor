<?php

use app\models\Products;
use wadeshuler\ckeditor\widgets\CKEditor;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $model Products */
/* @var $form ActiveForm */
?>

<div class="products-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'productname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'amount')->textInput() ?>
    
    <?= $form->field($model, 'discount')->textInput()->label('Diskon <code>%</code>') ?>

    <?= $form->field($model, 'description')->widget(CKEditor::className()) ?>

    <?= $form->field($model, 'size')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'note')->textarea(['rows' => 4]) ?>

    <?= $form->field($model, 'link1')->textInput(['maxlength' => true])->label('Link Shoope') ?>

    <?= $form->field($model, 'link2')->textInput(['maxlength' => true])->label('Link Tokopedia') ?>

  

    <?php
    if (!$model->isNewRecord) {
        $imgs = explode(';', $model->img);
        $model->img1 = isset($imgs[0]) ? $imgs[0] : null;
        $model->img2 = isset($imgs[1]) ? $imgs[1] : null;
        $model->img3 = isset($imgs[2]) ? $imgs[2] : null;
        $model->img4 = isset($imgs[3]) ? $imgs[3] : null;
        $model->img5 = isset($imgs[4]) ? $imgs[4] : null; 
        
        echo $form->field($model, 'img1')->fileInput();
        if (isset($imgs[0])) {
            echo Html::img(Url::base() . '/uploads/img/' . $imgs[0], ['style' => 'width:100px;', 'id' => 'myImg']);
            echo '<div class="form-group">
            <input type="checkbox" name="DELETEPHOTO1" id="DELETEPHOTO1" value="1"> <label class="control-label" for="DELETEPHOTO">Hapus Photo 1</label>
            </div><hr>';
        }
        echo $form->field($model, 'img2')->fileInput();
        if (isset($imgs[1])) {
            echo Html::img(Url::base() . '/uploads/img/' . $imgs[1], ['style' => 'width:100px;', 'id' => 'myImg']);
            echo '<div class="form-group">
            <input type="checkbox" name="DELETEPHOTO2" id="DELETEPHOTO2" value="1"> <label class="control-label" for="DELETEPHOTO">Hapus Photo 2</label>
            </div><hr>';
        }
        echo $form->field($model, 'img3')->fileInput();
        if (isset($imgs[2])) {
            echo Html::img(Url::base() . '/uploads/img/' . $imgs[2], ['style' => 'width:100px;', 'id' => 'myImg']);
            echo '<div class="form-group">
            <input type="checkbox" name="DELETEPHOTO3" id="DELETEPHOTO3" value="1"> <label class="control-label" for="DELETEPHOTO">Hapus Photo 3</label>
            </div><hr>';
        }
        echo $form->field($model, 'img4')->fileInput();
        if (isset($imgs[3])) {
            echo Html::img(Url::base() . '/uploads/img/' . $imgs[3], ['style' => 'width:100px;', 'id' => 'myImg']);
            echo '<div class="form-group">
            <input type="checkbox" name="DELETEPHOTO4" id="DELETEPHOTO4" value="1"> <label class="control-label" for="DELETEPHOTO">Hapus Photo 4</label>
            </div><hr>';
        }
        echo $form->field($model, 'img5')->fileInput();
        if (isset($imgs[4])) {
            echo Html::img(Url::base() . '/uploads/img/' . $imgs[4], ['style' => 'width:100px;', 'id' => 'myImg']);
            echo '<div class="form-group">
            <input type="checkbox" name="DELETEPHOTO5" id="DELETEPHOTO5" value="1"> <label class="control-label" for="DELETEPHOTO">Hapus Photo 5</label>
            </div><hr>';
        }
    } else {
        ?>

        <?= $form->field($model, 'img1')->fileInput() ?>

        <?= $form->field($model, 'img2')->fileInput() ?> 

        <?= $form->field($model, 'img3')->fileInput() ?>

        <?= $form->field($model, 'img4')->fileInput() ?> 

        <?= $form->field($model, 'img5')->fileInput() ?> 

    <?php } ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
