<?php

use app\helpers\Dlist;
use app\helpers\ProductHelper;
use app\helpers\Url;
use app\models\Products;
use yii\helpers\Html;
use yii\web\View;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this View */
/* @var $model Products */

$this->title = ProductHelper::textProductname($model, 'Resume');
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);
$varian = ProductHelper::getVarianProduct($model->idproduct);
$images = ProductHelper::getVarianProduct($model->idproduct, ProductHelper::TYPE_IMG);
$price = ProductHelper::getPrice($varian);
$discount = ProductHelper::getDiscountProduct($model->idproduct);
?>
<div class="products-view">

    <?php
    echo $this->render('_tab', [
        'model' => $model,
    ]);
    ?>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->idproduct], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Delete', ['delete', 'id' => $model->idproduct], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <h4>Gambar</h4>
    <div class="card-group">
        <?php
        foreach ($images as $perImg) {
            echo '
            <div class="card" style="width: 18rem;">
                    <a href="#" class="fancybox" data-toggle="modal" data-target="#lightbox"><img class="card-img-top" src="' . Url::getBaseImg($perImg['img'], $perImg['datecreated']) . '" style="width:200px"/></a>
                    <div class="card-body">
                        <p class="card-text">' . $perImg['parentname'] . '</p>
                    </div>
                </div>';
        }
        ?>
    </div><br><br>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'sku:ntext:SKU',
            'productname',
            [
                'label' => 'Harga',
                'attribute' => 'price',
                'format' => 'html',
                'value' => function ($model) use ($price) {
                    return $price['formatPrice'];
                }
            ],
            'idcategory0.name:ntext:Kategori',
            [
                'label' => 'Total Stok',
                'attribute' => 'amount',
                'format' => 'html',
                'value' => function ($model) use ($price) {
                    return $price['totalQty'];
                }
            ],
            [
                'label' => 'Deskripsi',
                'attribute' => 'description',
                'format' => 'html',
                'value' => function ($model) {
                    return $model->description;
                }
            ],
            'datecreated:ntext:Tanggal buat',
            'status',
        ],
    ])
    ?><br>

    <h4>Pilihan & Harga</h4>
    <?php
    echo Html::beginTag('table', ['class' => 'table table-striped table-hover table-bordered']);
    echo Html::beginTag('thead');
    echo Html::tag('th', 'No');
    echo Html::tag('th', 'Nama / Warna');
    echo Html::tag('th', 'Size');
    echo Html::tag('th', 'Harga');
    echo Html::tag('th', 'Stok');
    echo Html::endTag('thead');

    echo Html::beginTag('tbody');
    $no = 1;
    foreach ($varian as $i => $row) {
        echo Html::beginTag('tr');
        echo Html::tag('td', $no++, ['width' => '3%']);
        echo Html::tag('td', $row['namevarian']);
        echo Html::tag('td', $row['size']);
        echo Html::tag('td', ProductHelper::idrPrice($row['price']));
        echo Html::tag('td', $row['qty']);
        echo Html::endTag('tr');
    }
    echo Html::endTag('tbody');

    echo Html::endTag('table');
    ?>
    <br>

    <h4>Spesifikasi</h4>
    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'Berat',
                'attribute' => 'weight',
                'format' => 'html',
                'value' => function ($model) {
                    return $model->weight . ' gram';
                }
            ],
            [
                'label' => 'Lebar',
                'attribute' => 'width',
                'format' => 'html',
                'value' => function ($model) {
                    return $model->width . ' cm';
                }
            ],
            [
                'label' => 'Tinggi',
                'attribute' => 'height',
                'format' => 'html',
                'value' => function ($model) {
                    return $model->height . ' cm';
                }
            ],
        ],
    ])
    ?><br>

    <h4>Diskon</h4>
    <?php
    echo Html::beginTag('table', ['class' => 'table table-striped table-hover table-bordered']);
    echo Html::beginTag('thead');
    echo Html::tag('th', 'No');
    echo Html::tag('th', 'Persen');
    echo Html::tag('th', 'Tanggal Mulai');
    echo Html::tag('th', 'Tanggal Selesai');
    echo Html::tag('th', 'Status');
    echo Html::endTag('thead');

    echo Html::beginTag('tbody');
    $no = 1;
    foreach ($discount as $i => $row) {
        echo Html::beginTag('tr');
        echo Html::tag('td', $no++, ['width' => '3%']);
        echo Html::tag('td', $row['percent']);
        echo Html::tag('td', $row['startdate']);
        echo Html::tag('td', $row['enddate']);
        echo Html::tag('td', $row['status']);
        echo Html::endTag('tr');
    }
    if (!$discount) {
        echo Html::beginTag('tr');
        echo Html::tag('td', 'Tidak Ada Diskon', ['colspan' => 5]);
        echo Html::endTag('tr');
    }
    echo Html::endTag('tbody');
    echo Html::endTag('table');
    ?>
    <br>

    <h4>Pengaturan</h4>
    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'Status',
                'attribute' => 'status',
                'format' => 'html',
                'value' => function ($model) {
                    $res = isset(Dlist::dListStatusActive()[$model->status]) ? Dlist::dListStatusActive()[$model->status] : '-';
                    return $res;
                }
            ],
            [
                'label' => 'Tampikan di halaman terbaru',
                'attribute' => 'isfeatured',
                'format' => 'html',
                'value' => function ($model) {
                    $res = isset(Dlist::dListIsfeatured()[$model->isfeatured]) ? Dlist::dListIsfeatured()[$model->isfeatured] : '-';
                    return $res;
                }
            ],
        ],
    ])
    ?><br>

</div>

<div id="lightbox" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="width: fit-content;">
            <div class="modal-lighbox">
                <img src="" alt="" />
                <label class="lbl" style="padding-top: 10px;"></label>
            </div>
        </div>
    </div>
</div>
