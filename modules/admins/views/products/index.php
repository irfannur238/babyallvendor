<?php

use app\helpers\Url;
use kartik\grid\GridView;
use yii\bootstrap4\LinkPager;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $dataProvider ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-index">

<!--    <p>
    <?= Html::a('Create Products', ['create'], ['class' => 'btn btn-success']) ?>
    </p>-->

    <div class="panel panel-form">
        <?=
        GridView::widget(Yii::$app->params['gridConfig'] + [
            'dataProvider' => $dataProvider,
            'filterModel' => $model,
            'pager' => [
                'class' => LinkPager::class
            ],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'label' => 'Gambar',
                    'format' => 'raw',
                    'value' => function ($model, $key) {
                        return Html::img(Url::getBaseImg($model['img'], $model['imgcreated']), ['width' => '90']);
                    }
                ],
                'productname',
                'sku',
                [
                    'attribute' => 'price',
                    'label' => 'Harga',
                    'format' => 'html',
                    'value' => function ($model, $key) {
                        return $model->price;
                    }
                ],
                //['class' => 'app\widgets\ActionColumn'], 
                ['class' => 'app\widgets\ActionColumn', 'template' => '{view}{update}{delete}',
                    'buttons' => [
                        'view' => function ($url, $model) {
                            return Html::a('<span class="far fa-eye"></span>', ['products/resume/', 'id' => $model['idproduct']], ['data-pjax' => 0, 'target' => "_blank"]) . '&nbsp;';
                        },
                    ],
                ],
            ],
            'toolbar' => [
                [
                    'content' =>
                    Html::a('<i class="fas fa-plus"></i> Tambah', ['create'], [
                        'class' => 'btn btn-success',
                        'title' => Yii::t('kvgrid', 'Add Product')
                    ]) . ' ' .
                    Html::a('<i class="fas fa-redo"></i>', ['index'], [
                        'class' => 'btn btn-secondary',
                        'title' => Yii::t('kvgrid', 'Reset Grid')
                    ]),
                ],
                '{export}',
                '{toggleData}'
            ],
        ]);
        ?>
    </div>

</div>
