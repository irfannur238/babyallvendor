<?php

use app\helpers\ProductHelper;
use app\models\Products;
use yii\helpers\Html;
use yii\web\View;
use kartik\form\ActiveForm;

/* @var $this View */
/* @var $model Products */

$this->title = ProductHelper::textProductname($model, 'Spesifikasi');
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-create">

    <?php
    echo $this->render('_tab', [
        'model' => $model,
    ]);
    ?>

    <div class="products-form">

        <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]); ?>
        <?=
        $form->field($model, 'weight', [
            'addon' => [
                'append' => ['content' => 'gram']
            ],
        ])->input('number', ['min'=>1, 'max'=>100000, 'placeholder' => 'Enter a rating between 1 and 5...']);
        ?>

        <?=
        $form->field($model, 'width', [
            'addon' => [
                'append' => ['content' => 'cm']
            ],
        ]);
        ?>

        <?=
        $form->field($model, 'height', [
            'addon' => [
                'append' => ['content' => 'cm']
            ],
        ]);
        ?>

        <div class="form-group float-right">
            <?= Html::submitButton('Next > Diskon', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
