<?php

use app\helpers\Configtype;
use app\models\Configweb;
use wadeshuler\ckeditor\widgets\CKEditor;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $model Configweb */
/* @var $form ActiveForm */
?>

<div class="configweb-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'type')->dropDownList(Configtype::arrConfigtype()) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?> 
    
    <?= $form->field($model, 'description')->widget(CKEditor::className()) ?>

    <?= $form->field($model, 'address')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'support')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'map')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'note')->textarea(['rows' => 6]) ?> 

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
