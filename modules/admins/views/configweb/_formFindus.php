<?php

use app\models\Configweb;
use kartik\color\ColorInput;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $model Configweb */
/* @var $form ActiveForm */
$this->title = 'Find Us';
$this->params['breadcrumbs'][] = ['label' => 'Web', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-create">

    <div class="configweb-form">

        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

        <?= $form->field($model, 'description')->label('Nama <code> &nbsp;Contoh : Shopee </code>') ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Link <code> &nbsp;Contoh : https://shopee.co.id/khairahijab</code>') ?> 

        <?=
        $form->field($model, 'note')->widget(ColorInput::classname(), [
            'options' => ['placeholder' => 'Select color ...'],
        ])->label('Warna Huruf <code> &nbsp;Klik panah bawah lalu pilih warna huruf . Contoh : #FFFF </code>')
        ?> 

        <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>

<?php ActiveForm::end(); ?>

    </div>
</div>
