<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Configweb */

$this->title = 'Update Configweb: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Configwebs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->idconfig]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="configweb-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
