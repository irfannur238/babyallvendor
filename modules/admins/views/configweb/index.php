<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Find Us';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="configweb-index">

    <p class="float-right">
        <?= Html::a('Tambah', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'description:ntext:Nama',
            'name:ntext:Link',
            //'note:ntext:Warna',
            'datecreated',
            //'type:ntext:Tipe',
            //'address:ntext',
            //'phone',
            //'support',
            //'map:ntext',
            //'status',
            //'description:ntext',
            ['class' => 'app\widgets\ActionColumn'],
        ],
    ]);
    ?>


</div>
