<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Configweb */

$this->title = 'Create Configweb';
$this->params['breadcrumbs'][] = ['label' => 'Configwebs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="configweb-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
