<?php

use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\bootstrap4\ActiveForm;

/* @var $this View */
/* @var $model Users */

$this->title = 'Toko: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Toko', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->idstore]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="users-update">

<!--    <h1><?= Html::encode($this->title) ?></h1>-->

    <div class="users-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'owner')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'phone')->textInput(['type' => 'number', 'maxlength' => true]) ?>

        <?=
        $form->field($model, 'province_id')->widget(Select2::classname(), [
            'data' => $provinces,
            'options' => [
                'placeholder' => '-- Pilih Provinsi --',
                'id' => 'province_id',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label('Provinsi');
        ?>

        <?=
        $form->field($model, 'city_id')->widget(DepDrop::classname(), [
            'data' => $cities,
            'type' => DepDrop::TYPE_SELECT2,
            'options' =>
            [
                'id' => 'city_id',
                'placeholder' => Yii::t('app', '--Pilih Kabupaten / Kota--'),
            ],
            'select2Options' => ['pluginOptions' => ['allowClear' => true]],
            'pluginOptions' => [
                'depends' => ['province_id'],
                'url' => Url::to(['/apps/api/depcity']),
                'placeholder' => Yii::t('app', '--Pilih Semua--'),
                'allowClear' => true,
            ]
        ])->label('Kabupaten / Kota');
        ?>

        <?= $form->field($model, 'district')->textInput() ?>
        
        <?= $form->field($model, 'postal_code')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'address')->textarea(['maxlength' => true]) ?>

        <?= $form->field($model, 'phone_other')->textInput(['type' => 'number', 'maxlength' => true]) ?>

        <?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>

        <div class="form-group">
            <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
