<?php

use app\helpers\Dlist;
use app\models\Payments;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use app\helpers\Url;
use yii\web\View;

/* @var $this View */
/* @var $model Payments */
/* @var $form ActiveForm */
?>

<div class="payments-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Nama Bank &nbsp;<code>Contoh : Bank BNI</code>') ?>

    <?= $form->field($model, 'accountnumber')->textInput(['maxlength' => true, 'required' => true])->label('Nomor Rekening &nbsp;<code>Contoh : 1236 7346 43</code>') ?>

    <?= $form->field($model, 'accountname')->textInput(['maxlength' => true, 'required' => true])->label('Atas Nama Rekening &nbsp;<code>Contoh : a/n Budi Bobi</code>') ?>

    <?= $form->field($model, 'status')->dropDownList(Dlist::dListStatusActive()) ?>

    <?php
    if (!$model->isNewRecord) {
        echo '<img class="myImg" src="' . Url::getImg($model->img, '/img/payment/') . '" style="max-height:100px">';
    }
    ?>
    <br><br>
    <?= $form->field($model, 'img')->fileInput()->label('Gambar'); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
