<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pembayaran';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payments-index">

    

    <p class="float-right">
        <?= Html::a('Tambah', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idpayment',
            //'idlevel',
            'name:ntext:Nama Bank',
            'accountnumber:ntext:No.Rekening',
            'accountname:ntext:Atas Nama',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    return app\helpers\Dlist::badgeActive($model->status);
                }
            ],
            //'img',
            //'parent',
            //'status',

            ['class' => 'app\widgets\ActionColumn', 'template' => '{view}{update}{delete}'],
        ],
    ]); ?>


</div>
