<?php

use app\helpers\OrderUtils;
use app\helpers\ProductHelper;
use app\helpers\Url;
use kartik\grid\GridView;
use yii\bootstrap4\LinkPager;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $dataProvider ActiveDataProvider */

$title = $model->status ? 'Pesanan ' . OrderUtils::labelStatusOrder($model->status) : 'Semua Pesanan';
$this->title = $title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-index">

    <div class="panel panel-form">
        <?=
        GridView::widget(Yii::$app->params['gridConfig'] + [
            'dataProvider' => $dataProvider,
            'filterModel' => $model,
            'pager' => [
                'class' => LinkPager::class
            ],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                ['class' => 'app\widgets\ActionColumn', 'template' => '{view}{delete}',
                    'buttons' => [
                        'view' => function ($url, $model) {
                            return Html::a('<span class="far fa-eye"></span>', ['orders/detail/', 'id' => $model['idorder']], ['data-pjax' => 0, 'class' => 'btn btn-sm btn-primary']) . '&nbsp;';
                        },
                        'delete' => function ($url, $model) {
                            if ($model['status'] == OrderUtils::STAT_WAITING_PAYMENT) {
                                return Html::a('<span class="fa fa-trash"></span>', ['orders/setcanceled/', 'id' => $model['idorder']], ['data' => ['confirm' => 'Yakin Ubah Status Order Dibatalakan ?', 'method' => 'post'], 'class' => 'btn btn-sm btn-danger']) . '&nbsp;';
                            }
                        },
                    ],
                ],
                [
                    'label' => '',
                    'format' => 'raw',
                    'value' => function ($model, $key) {
                        return Html::img(Url::getBaseImg($model['orderdetail'][0]['img'], $model['orderdetail'][0]['dateimg']), ['width' => '90']);
                    },
                    'contentOptions' => ['style' => 'min-width: 70px;']
                ],
                [
                    'attribute' => 'namevarian', 
                    'label' => 'Produk dibeli',
                    'format' => 'raw',
                    'value' => function ($model, $key) {
                        return $model['namevarian'] ? $model['namevarian'] : OrderUtils::textVarian($model['orderdetail']);
                    },
                    'contentOptions' => ['style' => 'min-width: 100px;']
                ],
                [
                    'attribute' => 'totalqty', 
                    'label' => 'Jumlah',
                    'format' => 'html',
                    'value' => function ($model, $key) {
                        return ($model['totalqty'] ? $model['totalqty'] : OrderUtils::textVarian($model['orderdetail'], true)) . ' Item';
                    }
                ],
                [
                    'attribute' => 'orderprice',
                    'label' => 'Total Harga',
                    'format' => 'html',
                    'value' => function ($model, $key) {
                        return ProductHelper::idrPrice($model['orderprice']);
                    }
                ],
                [
                    'label' => 'Status',
                    'format' => 'raw',
                    'value' => function ($model, $key) {
                        return OrderUtils::labelStatusOrder($model['status'], true);
                    }
                ],
                'invoicekey:ntext:Nomor Pesanan',
                'orderdate:ntext:Tanggal Pesan',
            ],
            'toolbar' => [
                [
                    'content' =>
                    Html::a('<i class="fas fa-redo"></i>', ['index'], [
                        'class' => 'btn btn-secondary',
                        'title' => Yii::t('kvgrid', 'Reset Grid')
                    ]),
                ],
                '{export}',
                '{toggleData}'
            ],
        ]);
        ?>
    </div>

</div>
