<?php

use app\helpers\Url;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
?>

<div class="row">
    <div class="col-lg-12">
        <div class="alert alert-info" role="alert" style="margin-top: 0px;">
            Bawa paket Anda ke counter <?= $dataOrder['namecourier'] ?> terdekat dan
            Silahkan input nomor resi pada form di bawah.
        </div>

        <?php
        $form = ActiveForm::begin([
                    'action' => Url::urlManager('admins/orders/inputreceiptnumber?id=' . $model['idorder']),
                    'options' => ['class' => 'checkout__form']
        ]);
        ?>
        <?= $form->field($model, 'receiptnumber')->textInput(['type' => 'text', 'placeholder' => 'contoh : JN873913748987', 'required' => true])->label('Input Nomor Resi') ?>
        <?=
        Html::submitButton('Setujui', [
            'class' => 'btn btn-success float-right',
            'data' => [
                'confirm' => 'Apakah anda yakin nomor resi sudah benar ? Pesanan akan dilanjutkan ke status DIKIRIM.',
            ]
        ]);
        ActiveForm::end();
        ?>
    </div>
</div>