<?php

use app\helpers\OrderUtils;
use app\helpers\ProductHelper;
use app\helpers\Url;
use app\models\User;
?>

<div class="bd-callout bd-callout-qyara" style="padding: 10px;">
    <div class="row">
        <div class="col-md-4 border-right">
            <b>Invoice : </b>
            <p id="conveying-meaning-to-assistive-technologies"><?= $dataOrder['invoicekey'] ?></p>
            <hr>
            <b>Status : </b>
            <p><?= OrderUtils::labelStatusOrder($dataOrder['status'], true) ?></p>
            <hr>
            <b>Nama Toko : </b>
            <p><?= User::getStoreName() ?></p>
            <hr>
            <b>Tanggal Pembelian :</b>
            <p><?= $dataOrder['orderdate'] ?></p>
        </div>
        <div class="col-md-8">
            <?php
            if ($data) {
                echo $this->render('_detailVerifypayment', [
                    'data' => $data,
                ]);
            }

            if ($model->status == OrderUtils::STAT_ONPROCESS) {
                echo $this->render('_inputReceiptnumber', [
                    'model' => $model,
                    'dataOrder' => $dataOrder,
                ]);
            }

            if ($model->status == OrderUtils::STAT_ONSHIPPING) {
                echo $this->render('_verifyDone', [
                    'model' => $model,
                    'dataOrder' => $dataOrder,
                ]);
            }
            ?>
        </div>
    </div>
    <hr>

    <h6><b>Daftar Produk</b></h6><br>
    <?php foreach ($dataOrder['orderdetail'] as $key => $perDet) { ?>
        <div class="row">
            <div class="col-md-6 border-right">
                <a href="">
                    <img class="card-img float-left" src="<?= Url::getBaseImg($perDet['img'], $perDet['dateimg']) ?>" alt="image cap" style="width: 50px;margin-right: 15px;">
                    <h6 id="conveying-meaning-to-assistive-technologies" style="margin-bottom: 0px;"><?= $perDet['namevarian'] ?></h6>
                    <p><small><b><?= ProductHelper::idrPrice($perDet['price']) ?></b> <?= $perDet['qty'] . ' Produk (' . OrderUtils::satuanKg($perDet['finalweight']) . ')' ?></small></p>
                </a>
            </div>
            <div class="col-md-6">
                <div class="float-left">
                    <p><small>Total Harga Produk</small></p>
                    <p><?= ProductHelper::idrPrice($perDet['finalprice']) ?></p>
                </div>
            </div>
        </div><hr>
    <?php } ?>

    <h6><b>Catatan Pembeli :</b></h6>
    <p><?= $dataOrder['note'] ?></p><hr>

    <h6><b>Pengiriman</b></h6>
    <div class="row">
        <div class="col-md-12">
            <p><?= $dataOrder['namecourier'] . ' (' . $dataOrder['nameservice'] . ')' ?></p>
            <p>No. Resi: <?= $dataOrder['receiptnumber'] ?></p><br>

            <div class="row">
                <div class="col-md-6">
                    <p>Dikirim kepada <b><?= $dataOrder['cus_name'] . ' ' . $dataOrder['cus_lastname'] ?></b></p>
                    <p><?= $dataOrder['address'] ?></p>
                    <p><?= $dataOrder['namecitydestination'] . ', ' . $dataOrder['nameprovdestination'] . ', ' . $dataOrder['postal_code'] ?></p>
                    <p>Telp: <?= $dataOrder['address_phone'] ?></p>
                </div>
            </div>
        </div>
    </div>
    <hr>

    <h6><b>Pembayaran</b></h6>
    <div class="row">
        <div class="col-md-6 border-right">
            <p>Total Harga <?= '(' . $dataOrder['totaldetail'] . ' Barang)' ?></p>
            <p>Total Ongkos Kirim <?= '(' . OrderUtils::satuanKg($dataOrder['totalweight']) . ')' ?></p>
            <p>Total Bayar</p>
            <p>Metode Pembayaran</p>
        </div>
        <div class="col-md-6">
            <p><b><?= ProductHelper::idrPrice($dataOrder['totalprice']) ?></b></p>
            <p><b><?= ProductHelper::idrPrice($dataOrder['cost']) ?></b></p>
            <p><b><?= ProductHelper::idrPrice($dataOrder['orderprice']) ?></b></p>
            <p><b><?= $dataOrder['typepaymentname'] . ' (' . $dataOrder['paymentname'] . ') ' ?></b></p>
        </div>
    </div>
    <hr>

    <h6><b>Status Pesanan</b></h6>
    <?php
    if ($orderLog) {
        foreach ($orderLog as $perLog) {
            echo '
                    <div class="row">
                        <div class="col-md-2 border-right">
                            ' . OrderUtils::labelStatusOrder($perLog['status'], true) . '
                        </div>
                        <div class="col-md-2 border-right">
                            <p class="float-left">' . $perLog['datelog'] . '</p>
                        </div>
                        <div class="col-md-8">
                            <p>' . $perLog['note'] . '</p>
                        </div>
                    </div><hr>
                ';
        }
    }
    ?>

</div>


<div id="lightbox" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="width: fit-content;">
            <div class="modal-lighbox">
                <img src="" alt="" />
                <label class="lbl" style="padding-top: 10px;"></label>
            </div>
        </div>
    </div>
</div>