<?php

use app\models\Users;
use yii\web\View;

/* @var $this View */
/* @var $model Users */

$this->title = 'Detail Pesanan No : ' . $model['invoicekey'];
$this->params['breadcrumbs'][] = ['label' => 'Pesanan', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model['invoicekey'], 'url' => ['view', 'id' => $model['idorder']]];
?>
<div class="users-update">
    <?php
    if ($dataOrder) {
        echo $this->render('_detailOrder', [
            'dataOrder' => $dataOrder,
            'orderLog' => $orderLog, 
            'model' => $model, 
            'data' => $data, 
        ]);
    }
    ?>
</div>
