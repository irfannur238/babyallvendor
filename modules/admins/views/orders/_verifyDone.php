<?php

use richardfan\widget\JSRegister;
use yii\helpers\Html;
?>

<div class="row">
    <div class="col-lg-12">
        <div class="alert alert-info" role="alert" style="margin-top: 0px;">
            <p>Pesanan akan berstatus SELESAI secara otomatis dalam 14 x 24 jam dari tanggal pengiriman jika tidak dikonfirmasi oleh Pembeli. 
                Untuk mengganti status SELESAI secara manual, silahkan klik <a href="#" class="manual-done">Selesaikan Manual</a>, lalu klik tombol Selesai.</p>
        </div>

        <?=
        Html::a('Selesai', ['approve', 'id' => $dataOrder['idorder']], [
            'class' => 'btn btn-success float-right btn-manual-done',
            'data' => ['method' => 'post', 'confirm' => 'Apakah anda yakin pesanan sudah sampai ke pembeli. Pesanan akan diteruskan ke status SELESAI.'],
            'style' => 'display:none;'
        ]);
        ?>
    </div>
</div>

<?php JSRegister::begin() ?>
<script>
    $(document).ready(function (e) {
        $('.manual-done').on('click', function (event) {
            $('.btn-manual-done').show();
        });
    });
</script>
<?php JSRegister::end() ?>