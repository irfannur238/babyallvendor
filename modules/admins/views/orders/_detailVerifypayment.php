<?php

use app\helpers\ProductHelper;
use app\helpers\Url;
use yii\helpers\Html;
?>


<div id="accordion">
    <div class="card">
        <div class="card-header" id="headingOne" style="background-color: #ffc107;">
            <h5 class="mb-0">
                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="color: #000;font-weight: 700;">
                    Bukti Pembayaran
                </button>
            </h5>
        </div>

        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4 border-right">
                        <a href="#" class="fancybox" data-toggle="modal" data-target="#lightbox"><img class="card-img-top" src="<?= Url::getBaseImg($data['img'], $data['date']) ?>" style="height:200px"/></a>
                    </div>
                    <div class="col-md-8">
                        <b>Atas Nama : </b>
                        <p id="conveying-meaning-to-assistive-technologies"><?= $data['accountname'] ?></p>
                        <b>Nominal Transfer : </b>
                        <p id="conveying-meaning-to-assistive-technologies"><?= ProductHelper::idrPrice($data['nominal']) ?></p>
                        <b>Tanggal Upload : </b>
                        <p id="conveying-meaning-to-assistive-technologies"><?= $data['date'] ?></p>
                        <b>Catatan : </b>
                        <p><?= $data['note'] ?></p>
                    </div>
                </div><hr>
                <div class="btn-group float-right">
                    <?= '&nbsp;' . Html::a('Setujui', ['approve', 'id' => $data['idorder']], ['class' => 'btn btn-success', 'data' => ['method' => 'post', 'confirm' => 'Apakah anda yakin Setujui bukti pembayaran. Pesanan akan diteruskan ke status DIPROSES.']]) ?>
                    <?= '&nbsp;' . Html::a('Tolak', ['reject', 'id' => $data['idorder']], ['class' => 'btn btn-danger', 'data' => ['method' => 'post', 'confirm' => 'Apakah anda yakin Tolak bukti pembayaran. Pesanan akan dikembalikan ke status MENUNGGU PEMBAYARAN.']]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
