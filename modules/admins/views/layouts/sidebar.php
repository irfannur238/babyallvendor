<?php

use app\helpers\OrderUtils;
use app\helpers\Rolename;
use app\models\User;
use hail812\adminlte3\widgets\Menu;
use yii\helpers\Url;
?>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?= Url::home() ?>" class="brand-link">
        <img src="<?= $assetDir ?>/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light"><?= User::getStoreName() ?></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <!--        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img src="<?= $assetDir ?>/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
                    </div>
                    <div class="info">
                        <a href="#" class="d-block">Alexander Pierce</a>
                    </div>
                </div>-->

        <!-- Sidebar Menu -->
        <nav class="mt-2">

            <?php
            $countOrder = OrderUtils::countOrderByStatus();
            echo Menu::widget([
                'items' => [
//                    [
//                        'label' => 'Dashboard',
//                        'icon' => 'tachometer-alt',
//                        'badge' => '<span class="right badge badge-danger">New</span>'
//                    ],
                    ['label' => 'Produk', 'header' => true],
                    [
                        'label' => 'Produk',
                        'icon' => 'list-alt',
                        //'badge' => '<span class="right badge badge-info">2</span>',
                        'items' => [
                            ['label' => 'Semua Produk', 'url' => ['/admins/products/index'], 'iconStyle' => 'far'],
                            ['label' => 'Kategori', 'url' => ['/admins/categories'], 'iconStyle' => 'far'],
                        ]
                    ],
                    ['label' => 'Penjualan', 'header' => true],
                    //['label' => 'Login', 'url' => ['site/login'], 'icon' => 'sign-in-alt', 'visible' => Yii::$app->user->isGuest],
                    ['label' => 'Pesanan Masuk', 'icon' => 'cart-arrow-down', 'badge' => ($countOrder['waiting_payment'] ? '<span class="right badge badge-info">' . $countOrder['waiting_payment'] . '</span>' : false), 'url' => ['/admins/orders/dataorder', 'status' => OrderUtils::STAT_WAITING_PAYMENT]],
                    ['label' => 'Konfirmasi Bayar', 'icon' => 'money-bill-alt', 'badge' => ($countOrder['payment_verifycation'] ? '<span class="right badge badge-info">' . $countOrder['payment_verifycation'] . '</span>' : false), 'url' => ['/admins/orders/dataorder', 'status' => OrderUtils::STAT_PAYMENT_VERIFICATION]],
                    ['label' => 'Diproses', 'icon' => 'box-open', 'badge' => ($countOrder['on_proccess'] ? '<span class="right badge badge-info">' . $countOrder['on_proccess'] . '</span>' : false), 'url' => ['/admins/orders/dataorder', 'status' => OrderUtils::STAT_ONPROCESS]],
                    ['label' => 'Dikirim', 'icon' => 'shipping-fast', 'badge' => ($countOrder['on_shipping'] ? '<span class="right badge badge-info">' . $countOrder['on_shipping'] . '</span>' : false), 'url' => ['/admins/orders/dataorder', 'status' => OrderUtils::STAT_ONSHIPPING]],
                    ['label' => 'Selesai', 'icon' => 'handshake', 'badge' => ($countOrder['done'] ? '<span class="right badge badge-info">' . $countOrder['done'] . '</span>' : false), 'url' => ['/admins/orders/dataorder', 'status' => OrderUtils::STAT_DONE]],
                    ['label' => 'Semua Pesanan', 'icon' => 'shopping-cart', 'badge' => ($countOrder['all_order'] ? '<span class="right badge badge-warning">' . $countOrder['all_order'] . '</span>' : false), 'iconClassAdded' => 'text-warning', 'url' => ['/admins/orders/index']],
                    ['label' => 'User', 'header' => true],
                    ['label' => 'Customer', 'icon' => 'user-circle', 'url' => ['/admins/users', 'role' => Rolename::CUSTOMER]],
                    ['label' => 'Pengguna', 'icon' => 'user', 'url' => ['/admins/users', 'role' => Rolename::SUPER_ADMIN]],
                    ['label' => 'Pengaturan', 'header' => true],
                    [
                        'label' => 'Pengaturan',
                        'icon' => 'cogs',
                        'items' => [
                            ['label' => 'Toko', 'url' => ['/admins/stores/mystore'], 'iconStyle' => 'far'],
                            ['label' => 'Pembayaran', 'url' => ['/admins/payments'], 'iconStyle' => 'far'],
                            ['label' => 'Kurir', 'url' => ['/admins/couriers'], 'iconStyle' => 'far'],
                            ['label' => 'Find Us', 'url' => ['/admins/configweb'], 'iconStyle' => 'far'],
                        ]
                    ],
//                    [
//                        'label' => 'Level1',
//                        'items' => [
//                            ['label' => 'Level2', 'iconStyle' => 'far'],
//                            [
//                                'label' => 'Level2',
//                                'iconStyle' => 'far',
//                                'items' => [
//                                    ['label' => 'Level3', 'iconStyle' => 'far', 'icon' => 'dot-circle'],
//                                    ['label' => 'Level3', 'iconStyle' => 'far', 'icon' => 'dot-circle'],
//                                    ['label' => 'Level3', 'iconStyle' => 'far', 'icon' => 'dot-circle']
//                                ]
//                            ],
//                            ['label' => 'Level2', 'iconStyle' => 'far']
//                        ]
//                    ],
//                    ['label' => 'Penting', 'header' => true],
//                    [
//                        'label' => 'Konfirmasi Bayar',
//                        'iconStyle' => 'far',
//                        'badge' => '<span class="right badge badge-danger">2</span>',
//                        'iconClassAdded' => 'text-danger'
//                    ],
//                    [
//                        'label' => 'Perlu dikirim',
//                        'iconStyle' => 'far',
//                        'badge' => '<span class="right badge badge-warning">2</span>',
//                        'iconClassAdded' => 'text-warning'
//                    ],
                // ['label' => 'Informational', 'iconStyle' => 'far', 'iconClassAdded' => 'text-info'],
                ],
            ]);
            ?>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>