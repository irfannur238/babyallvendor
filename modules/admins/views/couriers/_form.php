<?php

use app\helpers\Dlist;
use app\helpers\Url;
use app\models\Couriers;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $model Couriers */
/* @var $form ActiveForm */
?>

<div class="couriers-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList(Dlist::dListStatusActive()) ?>

    <?= $form->field($model, 'note')->textInput(['maxlength' => true]) ?>

    <?php
    if (!$model->isNewRecord) {
        echo '<img class="myImg" src="' . Url::getImg($model->img, '/img/payment/') . '" style="max-height:50px">';
    }
    ?>
    <br><br>
    <?= $form->field($model, 'img')->fileInput()->label('Gambar'); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
