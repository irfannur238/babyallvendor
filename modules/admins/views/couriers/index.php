<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kurir';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="couriers-index">

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'idcourier',
            'name',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    return app\helpers\Dlist::badgeActive($model->status);
                }
            ],
            ['class' => 'app\widgets\ActionColumn', 'template' => '{view}{update}'],
        ],
    ]);
    ?>


</div>
