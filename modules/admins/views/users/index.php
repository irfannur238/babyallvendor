<?php

use app\helpers\Rolename;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $dataProvider ActiveDataProvider */
$title = $role == Rolename::CUSTOMER ? 'Customer' : 'Pengguna';
$this->title = $title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

<!--    <p class="float-right">
        <?= Html::a('Tambah', ['create'], ['class' => 'btn btn-success']) ?>
    </p>-->


    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'iduser',
            'username',
            //'password',
            'name',
            'email',
            'phone',
            //'lastlogin',
            //['class' => 'app\widgets\ActionColumn'],
        ],
    ]);
    ?>


</div>
