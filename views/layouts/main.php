<?php
/* @var $this View */
/* @var $content string */

use app\assets\AppAsset;
use app\helpers\OrderUtils;
use app\helpers\ProductHelper;
use app\helpers\Url;
use app\models\Categories;
use app\models\Couriers;
use app\models\Payments;
use app\models\User;
use kartik\alert\AlertBlock;
use richardfan\widget\JSRegister;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html as Html2;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\web\View;

AppAsset::register($this);
$css = "
    .pagination .page-item.active .page-link {
            background-color: #111213;
    border-color: #161617;
}

#snoAlertBox{
    position:absolute;
    z-index:1400;
   top:2%;
    right:4%;
    margin:0px auto;
	text-align:center;
    display:none;
}
";
$this->registerCss($css);
$route = ProductHelper::getRoute();
$actTemukankami = $route == 'apps/site/findus' ? 'class="active"' : '';
$actBeranda = $route == 'apps/p' ? 'class="active"' : '';
$actVerifypayment = $route == 'apps/customer/verifypayment' ? 'class="active"' : '';
$actCategory = Url::getController() . '/' . Url::getAction() == 'p/c' ? 'class="active"' : '';
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php $this->registerMetaTag(['name' => 'facebook-domain-verification', 'content' => 'zcllngcwu5mh099th74o19r4rmp2xw']) ?>
        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Cookie&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800;900&display=swap" rel="stylesheet">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <div id="preloder">
            <!--<div class="img-loader-q loader-zoom">
                <img src="<?= Url::base(true) . '/img/q_logo.png' ?>" style="max-width: 300%;">
            </div>-->
            <div class="loader"></div>
        </div>

        <!-- Offcanvas Menu Begin -->
        <div class="offcanvas-menu-overlay"></div>
        <div class="offcanvas-menu-wrapper" style="margin-top: -70px;">
            <div class="offcanvas__close">+</div>
            <ul class="offcanvas__widget" style="text-align: left;">
                <li><span class="icon_search search-switch"></span></li>
<!--                <li><a href="#"><span class="icon_heart_alt"></span><div class="tip">2</div></a></li>-->
                <li><?=
                    Html::a('<span class="icon_bag_alt"></span>
                                        <div class="tip total-qty-bag">' . OrderUtils::getTotalQtyCart() . '</div>',
                            '#',
                            ['url' => Url::to(['api/cartheader'], true),
                                'class' => 'btn-cart-header btn-modal']);
                    ?>
                </li>
            </ul>
            <div id="mobile-menu-wrap"></div>
            <div class="offcanvas__auth">
                <?php if (Yii::$app->user->isGuest) { ?>
                    <div class="header__right__auth">
                        <a href="<?= Url::base() . '/apps/site/login' ?>" id="log">Login</a>
                        <a href="<?= Url::base() . '/apps/site/register' ?>">Daftar</a>
                    </div>
                <?php } else { ?>
                    <a class="dropdown-toggle"
                       id="dropdownMenu4" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        Hi, <?= User::me()->name ?> 
                    </a>
                    <div class="dropdown-menu">
                        <?php
                        echo Html::a('Transaksi', ['/apps/customer/'], ['class' => 'dropdown-item']);
                        //echo Html::a('Wishlist', ['/apps/customer/wishlist'], ['class' => 'dropdown-item']);
                        echo Html::a('Pengaturan', ['/apps/customer/profile'], ['class' => 'dropdown-item']);
                        echo '<hr>';
                        echo Html::a('<i class="fas fa-sign-out-alt"></i> Logout', ['/apps/site/logout'], ['data-method' => 'post', 'class' => 'dropdown-item']);
                        ?>
                    </div>
                <?php } ?>
            </div>
        </div>
        <!-- Offcanvas Menu End -->

        <!-- Header Section Begin -->
        <header class="fixed-top header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-3 col-lg-2">
                        <div class="header__logo">
                            <a href="<?= Url::base() . '/apps' ?>"><img src="<?= Url::base(true) . '/img/logo.png' ?>" alt="" style="height: 45px;margin-left: 0px;"></a>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-7">
                        <nav class="header__menu">
                            <ul>
                                <li <?= $actBeranda ?>><a href="<?= Url::base() . '/apps' ?>">Beranda</a></li>
                                <li <?= $actCategory ?>><a href="#">Kategori</a>
                                    <ul class="dropdown">
                                        <?php
                                        $cat = Categories::find()->all();
                                        foreach ($cat as $per) {
                                            echo '<li><a href="' . Url::base() . '/apps/p/c?id=' . $per->idcategory . '">' . $per->name . '</a></li>';
                                        }
                                        ?>
                                    </ul>
                                </li>
                                <li  <?= $actTemukankami ?>><a href="<?= Url::base() . '/apps/site/findus' ?>">Temukan Kami</a></li>
                                <li  <?= $actVerifypayment ?>><a href="<?= Url::base() . '/apps/customer/verifypayment' ?>">Konfirmasi Bayar</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="col-lg-3">
                        <div class="header__right">
                            <?php if (Yii::$app->user->isGuest) { ?>
                                <div class="header__right__auth">
                                    <a href="<?= Url::base() . '/apps/site/login' ?>" id="log">Login</a>
                                    <a href="<?= Url::base() . '/apps/site/register' ?>">Daftar</a>
                                </div>
                            <?php } else { ?>
                                <div class="header__right__auth">
                                    <a class="dropdown-toggle"
                                       id="dropdownMenu4" data-toggle="dropdown"
                                       aria-haspopup="true" aria-expanded="false">
                                        Hi, <?= User::me()->name ?> 
                                    </a>
                                    <div class="dropdown-menu" style="background: #00000 !important">
                                        <?php
                                        echo Html::a('Transaksi', ['/apps/customer/'], ['class' => 'dropdown-item']);
                                        //echo Html::a('Wishlist', ['/apps/customer/wishlist'], ['class' => 'dropdown-item']);
                                        echo Html::a('Pengaturan', ['/apps/customer/profile'], ['class' => 'dropdown-item']);
                                        echo '<hr>';
                                        echo Html::a('<i class="fas fa-sign-out-alt"></i> Logout', ['/apps/site/logout'], ['data-method' => 'post', 'class' => 'dropdown-item']);
                                        ?>
                                    </div>
                                </div>
                            <?php } ?>
                            <ul class="header__right__widget">
                                <li><span class="icon_search search-switch"></span> </li>
                                <li><a href="#"><span class="icon_heart_alt"></span>
                                    </a></li>
                                <li>
                                    <?=
                                    Html::a('<span class="icon_bag_alt"></span>
                                        <div class="tip total-qty-bag">' . OrderUtils::getTotalQtyCart() . '</div>',
                                            '#',
                                            ['url' => Url::to(['api/cartheader'], true),
                                                'class' => 'btn-cart-header btn-modal']);
                                    ?>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="canvas__open">
                    <i class="fa fa-bars"></i>
                </div>
            </div>
        </header>
        <!-- Header Section End -->

        <div class="content-container" style="margin-top:80px">
            <?=
            Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ])
            ?>

            <?php
            Modal::begin([
                'title' => '<h6 class="modal-title">Keranjang</h6>',
                'id' => 'modal-form',
                'size' => 'modal-lg',
            ]);
            echo "<div id='modal-content'>Mohon tunggu ...</div>";
            Modal::end();
            ?>

            <?php //echo Alert::widget() ?>
            <?php
            echo AlertBlock::widget([
                'useSessionFlash' => true,
                'type' => AlertBlock::TYPE_GROWL,
                'delay' => false,
                'options' => [
                    'style' => 'top:200px;'
                ],
            ]);
            ?>
            <?= $content ?>
        </div>


        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-7">
                        <div class="footer__about">
                            <div class="footer__logo">
                                <a href="./index.html"></a>
                            </div>
                            <p>Cute, Comfy, High Quality.</p>
                            <div class="footer__payment">
                                <?php
                                $payment = Payments::find()->where(['idlevel' => 2, 'status' => 2])->all();
                                foreach ($payment as $perPay) {
                                    echo '<a href="#"><img src="' . Url::getImg($perPay->img, '/img/payment/') . '" alt="" style="max-width: 50px;"></a>';
                                }
                                ?>
                            </div>
                            <div class="footer__payment">
                                <?php
                                $couriers = Couriers::find()->where(['status' => 2])->all();
                                foreach ($couriers as $perPay) {
                                    echo '<a href="#"><img src="' . Url::getImg($perPay->img, '/img/payment/') . '" alt="" style="max-width: 50px;"></a>';
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-5">
                        <div class="footer__widget">
                            <h6>Menu</h6>
                            <ul>
                                <li><a href="<?= Url::base() . '/apps' ?>">Beranda</a></li>
                                <li><a href="<?= Url::base() . '/apps/site/findus' ?>">Temukan Kami</a></li>
                                <li><a href="<?= Url::base() . '/admins/' ?>">Login</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-4">
                        <div class="footer__widget">
                            <h6>Akun</h6>
                            <ul>
                                <li><a href="<?= Url::base() . '/apps/customer' ?>">Transaksi</a></li>
                                <li><a href="<?= Url::base() . '/apps/customer/verifypayment' ?>">Konfirmasi Bayar</a></li>
                                <li><a href="<?= Url::base() . '/apps/customer/profile' ?>">Profil</a></li>
                                <li><a href="<?= Url::base() . '/apps/customer/address' ?>">Alamat</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-8 col-sm-8">
                        <div class="footer__newslatter">
                            <h6>Subscribe</h6>
                            <form action="#">
                                <input type="text" placeholder="Email">
                                <button type="submit" class="site-btn">Subscribe</button>
                            </form>
                            <div class="footer__social">
<!--                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-youtube-play"></i></a>
                                <a href="#"><i class="fa fa-instagram"></i></a>-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer__copyright__text">
                            <p>Copyright &copy; <script>document.write(new Date().getFullYear());</script> All rights reserved | <?= User::getStoreName() ?> | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Search Begin -->
        <div class="search-model">
            <div class="h-100 d-flex align-items-center justify-content-center">
                <div class="search-close-switch">+</div>
                <?php
                $qsearch = ProductHelper::session('_QSEARCH');
                echo Html2::beginForm(
                        ['p/r'],
                        'post',
                        ['class' => 'search-model-form']
                );
                echo Html2::input('text', 'search', $qsearch, ['class' => 'search-input', 'placeholder' => 'Cari .....']);
                echo Html2::submitButton('Cari', ['class' => 'btn btn-primary btn-md', 'style' => 'background-color: black;']);
                echo Html2::endForm();
                ?>
                <!--                <form class="search-model-form">
                                    <input type="text" id="search-input" placeholder="Cari .....">
                                </form>-->
            </div>
        </div>
        <!-- Search End -->
        <?php $this->endBody() ?>

        <?php JSRegister::begin(); ?>
        <script>
            var _csrf = '<?= Yii::$app->request->csrfToken ?>';
            var datas = {_csrf};

            $('.btn-modal').click(function (e) {
                e.preventDefault();
                $('#modal-content').html('Mohon tunggu ...');
                $('#modal-form').modal('show');
                ajaxTotalQtyBag();

                $.ajax({
                    url: $(this).attr('url'),
                    method: 'post',
                    dataType: 'html',
                    data: datas,
                    success: function (data) {
                        $('#modal-content').html(data);
                    },
                    error: function (xhr, error) {
                        $('#modal-form').modal('hide');
                        alert('Gagal, silakan coba kembali.');
                    }

                });

            });

            function ajaxTotalQtyBag() {
                $.ajax({
                    url: "<?= Url::to(['api/ajax_total_qty_bag']) ?>",
                    type: "GET",
                    dataType: 'html',
                    success: function (data) {
                        $('.total-qty-bag').text(data);
                    },
                });
            }
        </script>
        <?php JSRegister::end(); ?>
    </body>
</html>
<?php $this->endPage() ?>
