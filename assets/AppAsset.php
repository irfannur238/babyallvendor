<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/site.css',
        'theme/css/bootstrap.min.css',
        'theme/css/font-awesome.min.css',
        'theme/css/elegant-icons.css',
        'theme/css/jquery-ui.min.css',
        'theme/css/magnific-popup.css',
        'theme/css/owl.carousel.min.css',
        'theme/css/slicknav.min.css',
        'theme/css/style.css',
        'https://use.fontawesome.com/releases/v5.3.1/css/all.css', 
    ];
    public $js = [
        'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.0.4/popper.js', 
        //'theme/js/jquery-3.3.1.min.js',
        'theme/js/bootstrap.min.js',
        'theme/js/jquery.magnific-popup.min.js',
        'theme/js/jquery-ui.min.js',
        'theme/js/mixitup.min.js',
        'theme/js/jquery.countdown.min.js',
        'theme/js/jquery.slicknav.js',
        'theme/js/owl.carousel.min.js',
        'theme/js/jquery.nicescroll.min.js',
        'theme/js/main.js', 
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];

}
