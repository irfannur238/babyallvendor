<?php

namespace app\helpers;

class DateUtils {

    public static function datetimeNow() {
        return date('Y-m-d H:i:s');
    }

    public static function dateNow() {
        return date('Y-m-d');
    }

}
