<?php

namespace app\helpers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\web\UnprocessableEntityHttpException;

class Url extends \yii\helpers\Url {

    const SECRET_KEY = 'Wvcvbv167476ERfgXSxfgfxg--_uioiyg';

    public static function baseAvatar($img = null) {
        $img = empty($img) ? 'ava.jpg' : $img;
        return Url::base(TRUE) . '/img/' . $img;
    }

    public static function baseImg($img = null) {
        return Url::base(TRUE) . '/img/' . $img;
    }

    public static function urlManager($url) {
        $act = Yii::$app->urlManager->createUrl([$url]);
        return $act;
    }

    public static function getPhoto($data) {
        return Url::base(TRUE) . '/uploads/img/' . $data;
    }

    public static function getImg($data, $path = null, $origin = false) {
        if ($path) {
            if ($origin) {
                return './' . $path . $data;
            }
            return Url::base(TRUE) . $path . $data;
        }
        if ($origin) {
            return './' . Url::base(TRUE) . '/img/categories/' . $data;
        }
        return Url::base(TRUE) . '/img/categories/' . $data;
    }

    public static function encrypt($data) {
        return base64_encode($data);
    }

    public static function decrypt($data) {
        return base64_decode($data);
    }

    public static function isExist($model, $msg = null) {
        if (!$model) {
            throw new NotFoundHttpException($msg ?: 'Halaman yang diminta tidak ada');
        }
        return TRUE;
    }

    public static function getBaseImg($data, $dataTime = null, $origin = false) {
        $paramTime = '2020-12-26';
        $paramTime = strtotime($paramTime);
        $dataTime = strtotime($dataTime);
        $dir = date('m_Y', $dataTime);
        $path = 'uploads/img/' . $dir . '/';
        $url = Url::base(TRUE) . '/';

        if ($dataTime == null) {
            return '-';
        }

        if ($origin == true) {
            return './' . $path . $data;
        }

        if ($dataTime < $paramTime) {
            return $url . 'uploads/img/' . $data;
        } else {
            if (is_dir($path)) {
                return $url . $path . $data;
            }
        }

        return '.-';
    }

    public static function setupBaseImg($dataTime) {
        $dir = date('m_Y', strtotime($dataTime));
        $path = 'uploads/img/' . $dir . '/';
        if (is_dir($path)) {
            return $path;
        } else {
            if (!mkdir($path, 0777, TRUE)) {
                throw new UnprocessableEntityHttpException(print_r('Failed Create Folder', true));
            } else {
                return $path;
            }
        }
    }

    public static function getModule() {
        return Yii::$app->controller->module->id;
    }

    public static function getController() {
        return Yii::$app->controller->id;
    }

    public static function getAction() {
        return Yii::$app->controller->action->id;
    }

    public static function getRoute() {
        return Yii::$app->requestedRoute;
    }

}
