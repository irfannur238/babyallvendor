<?php

namespace app\helpers;

use app\models\Orderdetails;
use app\models\Orderlog;
use app\models\Orders;
use app\models\Shippers;
use app\models\User;
use app\models\Users;
use app\models\Varianimg;
use app\models\Verifypayment;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\Json;

class OrderUtils {

    const _SESS_CART_NAME = '_ADDCART';
    const STAT_WAITING_PAYMENT = 1;
    const STAT_PAYMENT_VERIFICATION = 2;
    const STAT_PAYMENT_VERIFIED = 3;
    const STAT_ONPROCESS = 4;
    const STAT_ONSHIPPING = 5;
    const STAT_DONE = 6;
    const STAT_CANCELED = 99;
    const OUTOFSTOCK = 2;
    const READYSTOCK = null;

    public static function getDataCartSession() {
        $session = Utils::session(self::_SESS_CART_NAME);
        return $session ? json_decode($session, true) : [];
    }

    public static function getTotalPriceCart($items, $satuan = true) {
        $total = [];
        foreach ($items as $perItem) {
            $total[] = (int) $perItem['price'] * (int) $perItem['qty'];
        }
        $total = array_sum($total);
        return ($satuan ? ProductHelper::idrPrice($total) : $total);
    }

    public static function getTotalPriceCartNew($items, $satuan = true) {
        $total = [];
        $totalOriPrice = [];
        $totalDiscount = [];
        $totalQty = [];
        foreach ($items as $perItem) {
            if ($perItem['outofstock'] != self::OUTOFSTOCK) {
                $total[] = (int) $perItem['price'] * (int) $perItem['qty'];
                $totalOriPrice[] = (int) $perItem['oriPrice'] * (int) $perItem['qty'];
                $totalDiscount[] = (int) $perItem['discountPrice'] * (int) $perItem['qty'];
                $totalQty[] = $perItem['qty'];
            }
        }
        $total = $total ? array_sum($total) : 0;
        $totalOriPrice = $totalOriPrice ? array_sum($totalOriPrice) : 0;
        $totalDiscount = $totalDiscount ? array_sum($totalDiscount) : 0;
        $totalQty = $totalQty ? array_sum($totalQty) : 0;

        return [
            'total' => ($satuan ? ProductHelper::idrPrice($total) : $total),
            'totalOriPrice' => ($satuan ? ProductHelper::idrPrice($totalOriPrice) : $totalOriPrice),
            'totalDiscount' => ($satuan ? ProductHelper::idrPrice($totalDiscount) : $totalDiscount),
            'totalQty' => $totalQty,
        ];
    }

    public static function getTotalWeightCart($items) {
        $total = [];
        foreach ($items as $perItem) {
            $total[] = (int) $perItem['weight'] * (int) $perItem['qty'];
        }
        $total = array_sum($total);
        return $total;
    }

    public static function getTotalQtyCart($items = null) {
        $items = $items ? $items : self::getDataCartSession();
        $total = [];
        foreach ($items as $perItem) {
            $total[] = (int) $perItem['qty'];
        }
        $total = array_sum($total);
        return $total;
    }

    public static function removeAllCart() {
        Utils::removeSession(self::_SESS_CART_NAME);
    }

    public static function totalWeightPerItem($weight, $qty, $kg = true, $satuan = true) {
        $total = $weight * $qty;
        $total = $kg ? ($total / 1000) : $total;
        $total = $satuan ? ($kg ? $total . ' Kg' : $total . ' gram') : $total;
        return $total;
    }

    public static function totalPricePerItem($price, $qty, $satuan = true) {
        $total = $price * $qty;
        $total = $satuan ? ProductHelper::idrPrice($total) : $total;
        return $total;
    }

    public static function measurePrice($price, $qty) {
        return ((int) $price * (int) $qty);
    }

    public static function measureWeight($weight, $qty) {
        return ((int) $weight * (int) $qty);
    }

    public static function satuanKg($weight) {
        return ($weight ? (int) $weight / 1000 . ' Kg' : '-');
    }

    public static function summaryPriceTotal($price, $cost) {
        $total = $price + $cost;
        return $total;
    }

    public static function getOrderData($idcustomer = null, $status = null, $idorder = null, $model = null) {
        /*
         * data status type array []
         * if $idorder not null
         * return array key idorder
         */

        $m_order = (new Query())
                ->select(['o.*', 'sh.*', 'c.name as cus_name', 'c.lastname as cus_lastname'])
                ->from(['o' => Orders::tableName()])
                ->leftJoin(['sh' => Shippers::tableName()], 'o.idorder = sh.idorder')
                ->innerJoin(['c' => Users::tableName()], 'o.idcustomer = c.iduser AND c.role = 1')
                ->andFilterWhere(['o.idcustomer' => $idcustomer])
                ->andFilterWhere(['o.idorder' => $idorder])
                ->andFilterWhere(['IN', 'o.status', $status]); //array

        if ($model) {
            $m_order = $m_order
                    ->andFilterWhere(['like', 'UPPER(o.invoicekey)', strtoupper($model->invoicekey)])
                    ->andFilterWhere(['like', 'UPPER(o.orderdate)', strtoupper($model->orderdate)])
                    ->andFilterWhere(['like', 'UPPER(o.namevarian)', strtoupper($model->namevarian)])
                    ->andFilterWhere(['like', 'UPPER(o.totalqty)', strtoupper($model->totalqty)])
                    ->andFilterWhere(['like', 'UPPER(o.orderprice)', strtoupper(str_replace(['IDR', '.'], '', $model->orderprice))]);
        }
        $m_order = $m_order
                ->orderBy(['o.orderdate' => SORT_DESC])
                ->all();

        $m_orderdet = (new Query)
                ->select(['od.*', 'v.namevarian as varian'])
                ->from(['od' => Orderdetails::tableName()])
                ->leftJoin(['v' => Varianimg::tableName()], 'od.idvarian = v.idvarian AND v.type = ' . ProductHelper::TYPE_VARIAN)
                ->innerJoin(['o' => Orders::tableName()], 'o.idorder = od.idorder')
                ->andFilterWhere(['o.idcustomer' => $idcustomer])
                ->andFilterWhere(['o.idorder' => $idorder])
                ->andFilterWhere(['o.status' => $status])
                ->all();

        $data = [];
        foreach ($m_order as $perOrder) {
            $data[$perOrder['idorder']] = $perOrder;
            unset($data[$perOrder['idorder']]['username']);
            unset($data[$perOrder['idorder']]['password']);

            $totalitem = 0;
            foreach ($m_orderdet as $perDet) {
                if ($perDet['idorder'] == $perOrder['idorder']) {
                    $data[$perOrder['idorder']]['orderdetail'][] = $perDet;
                    $totalitem++;
                }
            }
            $data[$perOrder['idorder']]['totaldetail'] = $totalitem;
        }

        return ($idorder ? (isset($data[$idorder]) ? $data[$idorder] : []) : $data);
    }

    public static function htmlBadge($type, $text, $float = null) {
        return '<span class="badge ' . $type . ' ' . $float . ' badge-log">' . $text . '</span>';
    }

    public static function labelStatusOrder($status, $badge = false, $float = null) {
        $label = [
            self::STAT_WAITING_PAYMENT => 'Menunggu Pembayaran',
            self::STAT_PAYMENT_VERIFICATION => 'Verifikasi Pembayaran',
            self::STAT_PAYMENT_VERIFIED => 'Pembayaran Terverifikasi',
            self::STAT_ONPROCESS => 'Diproses',
            self::STAT_ONSHIPPING => 'Dikirim',
            self::STAT_DONE => 'Selesai',
            self::STAT_CANCELED => 'Pembelian Dibatalkan',
        ];

        $htmlBadge = [
            self::STAT_WAITING_PAYMENT => self::htmlBadge('badge-secondary', 'Menunggu Pembayaran', $float),
            self::STAT_PAYMENT_VERIFICATION => self::htmlBadge('badge-warning', 'Verifikasi Pembayaran', $float),
            self::STAT_PAYMENT_VERIFIED => self::htmlBadge('badge-secondary', 'Pembayaran Terverifikasi', $float),
            self::STAT_ONPROCESS => self::htmlBadge('badge-primary', 'Diproses', $float),
            self::STAT_ONSHIPPING => self::htmlBadge('badge-success', 'Dikirim', $float),
            self::STAT_DONE => self::htmlBadge('badge-info', 'Selesai', $float),
            self::STAT_CANCELED => self::htmlBadge('badge-warning', 'Pembelian Dibatalkan', $float),
        ];

        if ($badge == true) {
            return (isset($htmlBadge[$status]) ? $htmlBadge[$status] : '-');
        }
        return (isset($label[$status]) ? $label[$status] : '-');
    }

    public static function dropListMyOrder($status, $idcustomer = null) {
        $idcustomer = $idcustomer ? $idcustomer : User::me()->getId();
        $data = self::getOrderData($idcustomer, [$status]);
        $dropList = [];
        foreach ($data as $k => $perData) {
            $dropList[$perData['idorder']] = 'Tgl ' . $perData['orderdate'] . ' - ' . $perData['invoicekey'] . ' - ' . $perData['paymentname'];
        }
        return $dropList;
    }

    public static function saveOrderLog($order, $note = null) {
        $model = new Orderlog();
        $model->idlog = Utils::getNextID('idlog', Orderlog::tableName());
        $model->idorder = $order->idorder;
        $model->status = $order->status;
        $model->datelog = DateUtils::datetimeNow();
        $model->note = self::noteOrderLog($model->status);
        $model->save();
    }

    public static function noteOrderLog($status) {
        $note = [
            self::STAT_WAITING_PAYMENT => 'Menunggu konfirmasi pembayaran dari pembeli',
            self::STAT_PAYMENT_VERIFICATION => 'Menunggu verifikasi bukti bayar oleh penjual',
            self::STAT_PAYMENT_VERIFIED => 'Pembayaran telah diverifikasi',
            self::STAT_ONPROCESS => 'Pembelian sedang diproses atau disiapkan untuk dikirim',
            self::STAT_ONSHIPPING => 'Nomor resi sudah release dan pesanan sedang dalam proses pengiriman ke alamat pembeli',
            self::STAT_DONE => 'Pesanan telah diterima pembeli, transaksi selesai',
            self::STAT_CANCELED => 'Pesanan telah dibatalkan, karena belum dibayar',
        ];
        return (isset($note[$status]) ? $note[$status] : '-');
    }

    public static function textVarian($data, $totalQtyOrder = false) {
        $text = [];
        if ($data) {
            $no = 1;
            $totalQty = [];
            foreach ($data as $perData) {
                $text[] = '<p>' . $no++ . '. ' . $perData['namevarian'] . '  (' . $perData['qty'] . 'x)</p>';
                $totalQty[] = $perData['qty'];
            }
            if ($totalQtyOrder) {
                return array_sum($totalQty);
            }
            return implode('', $text);
        }
        return '';
    }

    public static function setStatusApprove($curStatus) {
        $res = [
            self::STAT_WAITING_PAYMENT => self::STAT_PAYMENT_VERIFICATION,
            self::STAT_PAYMENT_VERIFICATION => self::STAT_ONPROCESS,
            self::STAT_ONPROCESS => self::STAT_ONSHIPPING,
            self::STAT_ONSHIPPING => self::STAT_DONE,
        ];
        return (isset($res[$curStatus]) ? $res[$curStatus] : 0);
    }

    public static function setStatusReject($curStatus) {
        $res = [
            self::STAT_PAYMENT_VERIFICATION => self::STAT_WAITING_PAYMENT,
        ];
        return (isset($res[$curStatus]) ? $res[$curStatus] : 0);
    }

    public static function dataVerifypayment($idorder) {
        $data = Verifypayment::find()
                ->where(['idorder' => $idorder])
                ->andWhere(['IS', 'isarchived', new Expression('null')])
                ->asArray()
                ->orderBy(['date' => SORT_DESC])
                ->one();
        return $data;
    }

    public static function archiveVerifypayment($idverify) {
        $m_verifypayment = Verifypayment::findOne($idverify);
        if ($m_verifypayment) {
            $m_verifypayment->isarchived = ProductHelper::ISARCHIVED;
            $m_verifypayment->save();
        }
    }

    public static function updateReceiptShipper($id, $data) {
        $m_shipper = Shippers::findOne(['idorder' => $id]);
        if ($m_shipper) {
            $m_shipper->receiptnumber = $data->receiptnumber;
            $m_shipper->shipdate = $data->shipdate;
            $m_shipper->save();
        }
    }

    public static function countOrder($status = null) {
        $m_order = Orders::find()
                ->andFilterWhere(['status' => $status])
                ->count();
        return $m_order;
    }

    public static function countOrderByStatus() {
        $waiting_payment = self::countOrder(self::STAT_WAITING_PAYMENT);
        $payment_verifycation = self::countOrder(self::STAT_PAYMENT_VERIFICATION);
        $on_proccess = self::countOrder(self::STAT_ONPROCESS);
        $on_shipping = self::countOrder(self::STAT_ONSHIPPING);
        $done = self::countOrder(self::STAT_DONE);
        $allOrder = array_sum([
            $waiting_payment,
            $payment_verifycation,
            $on_proccess,
            $on_shipping,
            $done,
        ]);

        return [
            'waiting_payment' => $waiting_payment,
            'payment_verifycation' => $payment_verifycation,
            'on_proccess' => $on_proccess,
            'on_shipping' => $on_shipping,
            'done' => $done,
            'all_order' => $allOrder,
        ];
    }

    public static function cutQty($idvarian, $cut) {
        $model = Varianimg::findOne($idvarian);
        $fcut = $model->qty > 0 ? $model->qty - $cut : 0;
        $model->qty = ($fcut < 0 ? 0 : $fcut);
        $model->save();
    }

    public static function checkQtyCart() {
        $dataCart = self::getDataCartSession();
        $sessName = self::_SESS_CART_NAME;
        $resData = [];

        if ($dataCart) {
            foreach ($dataCart as $k => $perData) {
                $check = ProductHelper::getVarianByParam($perData['idproduct'], null, null, $perData['idvarian']);
                $currQty = $check ? $check['qty'] : 0;

                $resData[$k] = $perData;
                if ($currQty == 0) {
                    $resData[$k]['qty'] = 0;
                    $resData[$k]['outofstock'] = self::OUTOFSTOCK;
                } else if ($perData['qty'] > $currQty) {
                    $resData[$k]['qty'] = $currQty;
                    $resData[$k]['outofstock'] = self::READYSTOCK;
                } else if ($currQty > 0 && $perData['qty'] == 0) {
                    $resData[$k]['qty'] = 1;
                    $resData[$k]['outofstock'] = self::READYSTOCK;
                }
            }

            Utils::removeSession($sessName);
            Utils::session($sessName, Json::encode($resData));

            $resCart = OrderUtils::getDataCartSession();
            return $resCart;
        } else {
            return [];
        }
    }

    public static function returnQtyVarian($idorder) {
        $orderdet = Orderdetails::find()->where(['idorder' => $idorder])->all();
        if ($orderdet) {
            foreach ($orderdet as $det) {
                $m_varian = Varianimg::findOne(['idvarian' => $det->idvarian]);
                if ($m_varian) {
                    $tmpQty = $m_varian->qty;
                    $m_varian->qty = ($tmpQty + $det->qty);
                    $m_varian->save();
                }
            }
        }
    }

}
