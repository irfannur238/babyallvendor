<?php

namespace app\helpers;

use app\models\Discount;
use app\models\Varianimg;
use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class ProductHelper {

    const ISVARIAN = 2;
    const TYPE_VARIAN = 1;
    const TYPE_IMG = 2;
    const TYPE_SIZE = 3;
    const STAT_NONACTIVE = 1;
    const ISARCHIVED = 2;

    public static function idrPrice($price) {
        $price = Yii::$app->formatter->asCurrency($price);
        $price = substr($price, 0, -3);
        return $price;
    }

    public static function finalPrice($model) {
        if (!$model->discount) {
            return self::idrPrice($model->price);
        }
        $discount = ($model->discount / 100) * $model->price;
        $price = $model->price - $discount;
        return self::idrPrice($price) . '<span> ' . self::idrPrice($model->price) . '</span>';
    }

    public static function session($name, $value = null) {
        return $value ? Yii::$app->session->set($name, $value) : Yii::$app->session->get($name);
    }

    public static function getRoute() {
        return Yii::$app->requestedRoute;
    }

    public static function getVarianProduct($idproduct, $type = null, $orderBy = null, $param = null) {
        /**
         * @property $type string ex. self::TYPE_VARIAN, self::TYPE_IMG dll
         * @property $orderBy array ex. ['varianimg.namevarian' => SORT_ASC]
         * @property $param array ex. [['IS NOT', 'varianimg.size', new Expression('null')]]
         */
        $orderBy = $orderBy ? $orderBy : ['varianimg.namevarian' => SORT_ASC];
        $type = $type ? $type : self::TYPE_VARIAN;

        $var = Varianimg::find()
                ->leftJoin(['v' => varianimg::tableName()], '`varianimg`.`parent` = `v`.`idvarian`')
                ->select([
                    "varianimg.*", new Expression("case when varianimg.size is not null then CONCAT(varianimg.namevarian, ' (', varianimg.size, ')') else varianimg.namevarian end AS nameformat"),
                    "v.namevarian parentname",
                    new Expression("UPPER(varianimg.namevarian) labelvarian"),
                    new Expression("UPPER(varianimg.size) labelsize")
                ])
                ->where(['varianimg.idproduct' => $idproduct])
                ->andWhere(['varianimg.type' => $type]);

        $finalQuery = SqlQuery::andFilter($var, $param)
                ->orderBy($orderBy)
                ->asArray()
                ->all();
        return $finalQuery;
    }

    public static function getParentVarian($idproduct) {
        return [null => '--Tidak ada varian--'] + ArrayHelper::map(ProductHelper::getVarianProduct($idproduct), ['idvarian'], ['nameformat']);
    }

    public static function getDiscountProduct($idproduct, $getDiscount = false) {
        $var = Discount::find()
                ->where(['idproduct' => $idproduct])
                ->andWhere(['status' => 2])
                ->orderBy(['startdate' => SORT_ASC])
                ->asArray()
                ->all();
        $curDaytime = strtotime(date('Y-m-d H:i:s'));

        $data = [];
        $discount = [];
        foreach ($var as $k => $perDis) {
            if ($curDaytime >= strtotime($perDis['startdate']) && $curDaytime <= strtotime($perDis['enddate'])) {
                $discount[] = $perDis['percent'];
            }
            $data[] = $perDis;
            $data[$k]['startdate'] = date('d-m-Y H:i', strtotime($perDis['startdate']));
            $data[$k]['enddate'] = date('d-m-Y H:i', strtotime($perDis['enddate']));
        }
        if ($getDiscount) {
            return $discount ? $discount[0] : null; //get discount old yg masih aktif
        }
        return $data;
    }

    public static function finalPriceDiscount($percent, $price, $onlyDiscPrice = false) {
        $dis = ($percent / 100) * $price;
        $res = $price - $dis;
        if ($onlyDiscPrice) {
            return $dis;
        }
        return $res;
    }

    public static function getPrice($varian) {
        $price = [];
        $qty = [];
        $idproduct = 'x';
        if ($varian) {
            foreach ($varian as $perVar) {
                $price[] = $perVar['price'];
                $qty[] = $perVar['qty'];
                $idproduct = $perVar['idproduct'];
            }
        }

        $getPercentDiscount = self::getDiscountProduct($idproduct, true);
        if ($price) {
            $min = min($price);
            $max = max($price);
            if ($min == $max) {
                if ($getPercentDiscount) {
                    $formatPrice = self::idrPrice(self::finalPriceDiscount($getPercentDiscount, $max));
                } else {
                    $formatPrice = self::idrPrice($max);
                }
            } else {
                if ($getPercentDiscount) {
                    $maxDisc = self::finalPriceDiscount($getPercentDiscount, $max);
                    $minDisc = self::finalPriceDiscount($getPercentDiscount, $min);
                    $formatPrice = self::idrPrice($minDisc) . ' - ' . self::idrPrice($maxDisc);
                } else {
                    $formatPrice = self::idrPrice($min) . ' - ' . self::idrPrice($max);
                }
            }
        }
        return [
            'lowest' => ($price ? min($price) : 0),
            'higher' => ($price ? max($price) : 0),
            'formatPrice' => ($price ? $formatPrice : 0),
            'totalQty' => ($qty ? array_sum($qty) : 0), 
            'discount' => ($getPercentDiscount ? $getPercentDiscount : null), 
        ];
    }

    public static function genUniqueKey($id, $name, $size = null) {
        $sz = $size && Utils::trimChar($size) != '' ? ':' . $size : null;
        $res = $id . ':' . $name . $sz;
        return Utils::trimChar(strtoupper($res));
    }

    public static function textProductname($model, $actionName) {
        $nameProduct = $model->isNewRecord ? 'Tambah Produk' : ucwords(strtolower($model->productname));
        $result = $actionName . ' / ' . $nameProduct;
        return $result;
    }

    public static function getVarianByParam($idproduct, $varian, $size = null, $idvarian = null) {
        $result = Varianimg::find()
                ->where(['type' => ProductHelper::TYPE_VARIAN])
                ->andFilterWhere(['idproduct' => $idproduct])
                ->andFilterWhere(['idvarian' => $idvarian])
                ->andFilterWhere(['TRIM(UPPER(namevarian))' => trim(strtoupper($varian))])
                ->andFilterWhere(['TRIM(UPPER(size))' => trim(strtoupper($size))])
                ->asArray()
                ->one();
        $discount = self::varianDiscount($idproduct, $result['price']);
        $result['oriPrice'] = $result['price'];
        $result['price'] = $discount['finalPrice'];
        $result['percent'] = $discount['percent'];
        $result['discountPrice'] = $discount['discountPrice'];

        return $result;
    }

    public static function varianDiscount($idproduct, $varianPrice) {
        $getPercentDiscount = self::getDiscountProduct($idproduct, true);
        if ($getPercentDiscount) {
            $finalPrice = self::finalPriceDiscount($getPercentDiscount, $varianPrice);
        } else {
            $finalPrice = $varianPrice;
        }
        return [
            'finalPrice' => $finalPrice,
            'percent' => ($getPercentDiscount ? $getPercentDiscount : 0),
            'discountPrice' => ($varianPrice - $finalPrice),
        ];
    }

    public static function getImgByParam($idproduct, $varian, $size = null) {
        $result = (new Query())
                ->select(['*'])
                ->from(['v' => Varianimg::tableName()])
                ->leftJoin(['i' => Varianimg::tableName()], 'v.idvarian = i.parent AND i.type = 2')
                ->where(['is not', 'i.img', new Expression('null')])
                ->andFilterWhere(['v.idproduct' => $idproduct])
                ->andFilterWhere(['TRIM(UPPER(v.namevarian))' => trim(strtoupper($varian))])
                ->andFilterWhere(['TRIM(UPPER(v.size))' => trim(strtoupper($size))])
                ->one();
        return $result;
    }

}
