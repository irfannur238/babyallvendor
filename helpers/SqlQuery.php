<?php

namespace app\helpers;

class SqlQuery {

    public static function andFilter($query, $param) {
        if ($param) {
            foreach ($param as $r) {
                $query->andFilterWhere($r);
            }
        }
        return $query;
    }

}
