<?php

namespace app\helpers;

use app\models\Store;

class Configtype {

    const TENTANG_KAMI = 1;
    const CARA_ORDER = 2;
    const LINK_TOPED = 11;
    const LINK_SHOPEE = 12;
    const LINK_WEBSITE = 13;
    const LINK_WA = 14;
    const LINK_IG = 15;
    const FIND_US = 3;

    public static function arrConfigtype() {
        return [
            self::LINK_TOPED => 'LINK TOKOPEDIA STORE',
            self::LINK_SHOPEE => 'LINK SHOPEE STORE',
            self::LINK_WEBSITE => 'LINK WEBSITE STORE',
            self::LINK_WA => 'LINK WHATSAPP',
            self::LINK_IG => 'LINK INSTAGRAM',
            self::TENTANG_KAMI => 'TENTANG KAMI',
            self::CARA_ORDER => 'CARA ORDER'
        ];
    }
    
    public static function getStoreInfo() {
        $data = Store::findOne(['status' => Utils::STAT_ACTIVE]);
        return $data;
    }

}
