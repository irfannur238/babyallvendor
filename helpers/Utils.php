<?php

namespace app\helpers;

use Yii;
use yii\web\NotFoundHttpException;

class Utils {

    const resp_200 = '200';
    const resp_100 = '100';
    const STAT_ACTIVE = 2;
    const STAT_INACTIVE = null;

    public static function flashSuccess($text) {
        return Yii::$app->session->setFlash("success", $text);
    }

    public static function flashDanger($text) {
        return Yii::$app->session->setFlash("error", $text);
    }

    public static function flashWarning($text) {
        return Yii::$app->session->setFlash("warning", $text);
    }

    public static function getID() {
        return (string) round(microtime(true) * 1000);
    }

    public static function getNextID($id, $table) {
        $sql = "SELECT (COALESCE(MAX($id),0) + 1) FROM $table";
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public static function trimChar($text) {
        return preg_replace('/\s+/', '', $text);
    }

    public static function session($name, $value = null) {
        return $value ? Yii::$app->session->set($name, $value) : Yii::$app->session->get($name);
    }

    public static function removeSession($key) {
        return Yii::$app->session->remove($key);
    }

    public static function issetVarNull($var) {
        $result = isset($var) ? $var : null;
        return $result;
    }

    public static function req() {
        return Yii::$app->request;
    }

    public static function randDigit($digit = null) {
        $digits = $digit ? $digit : 3;
        return str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT);
    }

    public static function getCharBeforeFind($string, $find) {
        return strtok($string, $find);
    }

    public static function isExist($model, $msg = null) {
        if (!$model) {
            throw new NotFoundHttpException($msg ?: 'Halaman yang diminta tidak ada');
        }
        return TRUE;
    }

}
