<?php

namespace app\helpers;

use app\models\Categories;
use app\models\Cities;
use app\models\Couriers;
use app\models\Provinces;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class Dlist {

    public static function dListCategory() {
        $cat = Categories::find()->all();
        return ArrayHelper::map($cat, 'idcategory', 'name');
    }

    public static function dListStatusActive() {
        return [
            2 => 'Aktif',
            1 => 'Non Aktif',
        ];
    }

    public static function dListIsfeatured() {
        return [
            2 => 'Tampilkan',
            1 => 'Tidak',
        ];
    }

    public static function dListIsvarian() {
        return [
            1 => 'Tidak Ada',
            2 => 'Terdapat Pilihan',
        ];
    }

    public static function badgeActive($status) {
        $label = [
            2 => OrderUtils::htmlBadge('badge-success', 'Aktif'),
            1 => OrderUtils::htmlBadge('badge-danger', 'Non Aktif'),
        ];

        return isset($label[$status]) ? $label[$status] : '-';
    }

    public static function dListVarian($idproduct, $isSize = false) {
        $varian = ProductHelper::getVarianProduct($idproduct, ProductHelper::TYPE_VARIAN, ['qty' => SORT_DESC]);
        $size = ProductHelper::getVarianProduct($idproduct, ProductHelper::TYPE_VARIAN, ['qty' => SORT_DESC], [['IS NOT', 'varianimg.size', new Expression('null')]]);

        $result = $isSize == false ? ArrayHelper::map($varian, ['labelvarian'], ['labelvarian']) : ArrayHelper::map($size, ['labelsize'], ['labelsize']);
        return $result;
    }

    public static function dListAddress($type = null) {
        $provinces = ArrayHelper::map(Provinces::find()->all(), 'province_id', 'province');
        $cities = ArrayHelper::map(Cities::find()->all(), 'city_id', 'city_name');

        return $type == 'city' ? $cities : $provinces;
    }

    public static function dListCourier() {
        $cou = Couriers::find()->where(['status' => 2])->all();
        return ArrayHelper::map($cou, 'idcourier', 'name');
    }

    public static function dListProvince() {
        $provinces = ArrayHelper::map(Provinces::find()->all(), 'province_id', 'province');
        return $provinces;
    }

    public static function dListCity($province_id = NULL) {
        $cities = ArrayHelper::map(Cities::find()->andFilterWhere(['province_id' => $province_id])->all(), 'city_id', 'city_name');
        return $cities;
    }

}
