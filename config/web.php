<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'id',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'modules' => [
        'apps' => [
            'class' => 'app\modules\apps\Module',
        ],
        'admins' => [
            'class' => 'app\modules\admins\Module',
        ],
        'ckeditor' => [
            'class' => 'wadeshuler\ckeditor\Module',
            //'controllerNamespace' => 'wadeshuler\ckeditor\controllers\default',    // to override my controller
            'preset' => 'full-all',    // default: basic - options: basic, standard, standard-all, full, full-all
            //'customCdn' => '//cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.10/ckeditor.js',    // must point to ckeditor.js
            'uploadDir' => '@app/web/uploads/imgCkeditor', // must be file path (required when using filebrowser*BrowseUrl below)
            'uploadUrl' => '@web/uploads/imgCkeditor', // must be valid URL (required when using filebrowser*BrowseUrl below)
            // how to add external plugins (must also list them in `widgetClientOptions` `extraPlugins` below)
            //'widgetExternalPlugins' => [
            //    ['name' => 'pluginname', 'path' => '/path/to/', 'file' => 'plugin.js'],
            //    ['name' => 'pluginname2', 'path' => '/path/to2/', 'file' => 'plugin.js'],
            //],
            // passes html options to the text area tag itself. Mostly useless as CKEditor hides the <textarea> and uses it's own div
            //'widgetOptions' => [
            //    'rows' => '10',
            //],
            // These are basically passed to the `CKEDITOR.replace()`
            'widgetClientOptions' => [
            //'skin' => 'moono',    // must be in skins directory
            //'skin' => 'kama,
            //'extraPlugins' => 'abbr,inserthtml',     // list of externalPlugins (loaded from `widgetExternalPlugins` above)
            //'customConfig' => '@web/js/myconfig.js',
            //'filebrowserBrowseUrl' => '/ckeditor/default/file-browse',
            //'filebrowserUploadUrl' => '/ckeditor/default/file-upload',
            //'filebrowserImageBrowseUrl' => '/ckeditor/default/image-browse',
            //'filebrowserImageUploadUrl' => '/ckeditor/default/image-upload',
            ]
        ],
        'gridview' => [
            'class' => '\kartik\grid\Module'
        // enter optional module parameters below - only if you need to  
        // use your own export download action or custom translation 
        // message source
        // 'downloadAction' => 'gridview/export/download',
        // 'i18n' => []
        ]
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'uEEZWN5qEk8utzbkjIxor-zBmCADpXtx',
            /*
            'csrfParam' => '_csrf-backend',
            'csrfCookie' => [
                'httpOnly' => true,
            ],
             * 
             */
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['apps/site/login'],
        ],
        //'homeUrl' =>  'apps/p',
        'errorHandler' => [
            'errorAction' => 'apps/site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'mail.beibeestore.com',
                'username' => 'beibee@beibeestore.com',
                'password' => 'Beibee2021!',
                'port' => '465',
                'encryption' => 'ssl',
                'streamOptions' => [
                    'ssl' => [
                        'allow_self_signed' => true,
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                    ],
                ],
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/' => 'apps/p/',
                '/admin' => 'admins/products', 
                '/findus' => 'apps/site/findus', 
            ],
        ],
        'i18n' => [
            'translations' => [
                'kvgrid' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@vendor/kartik-v/yii2-grid/src/messages',
                ],
            ]
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'nullDisplay' => '-',
            'dateFormat' => 'dd-MM-yyyy',
            'datetimeFormat' => 'dd-MM-yyyy HH:mm',
            'decimalSeparator' => ',',
            'thousandSeparator' => '.',
            'currencyCode' => 'IDR',
            'locale' => 'id_ID.utf8',
        ],
//        'assetManager' => [
//            'bundles' => [
//                'yii\web\JqueryAsset' => [
//                    'sourcePath' => null, // do not publish the bundle
//                    'js' => [
//                        'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.full.min.js',
//                        'https://cdn.jsdelivr.net/gh/kartik-v/yii2-widget-select2@2.1.0/assets/js/select2-krajee.min.js',
//                    ]
//                ],
//                'kartik\form\ActiveFormAsset' => [
//                    'bsDependencyEnabled' => false // do not load bootstrap assets for a specific asset bundle
//                ],
//            ],
//        ],
//        'view' => [
//            'theme' => [
//                'pathMap' => [
//                    '@app/views' => '@vendor/hail812/yii2-adminlte3/src/views'
//                ],
//            ],
//        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
            // uncomment the following to add your IP if you are not connecting from localhost.
            //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
            // uncomment the following to add your IP if you are not connecting from localhost.
            //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
