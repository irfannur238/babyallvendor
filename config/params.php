<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'hail812/yii2-adminlte3' => [
        'pluginMap' => [
            'sweetalert2' => [
                'css' => 'sweetalert2-theme-bootstrap-4/bootstrap-4.min.css',
                'js' => 'sweetalert2/sweetalert2.min.js'
            ],
            'toastr' => [
                'css' => ['toastr/toastr.min.css'],
                'js' => ['toastr/toastr.min.js']
            ],
        ]
    ],
    'bsVersion' => '4.x',
    'bsDependencyEnabled' => false,
    'gridConfig' => [
        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => '-'],
        'autoXlFormat' => true,
        'export' => [
            'showConfirmAlert' => false,
            'target' => kartik\grid\GridView::TARGET_BLANK
        ],
        'exportConfig' => [kartik\grid\GridView::PDF => [], kartik\grid\GridView::CSV => []],
        'pjax' => true,
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'responsiveWrap' => false,
        'hover' => true,
        'pager' => [
            'options' => ['class' => 'pagination'], // set clas name used in ui list of pagination
            'prevPageLabel' => '<', // Set the label for the "previous" page button
            'nextPageLabel' => '>', // Set the label for the "next" page button
            'firstPageLabel' => '<<', // Set the label for the "first" page button
            'lastPageLabel' => '>>', // Set the label for the "last" page button
            'nextPageCssClass' => 'next', // Set CSS class for the "next" page button
            'prevPageCssClass' => 'prev', // Set CSS class for the "previous" page button
            'firstPageCssClass' => 'first', // Set CSS class for the "first" page button
            'lastPageCssClass' => 'last', // Set CSS class for the "last" page button
        ],
        'floatHeader' => false,
        "resizableColumns" => false,
        'persistResize' => false,
        'resizeStorageKey' => 'ecomm-' . date("m"),
        'panel' => [
            'heading' => false,
            'footer' => false,
            'type' => kartik\grid\GridView::TYPE_DEFAULT
        ],
        'panelBeforeTemplate' => '{toolbarContainer}<div class="clearfix"></div>',
        'panelTemplate' => '
        <div class="panel panel-{type}">
            {panelBefore}
            {items}
            <div class="panel-pager">
            {summary} {pager}
                <div class="clearfix"></div>
            </div>
        </div>'
    ]
];
