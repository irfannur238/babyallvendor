-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 21, 2021 at 08:41 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecomm`
--

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE `addresses` (
  `idaddress` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `province_id` varchar(10) NOT NULL,
  `province_name` varchar(50) DEFAULT NULL,
  `city_id` varchar(20) NOT NULL,
  `city_name` varchar(50) DEFAULT NULL,
  `postal_code` varchar(10) NOT NULL,
  `address` varchar(225) NOT NULL,
  `description` varchar(225) DEFAULT NULL,
  `note` varchar(225) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `district` varchar(50) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`idaddress`, `iduser`, `province_id`, `province_name`, `city_id`, `city_name`, `postal_code`, `address`, `description`, `note`, `phone`, `status`, `district`, `name`) VALUES
(2, 4, '10', 'Jawa Tengah', '497', 'Wonogiri', '57692', 'Janggan rt 002/ 001,  kec. Jatiroto', NULL, NULL, '085338325183', 2, NULL, NULL),
(3, 1, '9', 'Jawa Barat', '171', 'Karawang', '57692', 'Janggan RT 02/01 Jatiroto Wonogiri (selatan gedung MTA cab. jatiroto) ', NULL, NULL, '085338325183', 2, 'Jatiroto', 'Rumah');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `idcategory` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`idcategory`, `name`, `description`) VALUES
(1, 'Pakaian Pria', NULL),
(2, 'Pakaian Wanita', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `city_id` varchar(30) NOT NULL,
  `province_id` varchar(10) NOT NULL,
  `province` varchar(50) NOT NULL,
  `type` varchar(30) NOT NULL,
  `city_name` varchar(50) NOT NULL,
  `postal_code` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`city_id`, `province_id`, `province`, `type`, `city_name`, `postal_code`) VALUES
('1', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Barat', 23681),
('10', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Timur', 24454),
('100', '19', 'Maluku', 'Kabupaten', 'Buru Selatan', 97351),
('101', '30', 'Sulawesi Tenggara', 'Kabupaten', 'Buton', 93754),
('102', '30', 'Sulawesi Tenggara', 'Kabupaten', 'Buton Utara', 93745),
('103', '9', 'Jawa Barat', 'Kabupaten', 'Ciamis', 46211),
('104', '9', 'Jawa Barat', 'Kabupaten', 'Cianjur', 43217),
('105', '10', 'Jawa Tengah', 'Kabupaten', 'Cilacap', 53211),
('106', '3', 'Banten', 'Kota', 'Cilegon', 42417),
('107', '9', 'Jawa Barat', 'Kota', 'Cimahi', 40512),
('108', '9', 'Jawa Barat', 'Kabupaten', 'Cirebon', 45611),
('109', '9', 'Jawa Barat', 'Kota', 'Cirebon', 45116),
('11', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Utara', 24382),
('110', '34', 'Sumatera Utara', 'Kabupaten', 'Dairi', 22211),
('111', '24', 'Papua', 'Kabupaten', 'Deiyai (Deliyai)', 98784),
('112', '34', 'Sumatera Utara', 'Kabupaten', 'Deli Serdang', 20511),
('113', '10', 'Jawa Tengah', 'Kabupaten', 'Demak', 59519),
('114', '1', 'Bali', 'Kota', 'Denpasar', 80227),
('115', '9', 'Jawa Barat', 'Kota', 'Depok', 16416),
('116', '32', 'Sumatera Barat', 'Kabupaten', 'Dharmasraya', 27612),
('117', '24', 'Papua', 'Kabupaten', 'Dogiyai', 98866),
('118', '22', 'Nusa Tenggara Barat (NTB)', 'Kabupaten', 'Dompu', 84217),
('119', '29', 'Sulawesi Tengah', 'Kabupaten', 'Donggala', 94341),
('12', '32', 'Sumatera Barat', 'Kabupaten', 'Agam', 26411),
('120', '26', 'Riau', 'Kota', 'Dumai', 28811),
('121', '33', 'Sumatera Selatan', 'Kabupaten', 'Empat Lawang', 31811),
('122', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Ende', 86351),
('123', '28', 'Sulawesi Selatan', 'Kabupaten', 'Enrekang', 91719),
('124', '25', 'Papua Barat', 'Kabupaten', 'Fakfak', 98651),
('125', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Flores Timur', 86213),
('126', '9', 'Jawa Barat', 'Kabupaten', 'Garut', 44126),
('127', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Gayo Lues', 24653),
('128', '1', 'Bali', 'Kabupaten', 'Gianyar', 80519),
('129', '7', 'Gorontalo', 'Kabupaten', 'Gorontalo', 96218),
('13', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Alor', 85811),
('130', '7', 'Gorontalo', 'Kota', 'Gorontalo', 96115),
('131', '7', 'Gorontalo', 'Kabupaten', 'Gorontalo Utara', 96611),
('132', '28', 'Sulawesi Selatan', 'Kabupaten', 'Gowa', 92111),
('133', '11', 'Jawa Timur', 'Kabupaten', 'Gresik', 61115),
('134', '10', 'Jawa Tengah', 'Kabupaten', 'Grobogan', 58111),
('135', '5', 'DI Yogyakarta', 'Kabupaten', 'Gunung Kidul', 55812),
('136', '14', 'Kalimantan Tengah', 'Kabupaten', 'Gunung Mas', 74511),
('137', '34', 'Sumatera Utara', 'Kota', 'Gunungsitoli', 22813),
('138', '20', 'Maluku Utara', 'Kabupaten', 'Halmahera Barat', 97757),
('139', '20', 'Maluku Utara', 'Kabupaten', 'Halmahera Selatan', 97911),
('14', '19', 'Maluku', 'Kota', 'Ambon', 97222),
('140', '20', 'Maluku Utara', 'Kabupaten', 'Halmahera Tengah', 97853),
('141', '20', 'Maluku Utara', 'Kabupaten', 'Halmahera Timur', 97862),
('142', '20', 'Maluku Utara', 'Kabupaten', 'Halmahera Utara', 97762),
('143', '13', 'Kalimantan Selatan', 'Kabupaten', 'Hulu Sungai Selatan', 71212),
('144', '13', 'Kalimantan Selatan', 'Kabupaten', 'Hulu Sungai Tengah', 71313),
('145', '13', 'Kalimantan Selatan', 'Kabupaten', 'Hulu Sungai Utara', 71419),
('146', '34', 'Sumatera Utara', 'Kabupaten', 'Humbang Hasundutan', 22457),
('147', '26', 'Riau', 'Kabupaten', 'Indragiri Hilir', 29212),
('148', '26', 'Riau', 'Kabupaten', 'Indragiri Hulu', 29319),
('149', '9', 'Jawa Barat', 'Kabupaten', 'Indramayu', 45214),
('15', '34', 'Sumatera Utara', 'Kabupaten', 'Asahan', 21214),
('150', '24', 'Papua', 'Kabupaten', 'Intan Jaya', 98771),
('151', '6', 'DKI Jakarta', 'Kota', 'Jakarta Barat', 11220),
('152', '6', 'DKI Jakarta', 'Kota', 'Jakarta Pusat', 10540),
('153', '6', 'DKI Jakarta', 'Kota', 'Jakarta Selatan', 12230),
('154', '6', 'DKI Jakarta', 'Kota', 'Jakarta Timur', 13330),
('155', '6', 'DKI Jakarta', 'Kota', 'Jakarta Utara', 14140),
('156', '8', 'Jambi', 'Kota', 'Jambi', 36111),
('157', '24', 'Papua', 'Kabupaten', 'Jayapura', 99352),
('158', '24', 'Papua', 'Kota', 'Jayapura', 99114),
('159', '24', 'Papua', 'Kabupaten', 'Jayawijaya', 99511),
('16', '24', 'Papua', 'Kabupaten', 'Asmat', 99777),
('160', '11', 'Jawa Timur', 'Kabupaten', 'Jember', 68113),
('161', '1', 'Bali', 'Kabupaten', 'Jembrana', 82251),
('162', '28', 'Sulawesi Selatan', 'Kabupaten', 'Jeneponto', 92319),
('163', '10', 'Jawa Tengah', 'Kabupaten', 'Jepara', 59419),
('164', '11', 'Jawa Timur', 'Kabupaten', 'Jombang', 61415),
('165', '25', 'Papua Barat', 'Kabupaten', 'Kaimana', 98671),
('166', '26', 'Riau', 'Kabupaten', 'Kampar', 28411),
('167', '14', 'Kalimantan Tengah', 'Kabupaten', 'Kapuas', 73583),
('168', '12', 'Kalimantan Barat', 'Kabupaten', 'Kapuas Hulu', 78719),
('169', '10', 'Jawa Tengah', 'Kabupaten', 'Karanganyar', 57718),
('17', '1', 'Bali', 'Kabupaten', 'Badung', 80351),
('170', '1', 'Bali', 'Kabupaten', 'Karangasem', 80819),
('171', '9', 'Jawa Barat', 'Kabupaten', 'Karawang', 41311),
('172', '17', 'Kepulauan Riau', 'Kabupaten', 'Karimun', 29611),
('173', '34', 'Sumatera Utara', 'Kabupaten', 'Karo', 22119),
('174', '14', 'Kalimantan Tengah', 'Kabupaten', 'Katingan', 74411),
('175', '4', 'Bengkulu', 'Kabupaten', 'Kaur', 38911),
('176', '12', 'Kalimantan Barat', 'Kabupaten', 'Kayong Utara', 78852),
('177', '10', 'Jawa Tengah', 'Kabupaten', 'Kebumen', 54319),
('178', '11', 'Jawa Timur', 'Kabupaten', 'Kediri', 64184),
('179', '11', 'Jawa Timur', 'Kota', 'Kediri', 64125),
('18', '13', 'Kalimantan Selatan', 'Kabupaten', 'Balangan', 71611),
('180', '24', 'Papua', 'Kabupaten', 'Keerom', 99461),
('181', '10', 'Jawa Tengah', 'Kabupaten', 'Kendal', 51314),
('182', '30', 'Sulawesi Tenggara', 'Kota', 'Kendari', 93126),
('183', '4', 'Bengkulu', 'Kabupaten', 'Kepahiang', 39319),
('184', '17', 'Kepulauan Riau', 'Kabupaten', 'Kepulauan Anambas', 29991),
('185', '19', 'Maluku', 'Kabupaten', 'Kepulauan Aru', 97681),
('186', '32', 'Sumatera Barat', 'Kabupaten', 'Kepulauan Mentawai', 25771),
('187', '26', 'Riau', 'Kabupaten', 'Kepulauan Meranti', 28791),
('188', '31', 'Sulawesi Utara', 'Kabupaten', 'Kepulauan Sangihe', 95819),
('189', '6', 'DKI Jakarta', 'Kabupaten', 'Kepulauan Seribu', 14550),
('19', '15', 'Kalimantan Timur', 'Kota', 'Balikpapan', 76111),
('190', '31', 'Sulawesi Utara', 'Kabupaten', 'Kepulauan Siau Tagulandang Biaro (Sitaro)', 95862),
('191', '20', 'Maluku Utara', 'Kabupaten', 'Kepulauan Sula', 97995),
('192', '31', 'Sulawesi Utara', 'Kabupaten', 'Kepulauan Talaud', 95885),
('193', '24', 'Papua', 'Kabupaten', 'Kepulauan Yapen (Yapen Waropen)', 98211),
('194', '8', 'Jambi', 'Kabupaten', 'Kerinci', 37167),
('195', '12', 'Kalimantan Barat', 'Kabupaten', 'Ketapang', 78874),
('196', '10', 'Jawa Tengah', 'Kabupaten', 'Klaten', 57411),
('197', '1', 'Bali', 'Kabupaten', 'Klungkung', 80719),
('198', '30', 'Sulawesi Tenggara', 'Kabupaten', 'Kolaka', 93511),
('199', '30', 'Sulawesi Tenggara', 'Kabupaten', 'Kolaka Utara', 93911),
('2', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Barat Daya', 23764),
('20', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kota', 'Banda Aceh', 23238),
('200', '30', 'Sulawesi Tenggara', 'Kabupaten', 'Konawe', 93411),
('201', '30', 'Sulawesi Tenggara', 'Kabupaten', 'Konawe Selatan', 93811),
('202', '30', 'Sulawesi Tenggara', 'Kabupaten', 'Konawe Utara', 93311),
('203', '13', 'Kalimantan Selatan', 'Kabupaten', 'Kotabaru', 72119),
('204', '31', 'Sulawesi Utara', 'Kota', 'Kotamobagu', 95711),
('205', '14', 'Kalimantan Tengah', 'Kabupaten', 'Kotawaringin Barat', 74119),
('206', '14', 'Kalimantan Tengah', 'Kabupaten', 'Kotawaringin Timur', 74364),
('207', '26', 'Riau', 'Kabupaten', 'Kuantan Singingi', 29519),
('208', '12', 'Kalimantan Barat', 'Kabupaten', 'Kubu Raya', 78311),
('209', '10', 'Jawa Tengah', 'Kabupaten', 'Kudus', 59311),
('21', '18', 'Lampung', 'Kota', 'Bandar Lampung', 35139),
('210', '5', 'DI Yogyakarta', 'Kabupaten', 'Kulon Progo', 55611),
('211', '9', 'Jawa Barat', 'Kabupaten', 'Kuningan', 45511),
('212', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Kupang', 85362),
('213', '23', 'Nusa Tenggara Timur (NTT)', 'Kota', 'Kupang', 85119),
('214', '15', 'Kalimantan Timur', 'Kabupaten', 'Kutai Barat', 75711),
('215', '15', 'Kalimantan Timur', 'Kabupaten', 'Kutai Kartanegara', 75511),
('216', '15', 'Kalimantan Timur', 'Kabupaten', 'Kutai Timur', 75611),
('217', '34', 'Sumatera Utara', 'Kabupaten', 'Labuhan Batu', 21412),
('218', '34', 'Sumatera Utara', 'Kabupaten', 'Labuhan Batu Selatan', 21511),
('219', '34', 'Sumatera Utara', 'Kabupaten', 'Labuhan Batu Utara', 21711),
('22', '9', 'Jawa Barat', 'Kabupaten', 'Bandung', 40311),
('220', '33', 'Sumatera Selatan', 'Kabupaten', 'Lahat', 31419),
('221', '14', 'Kalimantan Tengah', 'Kabupaten', 'Lamandau', 74611),
('222', '11', 'Jawa Timur', 'Kabupaten', 'Lamongan', 64125),
('223', '18', 'Lampung', 'Kabupaten', 'Lampung Barat', 34814),
('224', '18', 'Lampung', 'Kabupaten', 'Lampung Selatan', 35511),
('225', '18', 'Lampung', 'Kabupaten', 'Lampung Tengah', 34212),
('226', '18', 'Lampung', 'Kabupaten', 'Lampung Timur', 34319),
('227', '18', 'Lampung', 'Kabupaten', 'Lampung Utara', 34516),
('228', '12', 'Kalimantan Barat', 'Kabupaten', 'Landak', 78319),
('229', '34', 'Sumatera Utara', 'Kabupaten', 'Langkat', 20811),
('23', '9', 'Jawa Barat', 'Kota', 'Bandung', 40111),
('230', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kota', 'Langsa', 24412),
('231', '24', 'Papua', 'Kabupaten', 'Lanny Jaya', 99531),
('232', '3', 'Banten', 'Kabupaten', 'Lebak', 42319),
('233', '4', 'Bengkulu', 'Kabupaten', 'Lebong', 39264),
('234', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Lembata', 86611),
('235', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kota', 'Lhokseumawe', 24352),
('236', '32', 'Sumatera Barat', 'Kabupaten', 'Lima Puluh Koto/Kota', 26671),
('237', '17', 'Kepulauan Riau', 'Kabupaten', 'Lingga', 29811),
('238', '22', 'Nusa Tenggara Barat (NTB)', 'Kabupaten', 'Lombok Barat', 83311),
('239', '22', 'Nusa Tenggara Barat (NTB)', 'Kabupaten', 'Lombok Tengah', 83511),
('24', '9', 'Jawa Barat', 'Kabupaten', 'Bandung Barat', 40721),
('240', '22', 'Nusa Tenggara Barat (NTB)', 'Kabupaten', 'Lombok Timur', 83612),
('241', '22', 'Nusa Tenggara Barat (NTB)', 'Kabupaten', 'Lombok Utara', 83711),
('242', '33', 'Sumatera Selatan', 'Kota', 'Lubuk Linggau', 31614),
('243', '11', 'Jawa Timur', 'Kabupaten', 'Lumajang', 67319),
('244', '28', 'Sulawesi Selatan', 'Kabupaten', 'Luwu', 91994),
('245', '28', 'Sulawesi Selatan', 'Kabupaten', 'Luwu Timur', 92981),
('246', '28', 'Sulawesi Selatan', 'Kabupaten', 'Luwu Utara', 92911),
('247', '11', 'Jawa Timur', 'Kabupaten', 'Madiun', 63153),
('248', '11', 'Jawa Timur', 'Kota', 'Madiun', 63122),
('249', '10', 'Jawa Tengah', 'Kabupaten', 'Magelang', 56519),
('25', '29', 'Sulawesi Tengah', 'Kabupaten', 'Banggai', 94711),
('250', '10', 'Jawa Tengah', 'Kota', 'Magelang', 56133),
('251', '11', 'Jawa Timur', 'Kabupaten', 'Magetan', 63314),
('252', '9', 'Jawa Barat', 'Kabupaten', 'Majalengka', 45412),
('253', '27', 'Sulawesi Barat', 'Kabupaten', 'Majene', 91411),
('254', '28', 'Sulawesi Selatan', 'Kota', 'Makassar', 90111),
('255', '11', 'Jawa Timur', 'Kabupaten', 'Malang', 65163),
('256', '11', 'Jawa Timur', 'Kota', 'Malang', 65112),
('257', '16', 'Kalimantan Utara', 'Kabupaten', 'Malinau', 77511),
('258', '19', 'Maluku', 'Kabupaten', 'Maluku Barat Daya', 97451),
('259', '19', 'Maluku', 'Kabupaten', 'Maluku Tengah', 97513),
('26', '29', 'Sulawesi Tengah', 'Kabupaten', 'Banggai Kepulauan', 94881),
('260', '19', 'Maluku', 'Kabupaten', 'Maluku Tenggara', 97651),
('261', '19', 'Maluku', 'Kabupaten', 'Maluku Tenggara Barat', 97465),
('262', '27', 'Sulawesi Barat', 'Kabupaten', 'Mamasa', 91362),
('263', '24', 'Papua', 'Kabupaten', 'Mamberamo Raya', 99381),
('264', '24', 'Papua', 'Kabupaten', 'Mamberamo Tengah', 99553),
('265', '27', 'Sulawesi Barat', 'Kabupaten', 'Mamuju', 91519),
('266', '27', 'Sulawesi Barat', 'Kabupaten', 'Mamuju Utara', 91571),
('267', '31', 'Sulawesi Utara', 'Kota', 'Manado', 95247),
('268', '34', 'Sumatera Utara', 'Kabupaten', 'Mandailing Natal', 22916),
('269', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Manggarai', 86551),
('27', '2', 'Bangka Belitung', 'Kabupaten', 'Bangka', 33212),
('270', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Manggarai Barat', 86711),
('271', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Manggarai Timur', 86811),
('272', '25', 'Papua Barat', 'Kabupaten', 'Manokwari', 98311),
('273', '25', 'Papua Barat', 'Kabupaten', 'Manokwari Selatan', 98355),
('274', '24', 'Papua', 'Kabupaten', 'Mappi', 99853),
('275', '28', 'Sulawesi Selatan', 'Kabupaten', 'Maros', 90511),
('276', '22', 'Nusa Tenggara Barat (NTB)', 'Kota', 'Mataram', 83131),
('277', '25', 'Papua Barat', 'Kabupaten', 'Maybrat', 98051),
('278', '34', 'Sumatera Utara', 'Kota', 'Medan', 20228),
('279', '12', 'Kalimantan Barat', 'Kabupaten', 'Melawi', 78619),
('28', '2', 'Bangka Belitung', 'Kabupaten', 'Bangka Barat', 33315),
('280', '8', 'Jambi', 'Kabupaten', 'Merangin', 37319),
('281', '24', 'Papua', 'Kabupaten', 'Merauke', 99613),
('282', '18', 'Lampung', 'Kabupaten', 'Mesuji', 34911),
('283', '18', 'Lampung', 'Kota', 'Metro', 34111),
('284', '24', 'Papua', 'Kabupaten', 'Mimika', 99962),
('285', '31', 'Sulawesi Utara', 'Kabupaten', 'Minahasa', 95614),
('286', '31', 'Sulawesi Utara', 'Kabupaten', 'Minahasa Selatan', 95914),
('287', '31', 'Sulawesi Utara', 'Kabupaten', 'Minahasa Tenggara', 95995),
('288', '31', 'Sulawesi Utara', 'Kabupaten', 'Minahasa Utara', 95316),
('289', '11', 'Jawa Timur', 'Kabupaten', 'Mojokerto', 61382),
('29', '2', 'Bangka Belitung', 'Kabupaten', 'Bangka Selatan', 33719),
('290', '11', 'Jawa Timur', 'Kota', 'Mojokerto', 61316),
('291', '29', 'Sulawesi Tengah', 'Kabupaten', 'Morowali', 94911),
('292', '33', 'Sumatera Selatan', 'Kabupaten', 'Muara Enim', 31315),
('293', '8', 'Jambi', 'Kabupaten', 'Muaro Jambi', 36311),
('294', '4', 'Bengkulu', 'Kabupaten', 'Muko Muko', 38715),
('295', '30', 'Sulawesi Tenggara', 'Kabupaten', 'Muna', 93611),
('296', '14', 'Kalimantan Tengah', 'Kabupaten', 'Murung Raya', 73911),
('297', '33', 'Sumatera Selatan', 'Kabupaten', 'Musi Banyuasin', 30719),
('298', '33', 'Sumatera Selatan', 'Kabupaten', 'Musi Rawas', 31661),
('299', '24', 'Papua', 'Kabupaten', 'Nabire', 98816),
('3', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Besar', 23951),
('30', '2', 'Bangka Belitung', 'Kabupaten', 'Bangka Tengah', 33613),
('300', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Nagan Raya', 23674),
('301', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Nagekeo', 86911),
('302', '17', 'Kepulauan Riau', 'Kabupaten', 'Natuna', 29711),
('303', '24', 'Papua', 'Kabupaten', 'Nduga', 99541),
('304', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Ngada', 86413),
('305', '11', 'Jawa Timur', 'Kabupaten', 'Nganjuk', 64414),
('306', '11', 'Jawa Timur', 'Kabupaten', 'Ngawi', 63219),
('307', '34', 'Sumatera Utara', 'Kabupaten', 'Nias', 22876),
('308', '34', 'Sumatera Utara', 'Kabupaten', 'Nias Barat', 22895),
('309', '34', 'Sumatera Utara', 'Kabupaten', 'Nias Selatan', 22865),
('31', '11', 'Jawa Timur', 'Kabupaten', 'Bangkalan', 69118),
('310', '34', 'Sumatera Utara', 'Kabupaten', 'Nias Utara', 22856),
('311', '16', 'Kalimantan Utara', 'Kabupaten', 'Nunukan', 77421),
('312', '33', 'Sumatera Selatan', 'Kabupaten', 'Ogan Ilir', 30811),
('313', '33', 'Sumatera Selatan', 'Kabupaten', 'Ogan Komering Ilir', 30618),
('314', '33', 'Sumatera Selatan', 'Kabupaten', 'Ogan Komering Ulu', 32112),
('315', '33', 'Sumatera Selatan', 'Kabupaten', 'Ogan Komering Ulu Selatan', 32211),
('316', '33', 'Sumatera Selatan', 'Kabupaten', 'Ogan Komering Ulu Timur', 32312),
('317', '11', 'Jawa Timur', 'Kabupaten', 'Pacitan', 63512),
('318', '32', 'Sumatera Barat', 'Kota', 'Padang', 25112),
('319', '34', 'Sumatera Utara', 'Kabupaten', 'Padang Lawas', 22763),
('32', '1', 'Bali', 'Kabupaten', 'Bangli', 80619),
('320', '34', 'Sumatera Utara', 'Kabupaten', 'Padang Lawas Utara', 22753),
('321', '32', 'Sumatera Barat', 'Kota', 'Padang Panjang', 27122),
('322', '32', 'Sumatera Barat', 'Kabupaten', 'Padang Pariaman', 25583),
('323', '34', 'Sumatera Utara', 'Kota', 'Padang Sidempuan', 22727),
('324', '33', 'Sumatera Selatan', 'Kota', 'Pagar Alam', 31512),
('325', '34', 'Sumatera Utara', 'Kabupaten', 'Pakpak Bharat', 22272),
('326', '14', 'Kalimantan Tengah', 'Kota', 'Palangka Raya', 73112),
('327', '33', 'Sumatera Selatan', 'Kota', 'Palembang', 30111),
('328', '28', 'Sulawesi Selatan', 'Kota', 'Palopo', 91911),
('329', '29', 'Sulawesi Tengah', 'Kota', 'Palu', 94111),
('33', '13', 'Kalimantan Selatan', 'Kabupaten', 'Banjar', 70619),
('330', '11', 'Jawa Timur', 'Kabupaten', 'Pamekasan', 69319),
('331', '3', 'Banten', 'Kabupaten', 'Pandeglang', 42212),
('332', '9', 'Jawa Barat', 'Kabupaten', 'Pangandaran', 46511),
('333', '28', 'Sulawesi Selatan', 'Kabupaten', 'Pangkajene Kepulauan', 90611),
('334', '2', 'Bangka Belitung', 'Kota', 'Pangkal Pinang', 33115),
('335', '24', 'Papua', 'Kabupaten', 'Paniai', 98765),
('336', '28', 'Sulawesi Selatan', 'Kota', 'Parepare', 91123),
('337', '32', 'Sumatera Barat', 'Kota', 'Pariaman', 25511),
('338', '29', 'Sulawesi Tengah', 'Kabupaten', 'Parigi Moutong', 94411),
('339', '32', 'Sumatera Barat', 'Kabupaten', 'Pasaman', 26318),
('34', '9', 'Jawa Barat', 'Kota', 'Banjar', 46311),
('340', '32', 'Sumatera Barat', 'Kabupaten', 'Pasaman Barat', 26511),
('341', '15', 'Kalimantan Timur', 'Kabupaten', 'Paser', 76211),
('342', '11', 'Jawa Timur', 'Kabupaten', 'Pasuruan', 67153),
('343', '11', 'Jawa Timur', 'Kota', 'Pasuruan', 67118),
('344', '10', 'Jawa Tengah', 'Kabupaten', 'Pati', 59114),
('345', '32', 'Sumatera Barat', 'Kota', 'Payakumbuh', 26213),
('346', '25', 'Papua Barat', 'Kabupaten', 'Pegunungan Arfak', 98354),
('347', '24', 'Papua', 'Kabupaten', 'Pegunungan Bintang', 99573),
('348', '10', 'Jawa Tengah', 'Kabupaten', 'Pekalongan', 51161),
('349', '10', 'Jawa Tengah', 'Kota', 'Pekalongan', 51122),
('35', '13', 'Kalimantan Selatan', 'Kota', 'Banjarbaru', 70712),
('350', '26', 'Riau', 'Kota', 'Pekanbaru', 28112),
('351', '26', 'Riau', 'Kabupaten', 'Pelalawan', 28311),
('352', '10', 'Jawa Tengah', 'Kabupaten', 'Pemalang', 52319),
('353', '34', 'Sumatera Utara', 'Kota', 'Pematang Siantar', 21126),
('354', '15', 'Kalimantan Timur', 'Kabupaten', 'Penajam Paser Utara', 76311),
('355', '18', 'Lampung', 'Kabupaten', 'Pesawaran', 35312),
('356', '18', 'Lampung', 'Kabupaten', 'Pesisir Barat', 35974),
('357', '32', 'Sumatera Barat', 'Kabupaten', 'Pesisir Selatan', 25611),
('358', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Pidie', 24116),
('359', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Pidie Jaya', 24186),
('36', '13', 'Kalimantan Selatan', 'Kota', 'Banjarmasin', 70117),
('360', '28', 'Sulawesi Selatan', 'Kabupaten', 'Pinrang', 91251),
('361', '7', 'Gorontalo', 'Kabupaten', 'Pohuwato', 96419),
('362', '27', 'Sulawesi Barat', 'Kabupaten', 'Polewali Mandar', 91311),
('363', '11', 'Jawa Timur', 'Kabupaten', 'Ponorogo', 63411),
('364', '12', 'Kalimantan Barat', 'Kabupaten', 'Pontianak', 78971),
('365', '12', 'Kalimantan Barat', 'Kota', 'Pontianak', 78112),
('366', '29', 'Sulawesi Tengah', 'Kabupaten', 'Poso', 94615),
('367', '33', 'Sumatera Selatan', 'Kota', 'Prabumulih', 31121),
('368', '18', 'Lampung', 'Kabupaten', 'Pringsewu', 35719),
('369', '11', 'Jawa Timur', 'Kabupaten', 'Probolinggo', 67282),
('37', '10', 'Jawa Tengah', 'Kabupaten', 'Banjarnegara', 53419),
('370', '11', 'Jawa Timur', 'Kota', 'Probolinggo', 67215),
('371', '14', 'Kalimantan Tengah', 'Kabupaten', 'Pulang Pisau', 74811),
('372', '20', 'Maluku Utara', 'Kabupaten', 'Pulau Morotai', 97771),
('373', '24', 'Papua', 'Kabupaten', 'Puncak', 98981),
('374', '24', 'Papua', 'Kabupaten', 'Puncak Jaya', 98979),
('375', '10', 'Jawa Tengah', 'Kabupaten', 'Purbalingga', 53312),
('376', '9', 'Jawa Barat', 'Kabupaten', 'Purwakarta', 41119),
('377', '10', 'Jawa Tengah', 'Kabupaten', 'Purworejo', 54111),
('378', '25', 'Papua Barat', 'Kabupaten', 'Raja Ampat', 98489),
('379', '4', 'Bengkulu', 'Kabupaten', 'Rejang Lebong', 39112),
('38', '28', 'Sulawesi Selatan', 'Kabupaten', 'Bantaeng', 92411),
('380', '10', 'Jawa Tengah', 'Kabupaten', 'Rembang', 59219),
('381', '26', 'Riau', 'Kabupaten', 'Rokan Hilir', 28992),
('382', '26', 'Riau', 'Kabupaten', 'Rokan Hulu', 28511),
('383', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Rote Ndao', 85982),
('384', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kota', 'Sabang', 23512),
('385', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Sabu Raijua', 85391),
('386', '10', 'Jawa Tengah', 'Kota', 'Salatiga', 50711),
('387', '15', 'Kalimantan Timur', 'Kota', 'Samarinda', 75133),
('388', '12', 'Kalimantan Barat', 'Kabupaten', 'Sambas', 79453),
('389', '34', 'Sumatera Utara', 'Kabupaten', 'Samosir', 22392),
('39', '5', 'DI Yogyakarta', 'Kabupaten', 'Bantul', 55715),
('390', '11', 'Jawa Timur', 'Kabupaten', 'Sampang', 69219),
('391', '12', 'Kalimantan Barat', 'Kabupaten', 'Sanggau', 78557),
('392', '24', 'Papua', 'Kabupaten', 'Sarmi', 99373),
('393', '8', 'Jambi', 'Kabupaten', 'Sarolangun', 37419),
('394', '32', 'Sumatera Barat', 'Kota', 'Sawah Lunto', 27416),
('395', '12', 'Kalimantan Barat', 'Kabupaten', 'Sekadau', 79583),
('396', '28', 'Sulawesi Selatan', 'Kabupaten', 'Selayar (Kepulauan Selayar)', 92812),
('397', '4', 'Bengkulu', 'Kabupaten', 'Seluma', 38811),
('398', '10', 'Jawa Tengah', 'Kabupaten', 'Semarang', 50511),
('399', '10', 'Jawa Tengah', 'Kota', 'Semarang', 50135),
('4', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Jaya', 23654),
('40', '33', 'Sumatera Selatan', 'Kabupaten', 'Banyuasin', 30911),
('400', '19', 'Maluku', 'Kabupaten', 'Seram Bagian Barat', 97561),
('401', '19', 'Maluku', 'Kabupaten', 'Seram Bagian Timur', 97581),
('402', '3', 'Banten', 'Kabupaten', 'Serang', 42182),
('403', '3', 'Banten', 'Kota', 'Serang', 42111),
('404', '34', 'Sumatera Utara', 'Kabupaten', 'Serdang Bedagai', 20915),
('405', '14', 'Kalimantan Tengah', 'Kabupaten', 'Seruyan', 74211),
('406', '26', 'Riau', 'Kabupaten', 'Siak', 28623),
('407', '34', 'Sumatera Utara', 'Kota', 'Sibolga', 22522),
('408', '28', 'Sulawesi Selatan', 'Kabupaten', 'Sidenreng Rappang/Rapang', 91613),
('409', '11', 'Jawa Timur', 'Kabupaten', 'Sidoarjo', 61219),
('41', '10', 'Jawa Tengah', 'Kabupaten', 'Banyumas', 53114),
('410', '29', 'Sulawesi Tengah', 'Kabupaten', 'Sigi', 94364),
('411', '32', 'Sumatera Barat', 'Kabupaten', 'Sijunjung (Sawah Lunto Sijunjung)', 27511),
('412', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Sikka', 86121),
('413', '34', 'Sumatera Utara', 'Kabupaten', 'Simalungun', 21162),
('414', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Simeulue', 23891),
('415', '12', 'Kalimantan Barat', 'Kota', 'Singkawang', 79117),
('416', '28', 'Sulawesi Selatan', 'Kabupaten', 'Sinjai', 92615),
('417', '12', 'Kalimantan Barat', 'Kabupaten', 'Sintang', 78619),
('418', '11', 'Jawa Timur', 'Kabupaten', 'Situbondo', 68316),
('419', '5', 'DI Yogyakarta', 'Kabupaten', 'Sleman', 55513),
('42', '11', 'Jawa Timur', 'Kabupaten', 'Banyuwangi', 68416),
('420', '32', 'Sumatera Barat', 'Kabupaten', 'Solok', 27365),
('421', '32', 'Sumatera Barat', 'Kota', 'Solok', 27315),
('422', '32', 'Sumatera Barat', 'Kabupaten', 'Solok Selatan', 27779),
('423', '28', 'Sulawesi Selatan', 'Kabupaten', 'Soppeng', 90812),
('424', '25', 'Papua Barat', 'Kabupaten', 'Sorong', 98431),
('425', '25', 'Papua Barat', 'Kota', 'Sorong', 98411),
('426', '25', 'Papua Barat', 'Kabupaten', 'Sorong Selatan', 98454),
('427', '10', 'Jawa Tengah', 'Kabupaten', 'Sragen', 57211),
('428', '9', 'Jawa Barat', 'Kabupaten', 'Subang', 41215),
('429', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kota', 'Subulussalam', 24882),
('43', '13', 'Kalimantan Selatan', 'Kabupaten', 'Barito Kuala', 70511),
('430', '9', 'Jawa Barat', 'Kabupaten', 'Sukabumi', 43311),
('431', '9', 'Jawa Barat', 'Kota', 'Sukabumi', 43114),
('432', '14', 'Kalimantan Tengah', 'Kabupaten', 'Sukamara', 74712),
('433', '10', 'Jawa Tengah', 'Kabupaten', 'Sukoharjo', 57514),
('434', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Sumba Barat', 87219),
('435', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Sumba Barat Daya', 87453),
('436', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Sumba Tengah', 87358),
('437', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Sumba Timur', 87112),
('438', '22', 'Nusa Tenggara Barat (NTB)', 'Kabupaten', 'Sumbawa', 84315),
('439', '22', 'Nusa Tenggara Barat (NTB)', 'Kabupaten', 'Sumbawa Barat', 84419),
('44', '14', 'Kalimantan Tengah', 'Kabupaten', 'Barito Selatan', 73711),
('440', '9', 'Jawa Barat', 'Kabupaten', 'Sumedang', 45326),
('441', '11', 'Jawa Timur', 'Kabupaten', 'Sumenep', 69413),
('442', '8', 'Jambi', 'Kota', 'Sungaipenuh', 37113),
('443', '24', 'Papua', 'Kabupaten', 'Supiori', 98164),
('444', '11', 'Jawa Timur', 'Kota', 'Surabaya', 60119),
('445', '10', 'Jawa Tengah', 'Kota', 'Surakarta (Solo)', 57113),
('446', '13', 'Kalimantan Selatan', 'Kabupaten', 'Tabalong', 71513),
('447', '1', 'Bali', 'Kabupaten', 'Tabanan', 82119),
('448', '28', 'Sulawesi Selatan', 'Kabupaten', 'Takalar', 92212),
('449', '25', 'Papua Barat', 'Kabupaten', 'Tambrauw', 98475),
('45', '14', 'Kalimantan Tengah', 'Kabupaten', 'Barito Timur', 73671),
('450', '16', 'Kalimantan Utara', 'Kabupaten', 'Tana Tidung', 77611),
('451', '28', 'Sulawesi Selatan', 'Kabupaten', 'Tana Toraja', 91819),
('452', '13', 'Kalimantan Selatan', 'Kabupaten', 'Tanah Bumbu', 72211),
('453', '32', 'Sumatera Barat', 'Kabupaten', 'Tanah Datar', 27211),
('454', '13', 'Kalimantan Selatan', 'Kabupaten', 'Tanah Laut', 70811),
('455', '3', 'Banten', 'Kabupaten', 'Tangerang', 15914),
('456', '3', 'Banten', 'Kota', 'Tangerang', 15111),
('457', '3', 'Banten', 'Kota', 'Tangerang Selatan', 15332),
('458', '18', 'Lampung', 'Kabupaten', 'Tanggamus', 35619),
('459', '34', 'Sumatera Utara', 'Kota', 'Tanjung Balai', 21321),
('46', '14', 'Kalimantan Tengah', 'Kabupaten', 'Barito Utara', 73881),
('460', '8', 'Jambi', 'Kabupaten', 'Tanjung Jabung Barat', 36513),
('461', '8', 'Jambi', 'Kabupaten', 'Tanjung Jabung Timur', 36719),
('462', '17', 'Kepulauan Riau', 'Kota', 'Tanjung Pinang', 29111),
('463', '34', 'Sumatera Utara', 'Kabupaten', 'Tapanuli Selatan', 22742),
('464', '34', 'Sumatera Utara', 'Kabupaten', 'Tapanuli Tengah', 22611),
('465', '34', 'Sumatera Utara', 'Kabupaten', 'Tapanuli Utara', 22414),
('466', '13', 'Kalimantan Selatan', 'Kabupaten', 'Tapin', 71119),
('467', '16', 'Kalimantan Utara', 'Kota', 'Tarakan', 77114),
('468', '9', 'Jawa Barat', 'Kabupaten', 'Tasikmalaya', 46411),
('469', '9', 'Jawa Barat', 'Kota', 'Tasikmalaya', 46116),
('47', '28', 'Sulawesi Selatan', 'Kabupaten', 'Barru', 90719),
('470', '34', 'Sumatera Utara', 'Kota', 'Tebing Tinggi', 20632),
('471', '8', 'Jambi', 'Kabupaten', 'Tebo', 37519),
('472', '10', 'Jawa Tengah', 'Kabupaten', 'Tegal', 52419),
('473', '10', 'Jawa Tengah', 'Kota', 'Tegal', 52114),
('474', '25', 'Papua Barat', 'Kabupaten', 'Teluk Bintuni', 98551),
('475', '25', 'Papua Barat', 'Kabupaten', 'Teluk Wondama', 98591),
('476', '10', 'Jawa Tengah', 'Kabupaten', 'Temanggung', 56212),
('477', '20', 'Maluku Utara', 'Kota', 'Ternate', 97714),
('478', '20', 'Maluku Utara', 'Kota', 'Tidore Kepulauan', 97815),
('479', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Timor Tengah Selatan', 85562),
('48', '17', 'Kepulauan Riau', 'Kota', 'Batam', 29413),
('480', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Timor Tengah Utara', 85612),
('481', '34', 'Sumatera Utara', 'Kabupaten', 'Toba Samosir', 22316),
('482', '29', 'Sulawesi Tengah', 'Kabupaten', 'Tojo Una-Una', 94683),
('483', '29', 'Sulawesi Tengah', 'Kabupaten', 'Toli-Toli', 94542),
('484', '24', 'Papua', 'Kabupaten', 'Tolikara', 99411),
('485', '31', 'Sulawesi Utara', 'Kota', 'Tomohon', 95416),
('486', '28', 'Sulawesi Selatan', 'Kabupaten', 'Toraja Utara', 91831),
('487', '11', 'Jawa Timur', 'Kabupaten', 'Trenggalek', 66312),
('488', '19', 'Maluku', 'Kota', 'Tual', 97612),
('489', '11', 'Jawa Timur', 'Kabupaten', 'Tuban', 62319),
('49', '10', 'Jawa Tengah', 'Kabupaten', 'Batang', 51211),
('490', '18', 'Lampung', 'Kabupaten', 'Tulang Bawang', 34613),
('491', '18', 'Lampung', 'Kabupaten', 'Tulang Bawang Barat', 34419),
('492', '11', 'Jawa Timur', 'Kabupaten', 'Tulungagung', 66212),
('493', '28', 'Sulawesi Selatan', 'Kabupaten', 'Wajo', 90911),
('494', '30', 'Sulawesi Tenggara', 'Kabupaten', 'Wakatobi', 93791),
('495', '24', 'Papua', 'Kabupaten', 'Waropen', 98269),
('496', '18', 'Lampung', 'Kabupaten', 'Way Kanan', 34711),
('497', '10', 'Jawa Tengah', 'Kabupaten', 'Wonogiri', 57619),
('498', '10', 'Jawa Tengah', 'Kabupaten', 'Wonosobo', 56311),
('499', '24', 'Papua', 'Kabupaten', 'Yahukimo', 99041),
('5', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Selatan', 23719),
('50', '8', 'Jambi', 'Kabupaten', 'Batang Hari', 36613),
('500', '24', 'Papua', 'Kabupaten', 'Yalimo', 99481),
('501', '5', 'DI Yogyakarta', 'Kota', 'Yogyakarta', 55111),
('51', '11', 'Jawa Timur', 'Kota', 'Batu', 65311),
('52', '34', 'Sumatera Utara', 'Kabupaten', 'Batu Bara', 21655),
('53', '30', 'Sulawesi Tenggara', 'Kota', 'Bau-Bau', 93719),
('54', '9', 'Jawa Barat', 'Kabupaten', 'Bekasi', 17837),
('55', '9', 'Jawa Barat', 'Kota', 'Bekasi', 17121),
('56', '2', 'Bangka Belitung', 'Kabupaten', 'Belitung', 33419),
('57', '2', 'Bangka Belitung', 'Kabupaten', 'Belitung Timur', 33519),
('58', '23', 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Belu', 85711),
('59', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Bener Meriah', 24581),
('6', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Singkil', 24785),
('60', '26', 'Riau', 'Kabupaten', 'Bengkalis', 28719),
('61', '12', 'Kalimantan Barat', 'Kabupaten', 'Bengkayang', 79213),
('62', '4', 'Bengkulu', 'Kota', 'Bengkulu', 38229),
('63', '4', 'Bengkulu', 'Kabupaten', 'Bengkulu Selatan', 38519),
('64', '4', 'Bengkulu', 'Kabupaten', 'Bengkulu Tengah', 38319),
('65', '4', 'Bengkulu', 'Kabupaten', 'Bengkulu Utara', 38619),
('66', '15', 'Kalimantan Timur', 'Kabupaten', 'Berau', 77311),
('67', '24', 'Papua', 'Kabupaten', 'Biak Numfor', 98119),
('68', '22', 'Nusa Tenggara Barat (NTB)', 'Kabupaten', 'Bima', 84171),
('69', '22', 'Nusa Tenggara Barat (NTB)', 'Kota', 'Bima', 84139),
('7', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Tamiang', 24476),
('70', '34', 'Sumatera Utara', 'Kota', 'Binjai', 20712),
('71', '17', 'Kepulauan Riau', 'Kabupaten', 'Bintan', 29135),
('72', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Bireuen', 24219),
('73', '31', 'Sulawesi Utara', 'Kota', 'Bitung', 95512),
('74', '11', 'Jawa Timur', 'Kabupaten', 'Blitar', 66171),
('75', '11', 'Jawa Timur', 'Kota', 'Blitar', 66124),
('76', '10', 'Jawa Tengah', 'Kabupaten', 'Blora', 58219),
('77', '7', 'Gorontalo', 'Kabupaten', 'Boalemo', 96319),
('78', '9', 'Jawa Barat', 'Kabupaten', 'Bogor', 16911),
('79', '9', 'Jawa Barat', 'Kota', 'Bogor', 16119),
('8', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Tengah', 24511),
('80', '11', 'Jawa Timur', 'Kabupaten', 'Bojonegoro', 62119),
('81', '31', 'Sulawesi Utara', 'Kabupaten', 'Bolaang Mongondow (Bolmong)', 95755),
('82', '31', 'Sulawesi Utara', 'Kabupaten', 'Bolaang Mongondow Selatan', 95774),
('83', '31', 'Sulawesi Utara', 'Kabupaten', 'Bolaang Mongondow Timur', 95783),
('84', '31', 'Sulawesi Utara', 'Kabupaten', 'Bolaang Mongondow Utara', 95765),
('85', '30', 'Sulawesi Tenggara', 'Kabupaten', 'Bombana', 93771),
('86', '11', 'Jawa Timur', 'Kabupaten', 'Bondowoso', 68219),
('87', '28', 'Sulawesi Selatan', 'Kabupaten', 'Bone', 92713),
('88', '7', 'Gorontalo', 'Kabupaten', 'Bone Bolango', 96511),
('89', '15', 'Kalimantan Timur', 'Kota', 'Bontang', 75313),
('9', '21', 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Tenggara', 24611),
('90', '24', 'Papua', 'Kabupaten', 'Boven Digoel', 99662),
('91', '10', 'Jawa Tengah', 'Kabupaten', 'Boyolali', 57312),
('92', '10', 'Jawa Tengah', 'Kabupaten', 'Brebes', 52212),
('93', '32', 'Sumatera Barat', 'Kota', 'Bukittinggi', 26115),
('94', '1', 'Bali', 'Kabupaten', 'Buleleng', 81111),
('95', '28', 'Sulawesi Selatan', 'Kabupaten', 'Bulukumba', 92511),
('96', '16', 'Kalimantan Utara', 'Kabupaten', 'Bulungan (Bulongan)', 77211),
('97', '8', 'Jambi', 'Kabupaten', 'Bungo', 37216),
('98', '29', 'Sulawesi Tengah', 'Kabupaten', 'Buol', 94564),
('99', '19', 'Maluku', 'Kabupaten', 'Buru', 97371);

-- --------------------------------------------------------

--
-- Table structure for table `configwebs`
--

CREATE TABLE `configwebs` (
  `idconfig` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `note` text DEFAULT NULL,
  `datecreated` datetime DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `support` varchar(100) DEFAULT NULL,
  `map` text DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `configwebs`
--

INSERT INTO `configwebs` (`idconfig`, `name`, `note`, `datecreated`, `type`, `address`, `phone`, `support`, `map`, `status`, `description`) VALUES
(1, 'https://www.tokopedia.com/byqyara', '', '2020-11-07 14:45:50', 11, '', '', '', '', NULL, ''),
(2, 'https://shopee.co.id/qyara.official', '', '2020-11-07 14:46:12', 12, '', '', '', '', NULL, ''),
(3, 'https://www.qyaraofficial.com', '', '2020-11-07 14:46:36', 13, '', '', '', '', NULL, ''),
(4, 'https://api.whatsapp.com/send?phone=6285876222797', '', '2020-11-07 14:46:55', 14, '', '', '', '', NULL, ''),
(5, 'https://www.instagram.com/qyara.official/', '', '2020-11-07 14:47:35', 15, '', '', '', '', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `couriers`
--

CREATE TABLE `couriers` (
  `idcourier` varchar(30) NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `note` varchar(1024) DEFAULT NULL,
  `img` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `couriers`
--

INSERT INTO `couriers` (`idcourier`, `name`, `status`, `note`, `img`) VALUES
('jne', 'JNE', 2, 'JNE (Jalur Nugraha Ekakurir)', NULL),
('pos', 'POS Indonesia', 2, 'PT POS Indonesia', NULL),
('tiki', 'TIKI', 2, 'PT Citra Van Titipan Kilat', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE `discounts` (
  `iddiscount` int(11) NOT NULL,
  `percent` int(11) NOT NULL,
  `startdate` datetime DEFAULT NULL,
  `enddate` datetime DEFAULT NULL,
  `idproduct` varchar(30) NOT NULL,
  `isperiod` tinyint(4) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `minqty` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `orderdetails`
--

CREATE TABLE `orderdetails` (
  `idorderdetail` varchar(30) NOT NULL,
  `idorder` varchar(30) NOT NULL,
  `idproduct` varchar(30) NOT NULL,
  `idvarian` varchar(30) NOT NULL,
  `namevarian` varchar(100) NOT NULL,
  `price` int(11) NOT NULL,
  `size` varchar(30) DEFAULT NULL,
  `weight` int(11) NOT NULL,
  `percent` int(11) DEFAULT NULL,
  `iddiscount` int(11) DEFAULT NULL,
  `qty` int(11) NOT NULL,
  `finalprice` int(11) NOT NULL,
  `finalweight` int(11) NOT NULL,
  `img` varchar(50) NOT NULL,
  `dateimg` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orderdetails`
--

INSERT INTO `orderdetails` (`idorderdetail`, `idorder`, `idproduct`, `idvarian`, `namevarian`, `price`, `size`, `weight`, `percent`, `iddiscount`, `qty`, `finalprice`, `finalweight`, `img`, `dateimg`) VALUES
('1612582991003', '1612582990977', '1608433232710', '1608934821550', 'ESSENTIAL STRUCTURED BLAZER - HIJAU, M', 200000, 'M', 100, NULL, NULL, 2, 400000, 200, '1609383915601', '2020-12-31 04:05:15'),
('1612582991020', '1612582990977', '1608960557791', '1608960629310', 'GAMIS UPYROCHA COAB - PINK, L', 200000, 'L', 200, NULL, NULL, 2, 400000, 400, 'img-1610243394138', '2021-01-10 02:49:54'),
('1612582991021', '1612582990977', '1608451869038', '1610242765654', 'ESSENTIAL BLAZER 8888 - HIJAU, L', 200000, 'L', 100, NULL, NULL, 1, 200000, 100, 'img-1610243144969', '2021-01-10 02:45:44'),
('1613782179384', '1613782179104', '1608451869038', '1610242765654', 'ESSENTIAL BLAZER 8888 - HIJAU, L', 200000, 'L', 100, NULL, NULL, 1, 200000, 100, 'img-1610243144969', '2021-01-10 02:45:44'),
('1613816528226', '1613816527706', '1609031073338', '1609041037104', 'Amani Dress Liliac - HIJAU, L', 200000, 'L', 300, NULL, NULL, 2, 400000, 600, 'img-1610243498213', '2021-01-10 02:51:38'),
('1613816528377', '1613816527706', '1608960557791', '1608960629310', 'GAMIS UPYROCHA COAB - PINK, L', 200000, 'L', 200, NULL, NULL, 1, 200000, 200, 'img-1610243394138', '2021-01-10 02:49:54'),
('1613892986605', '1613892986024', '1608451869038', '1610242765654', 'ESSENTIAL BLAZER 8888 - HIJAU, L', 200000, 'L', 100, NULL, NULL, 1, 200000, 100, 'img-1610243144969', '2021-01-10 02:45:44');

-- --------------------------------------------------------

--
-- Table structure for table `orderlog`
--

CREATE TABLE `orderlog` (
  `idlog` int(11) NOT NULL,
  `idorder` varchar(30) NOT NULL,
  `datelog` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `note` varchar(1080) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orderlog`
--

INSERT INTO `orderlog` (`idlog`, `idorder`, `datelog`, `status`, `note`) VALUES
(1, '1612582990977', '0000-00-00 00:00:00', 2, 'Menunggu verifikasi bukti bayar oleh penjual'),
(2, '1613782179104', '2021-02-20 01:49:39', 1, 'Menunggu konfirmasi pembayaran dari pembeli'),
(3, '1613782179104', '2021-02-20 02:07:58', 2, 'Menunggu verifikasi bukti bayar oleh penjual'),
(4, '1613816527706', '2021-02-20 11:22:08', 1, 'Menunggu konfirmasi pembayaran dari pembeli'),
(5, '1613782179104', '2021-02-21 01:03:14', 4, 'Pembelian sedang diproses atau disiapkan untuk dikirim'),
(6, '1612582990977', '2021-02-21 01:18:43', 1, 'Menunggu konfirmasi pembayaran dari pembeli'),
(7, '1613816527706', '2021-02-21 01:20:31', 2, 'Menunggu verifikasi bukti bayar oleh penjual'),
(11, '1613782179104', '2021-02-21 04:28:30', 5, 'Nomor resi sudah release dan pesanan sedang dalam proses pengiriman ke alamat pembeli'),
(12, '1613816527706', '2021-02-21 04:32:52', 4, 'Pembelian sedang diproses atau disiapkan untuk dikirim'),
(13, '1613816527706', '2021-02-21 04:49:29', 5, 'Nomor resi sudah release dan pesanan sedang dalam proses pengiriman ke alamat pembeli'),
(14, '1612582990977', '2021-02-21 05:16:38', 2, 'Menunggu verifikasi bukti bayar oleh penjual'),
(15, '1612582990977', '2021-02-21 05:17:00', 4, 'Pembelian sedang diproses atau disiapkan untuk dikirim'),
(16, '1612582990977', '2021-02-21 05:17:45', 5, 'Nomor resi sudah release dan pesanan sedang dalam proses pengiriman ke alamat pembeli'),
(17, '1613816527706', '2021-02-21 06:32:40', 6, 'Pesanan telah diterima pembeli, transaksi selesai'),
(18, '1613782179104', '2021-02-21 07:13:35', 6, 'Pesanan telah diterima pembeli, transaksi selesai'),
(19, '1613892986024', '2021-02-21 08:36:26', 1, 'Menunggu konfirmasi pembayaran dari pembeli'),
(20, '1613892986024', '2021-02-21 08:37:17', 2, 'Menunggu verifikasi bukti bayar oleh penjual'),
(21, '1613892986024', '2021-02-21 08:40:11', 4, 'Pembelian sedang diproses atau disiapkan untuk dikirim');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `idorder` varchar(30) NOT NULL,
  `orderdate` datetime NOT NULL,
  `status` int(10) NOT NULL,
  `idcustomer` int(11) NOT NULL,
  `invoicekey` varchar(50) NOT NULL,
  `totalweight` int(11) DEFAULT NULL,
  `totalprice` int(11) DEFAULT NULL,
  `typepayment` int(11) DEFAULT NULL,
  `idpayment` varchar(30) DEFAULT NULL,
  `paymentname` varchar(30) DEFAULT NULL,
  `note` varchar(1024) DEFAULT NULL,
  `orderprice` int(11) DEFAULT NULL,
  `typepaymentname` varchar(50) DEFAULT NULL,
  `receiptnumber` varchar(50) DEFAULT NULL,
  `shipdate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`idorder`, `orderdate`, `status`, `idcustomer`, `invoicekey`, `totalweight`, `totalprice`, `typepayment`, `idpayment`, `paymentname`, `note`, `orderprice`, `typepaymentname`, `receiptnumber`, `shipdate`) VALUES
('1612582990977', '2021-02-06 04:43:10', 5, 1, 'INV1612582990977', 700, 1000000, 123124356, '128632567', 'Bank BNI', '', 1018000, 'Transfer Bank', 'DE542356367', '2021-02-21 05:17:45'),
('1613782179104', '2021-02-20 01:49:39', 6, 1, 'INV1613782179104', 100, 200000, 123124356, '128632567', 'Bank BNI', '', 218000, 'Transfer Bank', 'JN647887548798732', NULL),
('1613816527706', '2021-02-20 11:22:07', 6, 1, 'INV1613816527706', 800, 600000, 123124356, '646246725', 'Bank BCA', '', 617000, 'Transfer Bank', 'TK847875789698', NULL),
('1613892986024', '2021-02-21 08:36:26', 4, 1, 'INV1613892986024', 100, 200000, 123124356, '128632567', 'Bank BNI', '', 217000, 'Transfer Bank', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `idpayment` varchar(30) NOT NULL,
  `idlevel` tinyint(4) NOT NULL,
  `name` varchar(50) NOT NULL,
  `accountnumber` varchar(30) DEFAULT NULL,
  `accountname` varchar(30) DEFAULT NULL,
  `img` varchar(50) DEFAULT NULL,
  `parent` varchar(30) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 2
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`idpayment`, `idlevel`, `name`, `accountnumber`, `accountname`, `img`, `parent`, `status`) VALUES
('123124356', 1, 'Transfer Bank', NULL, NULL, NULL, NULL, 2),
('128632567', 2, 'Bank BNI', '1234 7346 43', 'a/n Josi Fara', 'bni.png', '123124356', 2),
('646246725', 2, 'Bank BCA', '1327 6456 667', 'a/n Josi Fara', 'bca.png', '123124356', 2);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `idproduct` varchar(30) NOT NULL,
  `productname` varchar(225) NOT NULL,
  `price` varchar(30) DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `idcategory` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `datecreated` datetime NOT NULL,
  `size` varchar(5) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `note` varchar(225) DEFAULT NULL,
  `img` varchar(225) DEFAULT NULL,
  `link1` text DEFAULT NULL,
  `link2` text DEFAULT NULL,
  `link3` text DEFAULT NULL,
  `sku` varchar(50) DEFAULT NULL,
  `isfeatured` tinyint(4) DEFAULT NULL,
  `isvarian` tinyint(4) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `createby` int(11) DEFAULT NULL,
  `hits` int(30) DEFAULT NULL,
  `imgcreated` datetime DEFAULT NULL,
  `isarchived` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`idproduct`, `productname`, `price`, `discount`, `idcategory`, `amount`, `description`, `datecreated`, `size`, `status`, `note`, `img`, `link1`, `link2`, `link3`, `sku`, `isfeatured`, `isvarian`, `width`, `height`, `weight`, `createby`, `hits`, `imgcreated`, `isarchived`) VALUES
('1608433232710', 'ESSENTIAL STRUCTURED BLAZER', 'IDR 100.000 - IDR 500.000', NULL, NULL, NULL, '', '2020-12-20 04:00:32', NULL, 2, NULL, 'img-1609382689019', NULL, NULL, NULL, '123', 2, 2, NULL, NULL, 100, NULL, NULL, '2020-12-31 03:44:49', NULL),
('1608451869038', 'ESSENTIAL BLAZER 8888', 'IDR 200.000', NULL, 1, NULL, '', '2020-12-20 09:11:09', NULL, 2, NULL, 'img-1610243144877', NULL, NULL, NULL, '2314', 2, 2, NULL, NULL, 100, NULL, NULL, '2021-01-10 02:45:44', NULL),
('1608960557791', 'GAMIS UPYROCHA COAB', 'IDR 200.000 - IDR 500.000', NULL, 1, NULL, '<p>AMANI DRESS&nbsp;</p>\r\n\r\n<p>SPESIFIKASI :&nbsp;&nbsp;<br />\r\nSIZE M<br />\r\nWarna Liliac<br />\r\nLebar Dada 100 cm<br />\r\nPanjang Tangan 57 cm<br />\r\nPanjang Baju 135 cm<br />\r\nTerdapat saku di kanan<br />\r\nBusui friendly dengan restleting di dada 25 cm<br />\r\nWudhu friendly dengan kancing benik<br />\r\nTali lepas yang membuat kesan ramping</p>\r\n\r\n<p>MATERIAL: Amunzen Wollycrepe dipadukan dengan Premium Lace</p>\r\n\r\n<p>Amani dress dapat kamu pakai untuk daily outfit maupun acara formal kamu. dengan material Amunzen Wollycrepe yang merupakan material crepe paling premium, bahannya jatuh, lembut dan tidak membuat gerah. Paduan lace premiumnya juga membuat kesan unik dan cantik, lace premium kami tidak membuat gatal dan nyaman digunakan. Berbagai fitur lain seperti busui friendly, wudhu friendly dan tali lepas membuat aktivitas kamu semakin nyaman menggunakan Amani Dress.&nbsp;</p>\r\n\r\n<p>Terdapat perbedaan warna kurang lebih 10% dari foto. Produk biasanya lebih gelap. Foto menggunakan kamera profesional, sehingga cahaya di foto akan lebih terang dibanding produk asli.</p>\r\n\r\n<p><br />\r\nTerimakasih atas kepercayaan kamu pada kami! ^^</p>\r\n', '2020-12-26 06:29:17', NULL, 2, NULL, 'img-1613481106076', NULL, NULL, NULL, '1234K4344LK', 1, 2, 44, NULL, 200, NULL, NULL, '2021-02-16 14:11:46', NULL),
('1609031073338', 'Amani Dress Liliac', 'IDR 200.000', NULL, 2, NULL, '<p>rtewr eg&nbsp;</p>\r\n', '2020-12-27 02:04:33', NULL, 2, NULL, 'img-1610243498256', NULL, NULL, NULL, '1234K4344LKwg', 2, 2, 44, 2, 300, NULL, NULL, '2021-01-10 02:51:38', NULL),
('1609066573725', 'Amani Dress Light Brown', NULL, NULL, 1, NULL, '<p>casca afds</p>\r\n', '2020-12-27 11:56:13', NULL, 1, NULL, NULL, NULL, NULL, NULL, 'SKU1609066573725', NULL, NULL, NULL, NULL, 100, NULL, NULL, NULL, 2),
('1613889346383', 'GAMIS RASSHYANH', 'IDR 150.000 - IDR 200.000', NULL, 2, NULL, '<h6><span style=\"font-size:14px\">AMANI DRESS&nbsp;</span></h6>\r\n\r\n<p>SPESIFIKASI :&nbsp;&nbsp;<br />\r\nSIZE M<br />\r\nWarna Liliac<br />\r\nLebar Dada 100 cm<br />\r\nPanjang Tangan 57 cm<br />\r\nPanjang Baju 135 cm<br />\r\nTerdapat saku di kanan<br />\r\nBusui friendly dengan restleting di dada 25 cm<br />\r\nWudhu friendly dengan kancing benik<br />\r\nTali lepas yang membuat kesan ramping</p>\r\n\r\n<p>MATERIAL: Amunzen Wollycrepe dipadukan dengan Premium Lace</p>\r\n\r\n<p>Amani dress dapat kamu pakai untuk daily outfit maupun acara formal kamu. dengan material Amunzen Wollycrepe yang merupakan material crepe paling premium, bahannya jatuh, lembut dan tidak membuat gerah. Paduan lace premiumnya juga membuat kesan unik dan cantik, lace premium kami tidak membuat gatal dan nyaman digunakan. Berbagai fitur lain seperti busui friendly, wudhu friendly dan tali lepas membuat aktivitas kamu semakin nyaman menggunakan Amani Dress.&nbsp;</p>\r\n\r\n<p>Terdapat perbedaan warna kurang lebih 10% dari foto. Produk biasanya lebih gelap. Foto menggunakan kamera profesional, sehingga cahaya di foto akan lebih terang dibanding produk asli.</p>\r\n\r\n<p><br />\r\nTerimakasih atas kepercayaan kamu pada kami! ^^</p>\r\n', '2021-02-21 07:35:46', NULL, 2, NULL, 'img-1613889597739', NULL, NULL, NULL, 'SKU1613889346383', 2, 2, 44, 10, 300, NULL, NULL, '2021-02-21 07:39:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `provinces`
--

CREATE TABLE `provinces` (
  `province_id` varchar(10) NOT NULL,
  `province` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `provinces`
--

INSERT INTO `provinces` (`province_id`, `province`) VALUES
('1', 'Bali'),
('10', 'Jawa Tengah'),
('11', 'Jawa Timur'),
('12', 'Kalimantan Barat'),
('13', 'Kalimantan Selatan'),
('14', 'Kalimantan Tengah'),
('15', 'Kalimantan Timur'),
('16', 'Kalimantan Utara'),
('17', 'Kepulauan Riau'),
('18', 'Lampung'),
('19', 'Maluku'),
('2', 'Bangka Belitung'),
('20', 'Maluku Utara'),
('21', 'Nanggroe Aceh Darussalam (NAD)'),
('22', 'Nusa Tenggara Barat (NTB)'),
('23', 'Nusa Tenggara Timur (NTT)'),
('24', 'Papua'),
('25', 'Papua Barat'),
('26', 'Riau'),
('27', 'Sulawesi Barat'),
('28', 'Sulawesi Selatan'),
('29', 'Sulawesi Tengah'),
('3', 'Banten'),
('30', 'Sulawesi Tenggara'),
('31', 'Sulawesi Utara'),
('32', 'Sumatera Barat'),
('33', 'Sumatera Selatan'),
('34', 'Sumatera Utara'),
('4', 'Bengkulu'),
('5', 'DI Yogyakarta'),
('6', 'DKI Jakarta'),
('7', 'Gorontalo'),
('8', 'Jambi'),
('9', 'Jawa Barat');

-- --------------------------------------------------------

--
-- Table structure for table `shippers`
--

CREATE TABLE `shippers` (
  `idshipping` varchar(30) NOT NULL,
  `idorder` varchar(30) NOT NULL,
  `idprovorigin` varchar(10) DEFAULT NULL,
  `nameprovorigin` varchar(50) DEFAULT NULL,
  `idcityorigin` varchar(10) DEFAULT NULL,
  `namecityorigin` varchar(50) DEFAULT NULL,
  `idprovdestination` varchar(10) DEFAULT NULL,
  `nameprovdestination` varchar(50) DEFAULT NULL,
  `idcitydestination` varchar(10) DEFAULT NULL,
  `namecitydestination` varchar(50) DEFAULT NULL,
  `idcourier` varchar(10) DEFAULT NULL,
  `namecourier` varchar(50) NOT NULL,
  `nameservice` varchar(50) DEFAULT NULL,
  `cost` int(11) NOT NULL,
  `weight` int(11) DEFAULT NULL,
  `receiptnumber` varchar(50) DEFAULT NULL,
  `address` varchar(1024) DEFAULT NULL,
  `postal_code` int(30) DEFAULT NULL,
  `address_phone` varchar(30) DEFAULT NULL,
  `shipdate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `shippers`
--

INSERT INTO `shippers` (`idshipping`, `idorder`, `idprovorigin`, `nameprovorigin`, `idcityorigin`, `namecityorigin`, `idprovdestination`, `nameprovdestination`, `idcitydestination`, `namecitydestination`, `idcourier`, `namecourier`, `nameservice`, `cost`, `weight`, `receiptnumber`, `address`, `postal_code`, `address_phone`, `shipdate`) VALUES
('1612582990991', '1612582990977', '10', 'D I Yogyakarta', '501', 'Kota Yogyakarta', '10', 'Jawa Tengah', 'Wonogiri', 'Wonogiri', 'jne', 'jne', 'YES', 18000, 700, 'DE542356367', 'Karangjati, RT 5, Jetis, Tamantirto, Kasihan, Bantul 55183 (samping gedung MTA Cabang Kasihan)', 58192, '085666777444', '2021-02-21 05:17:45'),
('1613782179283', '1613782179104', '10', 'DI Yogyakarta', '501', 'Yogyakarta', '10', 'Jawa Tengah', '497', 'Wonogiri', 'jne', 'JNE', 'YES', 18000, 100, NULL, 'Janggan RT 02/01 Jatiroto Wonogiri (selatan gedung MTA cab. jatiroto) ', 57692, '085338325183', NULL),
('1613816528059', '1613816527706', '10', 'DI Yogyakarta', '501', 'Yogyakarta', '10', 'Jawa Tengah', '497', 'Wonogiri', 'tiki', 'TIKI', 'ONS', 17000, 800, 'TK847875789698', 'Janggan RT 02/01 Jatiroto Wonogiri (selatan gedung MTA cab. jatiroto) ', 57692, '085338325183', NULL),
('1613892986091', '1613892986024', '9', 'DI Yogyakarta', '501', 'Yogyakarta', '9', 'Jawa Barat', '171', 'Karawang', 'jne', 'JNE', 'REG', 17000, 100, NULL, 'Janggan RT 02/01 Jatiroto Wonogiri (selatan gedung MTA cab. jatiroto) ', 57692, '085338325183', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `store`
--

CREATE TABLE `store` (
  `idstore` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `owner` varchar(30) DEFAULT NULL,
  `province_id` varchar(10) NOT NULL,
  `province_name` varchar(50) NOT NULL,
  `city_id` varchar(20) NOT NULL,
  `city_name` varchar(50) NOT NULL,
  `postal_code` varchar(10) NOT NULL,
  `address` varchar(225) DEFAULT NULL,
  `phone` varchar(20) NOT NULL,
  `phone_other` varchar(10) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `district` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `store`
--

INSERT INTO `store` (`idstore`, `name`, `description`, `owner`, `province_id`, `province_name`, `city_id`, `city_name`, `postal_code`, `address`, `phone`, `phone_other`, `email`, `district`, `status`) VALUES
(2, 'Qyara Official', '', 'Mega', '5', 'DI Yogyakarta', '501', 'Yogyakarta', '53183', '', '085338325183', '', 'i@g.com', 'Kasihan', 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `iduser` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `role` int(11) NOT NULL,
  `lastlogin` datetime DEFAULT NULL,
  `userkey` varchar(30) NOT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `phone` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`iduser`, `username`, `password`, `name`, `role`, `lastlogin`, `userkey`, `lastname`, `email`, `status`, `phone`) VALUES
(1, 'customer', '91ec1f9324753048c0096d036a694f86', 'Rasma Ar', 1, NULL, '12321241', 'Runda Jafar', 'customer@g.com', 2, '082656111222'),
(2, 'super', 'ecb1d4ce2c43c60fbdf74a70648cf020', 'Owner', 5, NULL, '32131238', NULL, '', 2, ''),
(4, 'joni3540', '24b90bc48a67ac676228385a7c71a119', 'joni', 1, NULL, '1610281168825', 'jon jin', 'joni3@g.com', 2, '085338325183');

-- --------------------------------------------------------

--
-- Table structure for table `varianimg`
--

CREATE TABLE `varianimg` (
  `idvarian` varchar(30) NOT NULL,
  `idproduct` varchar(30) NOT NULL,
  `namevarian` varchar(100) NOT NULL,
  `qty` int(11) NOT NULL,
  `minqty` int(11) DEFAULT NULL,
  `price` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `img` varchar(50) DEFAULT NULL,
  `size` varchar(30) DEFAULT NULL,
  `uniquekey` varchar(100) DEFAULT NULL,
  `parent` varchar(30) DEFAULT NULL,
  `datecreated` datetime DEFAULT NULL,
  `ordering` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `varianimg`
--

INSERT INTO `varianimg` (`idvarian`, `idproduct`, `namevarian`, `qty`, `minqty`, `price`, `type`, `img`, `size`, `uniquekey`, `parent`, `datecreated`, `ordering`) VALUES
('1608934821433', '1608433232710', 'Hijau', 20, NULL, 300000, 1, NULL, 'L', '1608433232710:HIJAU:L', NULL, NULL, NULL),
('1608934821550', '1608433232710', 'Hijau', 15, NULL, 200000, 1, NULL, 'M', '1608433232710:HIJAU:M', NULL, NULL, NULL),
('1608934821553', '1608433232710', 'Biru', 10, NULL, 200000, 1, NULL, 'L', '1608433232710:BIRU:L', NULL, NULL, NULL),
('1608956137637', '1608433232710', 'img', 1, NULL, 1, 2, '1609383915601', NULL, NULL, '1609499110921', '2020-12-31 04:05:15', 6),
('1608956137656', '1608433232710', 'img', 1, NULL, 1, 2, 'img-1608956137656UP', NULL, NULL, '1609495549293', '2020-12-31 03:54:18', 5),
('1608960629293', '1608960557791', 'HIJAU', 5, NULL, 200000, 1, NULL, 'L', '1608960557791:HIJAU:L', NULL, NULL, NULL),
('1608960629310', '1608960557791', 'PINK', 5, NULL, 200000, 1, NULL, 'L', '1608960557791:PINK:L', NULL, NULL, NULL),
('1608960629416', '1608960557791', 'HIJAU', 7, NULL, 500000, 1, NULL, 'M', '1608960557791:HIJAU:M', NULL, NULL, NULL),
('1608960746769', '1608960557791', 'img', 1, NULL, 1, 2, 'img-1610243394079', NULL, NULL, '1608960629293', '2021-01-10 02:49:54', 1),
('1608960746811', '1608960557791', 'img', 1, NULL, 1, 2, 'img-1610243394121', NULL, NULL, '1608960629416', '2021-01-10 02:49:54', 2),
('1608960746813', '1608960557791', 'img', 1, NULL, 1, 2, 'img-1610243394138', NULL, NULL, '1608960629310', '2021-01-10 02:49:54', 3),
('1609017306016', '1608433232710', 'img', 1, NULL, 1, 2, 'img-1609017306016UP', NULL, NULL, '1609497990388', '2020-12-31 03:54:18', 3),
('1609017306038', '1608433232710', 'img', 1, NULL, 1, 2, 'img-1609017306038UP', NULL, NULL, '1608934821553', '2020-12-31 03:54:18', 4),
('1609030023584', '1608960557791', 'img', 1, NULL, 1, 2, 'img-1610243394141', NULL, NULL, '1608960629310', '2021-01-10 02:49:54', 4),
('1609041037104', '1609031073338', 'HIJAU', 3, NULL, 200000, 1, NULL, 'L', '1609031073338:HIJAU:L', NULL, NULL, NULL),
('1609041070460', '1609031073338', 'BIRU', 3, NULL, 200000, 1, NULL, 'L', '1609031073338:BIRU:L', NULL, NULL, NULL),
('1609041070462', '1609031073338', 'HIJAU', 3, NULL, 200000, 1, NULL, 'M', '1609031073338:HIJAU:M', NULL, NULL, NULL),
('1609041096772', '1609031073338', 'img', 1, NULL, 1, 2, 'img-1610243498213', NULL, NULL, '1609041037104', '2021-01-10 02:51:38', 2),
('1609041096801', '1609031073338', 'img', 1, NULL, 1, 2, 'img-1610243498256', NULL, NULL, '1609041070460', '2021-01-10 02:51:38', 1),
('1609041096803', '1609031073338', 'img', 1, NULL, 1, 2, 'img-1609041096803', NULL, NULL, '1609041070462', '2020-12-27 04:51:36', 4),
('1609041207848', '1609031073338', 'img', 1, NULL, 1, 2, 'img-1610243498260', NULL, NULL, '1609041070460', '2021-01-10 02:51:38', 3),
('1609052932323', '1609031073338', 'BIRU', 3, NULL, 200000, 1, NULL, 'M', '1609031073338:BIRU:M', NULL, NULL, NULL),
('1609052950955', '1609031073338', 'img', 1, NULL, 1, 2, 'img-1610243498263', NULL, NULL, '1609041037104', '2021-01-10 02:51:38', 10),
('1609382518312', '1608433232710', 'img', 1, NULL, 1, 2, '1609417855150', NULL, NULL, '1609498089025', '2020-12-31 13:30:55', 2),
('1609382689019', '1608433232710', 'img', 1, NULL, 1, 2, 'img-1609382689019', NULL, NULL, '1608934821433', '2020-12-31 03:44:49', 1),
('1609495549293', '1608433232710', 'biru', 0, NULL, 100000, 1, NULL, 'M', '1608433232710:BIRU:M', NULL, NULL, NULL),
('1609497990388', '1608433232710', 'ungu', 2, NULL, 500000, 1, NULL, 'L', '1608433232710:UNGU:L', NULL, NULL, NULL),
('1609498089025', '1608433232710', 'ungu', 33, NULL, 200000, 1, NULL, 'XL', '1608433232710:UNGU:XL', NULL, NULL, NULL),
('1609499110921', '1608433232710', 'armi satu', 1, NULL, 300000, 1, NULL, NULL, '1608433232710:ARMISATU', NULL, NULL, NULL),
('1610242765654', '1608451869038', 'HIJAU', 3, NULL, 200000, 1, NULL, 'L', '1608451869038:HIJAU:L', NULL, NULL, NULL),
('1610242765697', '1608451869038', 'PINK', 3, NULL, 200000, 1, NULL, 'M', '1608451869038:PINK:M', NULL, NULL, NULL),
('1610243144877', '1608451869038', 'img', 1, NULL, 1, 2, 'img-1610243144877', NULL, NULL, '1610242765697', '2021-01-10 02:45:44', NULL),
('1610243144969', '1608451869038', 'img', 1, NULL, 1, 2, 'img-1610243144969', NULL, NULL, '1610242765654', '2021-01-10 02:45:44', NULL),
('1613481106076', '1608960557791', 'img', 1, NULL, 1, 2, 'img-1613481106076', NULL, NULL, '', '2021-02-16 14:11:46', NULL),
('1613889477175', '1613889346383', 'HIJAU', 3, NULL, 200000, 1, NULL, 'L', '1613889346383:HIJAU:L', NULL, NULL, NULL),
('1613889477193', '1613889346383', 'BIRU', 5, NULL, 200000, 1, NULL, 'M', '1613889346383:BIRU:M', NULL, NULL, NULL),
('1613889477194', '1613889346383', 'BIRU', 3, NULL, 150000, 1, NULL, 'S', '1613889346383:BIRU:S', NULL, NULL, NULL),
('1613889477195', '1613889346383', 'HIJAU', 5, NULL, 200000, 1, NULL, 'S', '1613889346383:HIJAU:S', NULL, NULL, NULL),
('1613889597739', '1613889346383', 'img', 1, NULL, 1, 2, 'img-1613889597739', NULL, NULL, '1613889477193', '2021-02-21 07:39:57', 1),
('1613889597758', '1613889346383', 'img', 1, NULL, 1, 2, 'img-1613889597758', NULL, NULL, '1613889477194', '2021-02-21 07:39:57', 2),
('1613889597904', '1613889346383', 'img', 1, NULL, 1, 2, 'img-1613889597904', NULL, NULL, '1613889477175', '2021-02-21 07:39:57', 3),
('1613889597907', '1613889346383', 'img', 1, NULL, 1, 2, 'img-1613889597907', NULL, NULL, '1613889477195', '2021-02-21 07:39:57', 4);

-- --------------------------------------------------------

--
-- Table structure for table `verifypayment`
--

CREATE TABLE `verifypayment` (
  `idverify` int(11) NOT NULL,
  `idorder` varchar(30) NOT NULL,
  `date` datetime NOT NULL,
  `note` varchar(1080) DEFAULT NULL,
  `img` varchar(100) NOT NULL,
  `isarchived` tinyint(4) DEFAULT NULL,
  `accountname` varchar(50) NOT NULL,
  `nominal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `verifypayment`
--

INSERT INTO `verifypayment` (`idverify`, `idorder`, `date`, `note`, `img`, `isarchived`, `accountname`, `nominal`) VALUES
(8, '1612582990977', '2021-02-19 22:53:15', 'BAYARR', 'VP1613771595660.png', 2, 'tika', 200000),
(9, '1613782179104', '2021-02-20 02:07:58', 'catat', 'VP1613783278525.png', NULL, 'vorid', 200000),
(10, '1613816527706', '2021-02-21 01:20:31', 'kirim hari ini ya', 'VP1613866831757.jpg', NULL, 'ahmad', 200000),
(12, '1612582990977', '2021-02-21 05:16:38', 'hhhhhhhh', 'VP1613880998800.png', NULL, 'babab', 200000),
(13, '1613892986024', '2021-02-21 08:37:17', '', 'VP1613893037254.jpg', NULL, 'Irfan Nur', 500000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`idaddress`),
  ADD KEY `fk_iduser_users` (`iduser`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`idcategory`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `configwebs`
--
ALTER TABLE `configwebs`
  ADD PRIMARY KEY (`idconfig`);

--
-- Indexes for table `couriers`
--
ALTER TABLE `couriers`
  ADD PRIMARY KEY (`idcourier`);

--
-- Indexes for table `discounts`
--
ALTER TABLE `discounts`
  ADD PRIMARY KEY (`iddiscount`),
  ADD KEY `fk_product_discount` (`idproduct`);

--
-- Indexes for table `orderdetails`
--
ALTER TABLE `orderdetails`
  ADD PRIMARY KEY (`idorderdetail`),
  ADD KEY `fk_orders_idorder` (`idorder`);

--
-- Indexes for table `orderlog`
--
ALTER TABLE `orderlog`
  ADD PRIMARY KEY (`idlog`),
  ADD KEY `fk_orderlog_idorder` (`idorder`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`idorder`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`idpayment`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`idproduct`),
  ADD KEY `fk_category_product` (`idcategory`);

--
-- Indexes for table `provinces`
--
ALTER TABLE `provinces`
  ADD PRIMARY KEY (`province_id`);

--
-- Indexes for table `shippers`
--
ALTER TABLE `shippers`
  ADD PRIMARY KEY (`idshipping`),
  ADD KEY `fk_ship_orders_idorder` (`idorder`);

--
-- Indexes for table `store`
--
ALTER TABLE `store`
  ADD PRIMARY KEY (`idstore`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`iduser`),
  ADD UNIQUE KEY `userkey` (`userkey`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `varianimg`
--
ALTER TABLE `varianimg`
  ADD PRIMARY KEY (`idvarian`),
  ADD KEY `fk_product_varianimg` (`idproduct`);

--
-- Indexes for table `verifypayment`
--
ALTER TABLE `verifypayment`
  ADD PRIMARY KEY (`idverify`),
  ADD KEY `fk_verpay_idorder` (`idorder`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addresses`
--
ALTER TABLE `addresses`
  MODIFY `idaddress` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `idcategory` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `configwebs`
--
ALTER TABLE `configwebs`
  MODIFY `idconfig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `discounts`
--
ALTER TABLE `discounts`
  MODIFY `iddiscount` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2147483648;

--
-- AUTO_INCREMENT for table `orderlog`
--
ALTER TABLE `orderlog`
  MODIFY `idlog` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `store`
--
ALTER TABLE `store`
  MODIFY `idstore` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `verifypayment`
--
ALTER TABLE `verifypayment`
  MODIFY `idverify` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `addresses`
--
ALTER TABLE `addresses`
  ADD CONSTRAINT `fk_iduser_users` FOREIGN KEY (`iduser`) REFERENCES `users` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `discounts`
--
ALTER TABLE `discounts`
  ADD CONSTRAINT `fk_product_discount` FOREIGN KEY (`idproduct`) REFERENCES `products` (`idproduct`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orderdetails`
--
ALTER TABLE `orderdetails`
  ADD CONSTRAINT `fk_orders_idorder` FOREIGN KEY (`idorder`) REFERENCES `orders` (`idorder`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orderlog`
--
ALTER TABLE `orderlog`
  ADD CONSTRAINT `fk_orderlog_idorder` FOREIGN KEY (`idorder`) REFERENCES `orders` (`idorder`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `fk_category_product` FOREIGN KEY (`idcategory`) REFERENCES `categories` (`idcategory`) ON UPDATE CASCADE;

--
-- Constraints for table `shippers`
--
ALTER TABLE `shippers`
  ADD CONSTRAINT `fk_ship_orders_idorder` FOREIGN KEY (`idorder`) REFERENCES `orders` (`idorder`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `varianimg`
--
ALTER TABLE `varianimg`
  ADD CONSTRAINT `fk_product_varianimg` FOREIGN KEY (`idproduct`) REFERENCES `products` (`idproduct`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `verifypayment`
--
ALTER TABLE `verifypayment`
  ADD CONSTRAINT `fk_verpay_idorder` FOREIGN KEY (`idorder`) REFERENCES `orders` (`idorder`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
