<?php

use app\components\Sendmail;
use app\helpers\OrderUtils;
use app\helpers\ProductHelper;
use app\models\Orders;
use app\models\Payments;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<html>
    <head>
        <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700" rel="stylesheet">

        <!-- CSS Reset : BEGIN -->
        <style>

            /* What it does: Remove spaces around the email design added by some email clients. */
            /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
            html,
            body {
                margin: 0 auto !important;
                padding: 0 !important;
                height: 100% !important;
                width: 100% !important;
                background: #f1f1f1;
            }

            /* What it does: Stops email clients resizing small text. */
            * {
                -ms-text-size-adjust: 100%;
                -webkit-text-size-adjust: 100%;
            }

            /* What it does: Centers email on Android 4.4 */
            div[style*="margin: 16px 0"] {
                margin: 0 !important;
            }

            /* What it does: Stops Outlook from adding extra spacing to tables. */
            table,
            td {
                mso-table-lspace: 0pt !important;
                mso-table-rspace: 0pt !important;
            }

            /* What it does: Fixes webkit padding issue. */
            table {
                border-spacing: 0 !important;
                border-collapse: collapse !important;
                table-layout: fixed !important;
                margin: 0 auto !important;
            }

            /* What it does: Uses a better rendering method when resizing images in IE. */
            img {
                -ms-interpolation-mode:bicubic;
            }

            /* What it does: Prevents Windows 10 Mail from underlining links despite inline CSS. Styles for underlined links should be inline. */
            a {
                text-decoration: none;
            }

            /* What it does: A work-around for email clients meddling in triggered links. */
            *[x-apple-data-detectors],  /* iOS */
            .unstyle-auto-detected-links *,
            .aBn {
                border-bottom: 0 !important;
                cursor: default !important;
                color: inherit !important;
                text-decoration: none !important;
                font-size: inherit !important;
                font-family: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
            }

            /* What it does: Prevents Gmail from displaying a download button on large, non-linked images. */
            .a6S {
                display: none !important;
                opacity: 0.01 !important;
            }

            /* What it does: Prevents Gmail from changing the text color in conversation threads. */
            .im {
                color: inherit !important;
            }

            /* If the above doesn't work, add a .g-img class to any image in question. */
            img.g-img + div {
                display: none !important;
            }

            /* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */
            /* Create one of these media queries for each additional viewport size you'd like to fix */

            /* iPhone 4, 4S, 5, 5S, 5C, and 5SE */
            @media only screen and (min-device-width: 320px) and (max-device-width: 374px) {
                u ~ div .email-container {
                    min-width: 320px !important;
                }
            }
            /* iPhone 6, 6S, 7, 8, and X */
            @media only screen and (min-device-width: 375px) and (max-device-width: 413px) {
                u ~ div .email-container {
                    min-width: 375px !important;
                }
            }
            /* iPhone 6+, 7+, and 8+ */
            @media only screen and (min-device-width: 414px) {
                u ~ div .email-container {
                    min-width: 414px !important;
                }
            }


        </style>

        <!-- CSS Reset : END -->

        <!-- Progressive Enhancements : BEGIN -->
        <style>

            .primary{
                background: #f3acac;
            }
            .bg_white{
                background: #ffffff;
            }
            .bg_light{
                background: #f7fafa;
            }
            .bg_black{
                background: #000000;
            }
            .bg_dark{
                background: rgba(0,0,0,.8);
            }
            .email-section{
                padding:2.5em;
            }

            /*BUTTON*/
            .btn{
                padding: 10px 15px;
                display: inline-block;
            }
            .btn.btn-primary{
                border-radius: 5px;
                background: #f3acac;
                color: #ffffff;
            }
            .btn.btn-white{
                border-radius: 5px;
                background: #ffffff;
                color: #000000;
            }
            .btn.btn-white-outline{
                border-radius: 5px;
                background: transparent;
                border: 1px solid #fff;
                color: #fff;
            }
            .btn.btn-black-outline{
                border-radius: 0px;
                background: transparent;
                border: 2px solid #000;
                color: #000;
                font-weight: 700;
            }
            .btn-custom{
                color: rgba(0,0,0,.3);
                text-decoration: underline;
            }

            h1,h2,h3,h4,h5,h6{
                font-family: 'Poppins', sans-serif;
                color: #000000;
                margin-top: 0;
                font-weight: 400;
            }

            body{
                font-family: 'Poppins', sans-serif;
                font-weight: 400;
                font-size: 15px;
                line-height: 1.8;
                color: rgba(0,0,0,.4);
            }

            a{
                color: #f3acac;
            }

            table{
            }
            /*LOGO*/

            .logo h1{
                margin: 0;
            }
            .logo h1 a{
                color: #f3acac;
                font-size: 24px;
                font-weight: 700;
                font-family: 'Poppins', sans-serif;
            }

            /*HERO*/
            .hero{
                position: relative;
                z-index: 0;
            }

            .hero .text{
                color: rgba(0,0,0,.3);
            }
            .hero .text h2{
                color: #000;
                font-size: 34px;
                margin-bottom: 0;
                font-weight: 200;
                line-height: 1.4;
            }
            .hero .text h3{
                font-size: 24px;
                font-weight: 300;
            }
            .hero .text h2 span{
                font-weight: 600;
                color: #000;
            }

            .text-author{
                border: 1px solid #c1c0c0;
                max-width: 50%;
                margin: 0 auto;
                padding: 2em;
            }

            .text-author h3{
                margin-bottom: 0;
            }
            ul.social{
                padding: 0;
            }
            ul.social li{
                display: inline-block;
                margin-right: 10px;
            }

            /*FOOTER*/

            .footer{
                border-top: 1px solid rgba(0,0,0,.05);
                color: rgba(0,0,0,.5);
            }
            .footer .heading{
                color: #000;
                font-size: 20px;
            }
            .footer ul{
                margin: 0;
                padding: 0;
            }
            .footer ul li{
                list-style: none;
                margin-bottom: 10px;
            }
            .footer ul li a{
                color: rgba(0,0,0,1);
            }

        </style>
        <style>
            /*PRODUCT*/
            .product-entry{
                display: block;
                position: relative;
                float: left;
                padding-top: 20px;
            }
            .product-entry .text{
                width: calc(100% - 125px);
                padding-left: 20px;
            }
            .product-entry .text h3{
                margin-bottom: 0;
                padding-bottom: 0;
            }
            .product-entry .text p{
                margin-top: 0;
            }
            .product-entry img, .product-entry .text{
                float: left;
            }

            ul.social{
                padding: 0;
            }
            ul.social li{
                display: inline-block;
                margin-right: 10px;
            }
        </style>
    </head>
    <?php
    $m_order = Orders::findOne($idorder);
    $m_payment = Payments::findOne($m_order->idpayment);
    $imgBank = $accNo = $accName = '';
    if ($m_payment) {
        $imgBank = $m_payment->img;
        $accNo = $m_payment->accountnumber;
        $accName = $m_payment->accountname;
    }
    $dataOrder = OrderUtils::getOrderData(null, null, $idorder)
    ?>
    <body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #f1f1f1;">
    <center style="width: 100%; background-color: #f1f1f1;">
        <div style="display: none; font-size: 1px;max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">
            &zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
        </div>
        <div style="max-width: 600px; margin: 0 auto;" class="email-container">
            <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">
                <tr>
                    <td valign="top" class="bg_white" style="padding: 1em 2.5em 0 2.5em;">
                        <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td class="logo" style="text-align: center;">
                                    <h1><a href="https://beibeestore.com" target="_blank">Beibeestore</a></h1>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="middle" class="hero bg_white" style="padding: 2em 0 4em 0;">
                        <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="padding: 0 2.5em; text-align: center; padding-bottom: 3em;">
                                    <div class="text">
                                        <h3>Silahkan lakukan pembayaran untuk pembelian anda, <?= $m_order->typepaymentname ?></h3>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">
                                    <div class="text-author">
                                        <img src="<?= $message->embed(Yii::getAlias('@app/web/img/payment/' . $imgBank)); ?>" alt="" style="width: 100px; max-width: 600px; height: auto; margin: auto; display: block;">
                                        <p><small><?= $dataOrder['paymentname'] ?>, No. Rek</small></p>
                                        <h3 class="name"><?= $accNo ?></h3>
                                        <span class="position"><?= $accName ?></span>
                                        <hr>
                                        <p><small>Total Tagihan</small></p>
                                        <h3 class="name"><b><?= ProductHelper::idrPrice($m_order->orderprice) ?></b></h3>
                                        <hr>
                                        <p>Setelah Transfer, Silahkan lakukan Konfirmasi Pembayaran, klik tombol di bawah</p>
                                        <p>Invoice : <b><?= $dataOrder['invoicekey'] ?></b></p>
                                        <p><a href="<?= Sendmail::domain . '/apps/customer/verifypayment' ?>" class="btn btn-primary btn-xs">Konfirmasi Bayar</a></p>
                                        <p><a href="<?= Sendmail::domain . '/apps/customer/detailorder?id=' . $idorder ?>" class="btn-custom">Detail Transaksi</a></p>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <style>
                .alg-right {
                    text-align: right;
                }
                .alg-left {
                    text-align: left;
                }
            </style>

            <div class="fbk" style="background-color:#fff">
                <h3>Rincian Pesanan</h3>
                <table>
                    <thead>
                        <tr>
                            <th width="60%" class="alg-left" style="text-align: left;">Produk</th>
                            <th width="10%"></th>
                            <th width="30%" class="alg-right" style="text-align: right;">Total Harga</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($dataOrder['orderdetail'] as $key => $perDet) { ?>
                            <tr>
                                <td class="alg-left" style="text-align: left;">
                                    <h5 id="conveying-meaning-to-assistive-technologies" style="margin-bottom: 0px;"><?= $perDet['namevarian'] ?></h5>
                                    <span>
                                        <b><?= ProductHelper::idrPrice($perDet['price']) ?></b> 
                                        <?php
                                        echo $perDet['qty'] . 'x (' . OrderUtils::satuanKg($perDet['finalweight']) . ')';
                                        if ($perDet['percent']) {
                                            echo '&nbsp;<span class="badge badge-primary">' . $perDet['percent'] . '%</span>
                                  &nbsp;<span style="text-decoration: line-through;color: #97a09d;">' . ProductHelper::idrPrice($perDet['oriprice']) . '</span>';
                                        }
                                        ?>
                                    </span>
                                </td>
                                <td></td>
                                <td class="alg-right" style="text-align: right;">
                                    <p><b><?= ProductHelper::idrPrice($perDet['finalprice']) ?></b></p>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table><br>

                <h3>Ringkasan Pembayaran</h3>
                <table class="tabel table-striped" style="width: 62%;">
                    <tbody>
                        <tr>
                            <td width="60%" class="alg-left" style="text-align: left;">Total Harga <?= '(' . $dataOrder['totaldetail'] . ' Barang)' ?></td>
                            <td width="10%"></td>
                            <td width="30%" class="alg-right" style="text-align: right;"><b><?= ProductHelper::idrPrice($dataOrder['totalprice']) ?></b></td>
                        </tr>
                        <tr>
                            <td class="alg-left" style="text-align: left;">Total Ongkos Kirim <?= $dataOrder['namecourier'] . ' (' . $dataOrder['nameservice'] . ') ' ?><?= '(' . OrderUtils::satuanKg($dataOrder['totalweight']) . ')' ?></td>
                            <td></td>
                            <td class="alg-right" style="text-align: right;"><b><?= ProductHelper::idrPrice($dataOrder['cost']) ?></b></td>
                        </tr>
                        <tr>
                            <td class="alg-left" style="text-align: left;"><b>Total Tagihan</b></td>
                            <td></td>
                            <td class="alg-right" style="text-align: right;"><b><?= ProductHelper::idrPrice($dataOrder['orderprice']) ?></b></td>
                        </tr>
                    </tbody>
                </table><br><br><br>
            </div>

            <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">
                <tr>
                    <td valign="middle" class="bg_light footer email-section">
                        <table>
                            <tr>
                                <td valign="top" width="33.333%" style="padding-top: 20px;">
                                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                        <tr>
                                            <td style="text-align: left; padding-left: 10px;">
                                                <h3 class="heading">Akun</h3>
                                                <ul>
                                                    <li><a href="<?= Sendmail::domain . '/apps/customer' ?>">Transaksi</a></li>
                                                    <li><a href="<?= Sendmail::domain . '/apps/customer/verifypayment' ?>">Konfirmasi Bayar</a></li>
                                                    <li><a href="<?= Sendmail::domain . '/apps/customer/profile' ?>">Profil</a></li>
                                                    <li><a href="<?= Sendmail::domain . '/apps/customer/address' ?>">Alamat</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr><!-- end: tr -->
                <tr>
                    <td class="bg_light" style="text-align: center;">
<!--                        <p>Selamat berbelanja di Beibeestore</p>-->
                    </td>
                </tr>
            </table>

        </div>
    </center>
</body>
</html>