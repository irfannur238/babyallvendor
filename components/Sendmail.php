<?php

namespace app\components;

use Swift_SwiftException;
use Yii;

class Sendmail {

    const domain = 'https://www.beibeestore.com/web';

    public static function sendMailPayment($to = '', $idorder = null) {
        $subject = 'Order Menunggu Pembayaran';
        $from = ['beibee@beibeestore.com' => 'Beibee'];
        $to = trim(str_replace(' ', '', $to));

        if (!filter_var($to, FILTER_VALIDATE_EMAIL)) {
            Yii::$app->session->setFlash('danger', "Email " . $to . " tidak terkirim.");
            return false;
        }

        try {
            Yii::$app->mailer->compose('orderPayment', ['idorder' => $idorder])
                    ->setFrom($from)
                    ->setTo($to)
                    ->setSubject($subject)
                    ->send();
        } catch (Swift_SwiftException $ex) {
            Yii::$app->session->setFlash('danger', "Email " . $to . " tidak terkirim.");
            return false;
        }
    }
    
    public static function sendMailConfirmPayment($to = '', $idorder = null) {
        $subject = 'Pembayaran Masuk';
        $from = ['beibee@beibeestore.com' => 'Beibee'];
        $to = trim(str_replace(' ', '', $to));

        if (!filter_var($to, FILTER_VALIDATE_EMAIL)) {
            Yii::$app->session->setFlash('danger', "Email " . $to . " tidak terkirim.");
            return false;
        }

        try {
            Yii::$app->mailer->compose('confirmPayment', ['idorder' => $idorder])
                    ->setFrom($from)
                    ->setTo($to)
                    ->setSubject($subject)
                    ->send();
        } catch (Swift_SwiftException $ex) {
            Yii::$app->session->setFlash('danger', "Email " . $to . " tidak terkirim.");
            return false;
        }
    }

    public static function sendattach($from = '', $to = '', $subject = '', $content = '', $filelocation = '') {
        $from = ['beibee@beibeestore.com' => 'Beibee'];
        $to = trim(str_replace(' ', '', $to));
        if (!filter_var($to, FILTER_VALIDATE_EMAIL)) {
            Yii::$app->session->setFlash('danger', "Email " . $to . " tidak terkirim.");
            return false;
        }
        try {
            Yii::$app->mailer->compose('layouts/html', ['content' => $content,])
                    ->setFrom($from)
                    ->setTo($to)
                    ->setSubject($subject)
                    ->attach($filelocation)
                    ->send();
        } catch (Swift_SwiftException $ex) {
            Yii::$app->session->setFlash('danger', "Email " . $to . " tidak terkirim.");
            return false;
        }
    }

    public static function sendTest($to = '', $subject = null, $content = null) {
        $subject = $subject ? $subject : 'Test !!';
        $content = $content ? $content : date("r") . ' Ini Tes Segala Tes.';
        $from = ['beibee@beibeestore.com' => 'Beibee'];
        $to = trim(str_replace(' ', '', $to));

        if (!filter_var($to, FILTER_VALIDATE_EMAIL)) {
            Yii::$app->session->setFlash('danger', "Email " . $to . " tidak terkirim.");
            return false;
        }

        try {
            Yii::$app->mailer->compose('layouts/html', ['content' => $content])
                    ->setFrom($from)
                    ->setTo($to)
                    ->setSubject($subject)
                    ->send();
        } catch (Swift_SwiftException $ex) {
            Yii::$app->session->setFlash('danger', "Email " . $to . " tidak terkirim.");
            return false;
        }
    }

}

?>
