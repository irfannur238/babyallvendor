<?php

namespace app\commands;

use app\components\Sendmail;
use app\helpers\OrderUtils;
use app\models\Orders;
use yii\console\Controller;

class OrdersController extends Controller {

    public function actionCanceledorder() {
        /*
         * cancel order 
         * 1 hari belum bayar
         */
        $date_raw = date("r");
        $minOneDay = date('Y-m-d H:i', strtotime('-1 day', strtotime($date_raw)));

        $orders = Orders::find()
                ->where(['<', "orderdate", $minOneDay])
                ->andWhere(['status' => OrderUtils::STAT_WAITING_PAYMENT])
                ->orderBy(['orderdate' => SORT_DESC])
                ->all();

        $msgErr = [];
        foreach ($orders as $perOrder) {
            $m_order = Orders::findOne(['idorder' => $perOrder->idorder]);
            if ($m_order) {
                $m_order->status = OrderUtils::STAT_CANCELED;
                if ($m_order->save()) {
                    OrderUtils::returnQtyVarian($m_order->idorder);
                    OrderUtils::saveOrderLog($m_order);
                    echo 'OK ID : ' . $m_order->idorder . ' DATE : ' . $m_order->orderdate . "\n";
                } else {
                    $msgErr[] = json_encode($m_order->getErrors());
                    echo 'FAILED ID : ' . $m_order->idorder . ' DATE : ' . $m_order->orderdate . "\n";
                }
            }
        }

        if ($msgErr) {
            $msg = implode('</br>', $msgErr);
            Sendmail::sendTest('irfanbackup23@gmail.com', 'Err Cron Canceled Order not Paid (' . date("r") . ')', $msg);
        }
    }

    public function actionDoneorder() {
        /*
         * done order auto
         * 14 hari setelah tgl shipping
         */
        $order = Orders::find()
                ->where(['status' => OrderUtils::STAT_ONSHIPPING])
                ->orderBy(['orderdate' => SORT_DESC])
                ->all();

        $msgErr = [];
        foreach ($order as $perOrder) {
            $howDayShip = strtotime('+14 day', strtotime($perOrder->shipdate));
            $curDay = strtotime(date('Y-m-d'));
            if ($curDay >= $howDayShip) {
                //print_r(date('Y-m-d', $howDayShip));echo '<br>';
                //echo '<pre>';print_r($perOrder);die;
                $m_order = Orders::findOne(['idorder' => $perOrder->idorder]);
                if ($m_order) {
                    $m_order->status = OrderUtils::STAT_DONE;
                    if ($m_order->save()) {
                        OrderUtils::saveOrderLog($m_order);
                        echo 'OK ID : ' . $m_order->idorder . ' SHIPDATE : ' . $m_order->shipdate . "\n";
                    } else {
                        $msgErr[] = json_encode($m_order->getErrors());
                        echo 'FAILED ID : ' . $m_order->idorder . ' SHIPDATE : ' . $m_order->shipdate . "\n";
                    }
                }
            }
        }

        if ($msgErr) {
            $msg = implode('</br>', $msgErr);
            Sendmail::sendTest('irfanbackup23@gmail.com', 'Err Cron Auto Done Order (' . date("r") . ')', $msg);
        }
    }

    public function actionTestmail($email = null) {
        $femail = $email ? $email : 'qyaraofficial2020@gmail.com';
        Sendmail::sendTest($femail);
    }

}
